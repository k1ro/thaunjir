﻿using System;
using UnityEngine;

namespace Kiro
{
    [Serializable]
    public class Bullet : MonoBehaviour
    {
        public float _speed = 50.0f;
        public float _gravity = 9.81f;
        private Vector3 _force;
        private Vector3 _velocity;
        private Vector3 _newPos;
        private Vector3 _oldPos;
        private Vector3 _direction;
        private float _distance;

        public GameObject _prefab;

        private void Start()
        {
            _newPos = transform.position;
        }

        private void Update()
        {
            _force = new Vector3(0f, -_gravity, 0f);
            _newPos += (_speed * transform.forward + _force) * Time.deltaTime;

            _direction = _newPos - _oldPos;
            _distance = _direction.magnitude;

            RaycastHit hit;
            if (Physics.Raycast(_oldPos, _direction, out hit, _distance))
            {
                Debug.Log(hit.point);
                Instantiate(_prefab, hit.point, Quaternion.identity);
            }
            else
            {
                _oldPos = _newPos;
                transform.position = _newPos;
            }
        }
    }
}

