﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public float _speed = 10.0f;
    public float _sens = 5.0f;

    private Rigidbody _rb;
    private Camera _cam;

    private void Start()
    {
        _cam = Camera.main;
        _rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        MovePosition();
        LookAround();
    }

    private void MovePosition()
    {
        float xMov = Input.GetAxis("Horizontal");
        float zMov = Input.GetAxis("Vertical");

        Vector3 _velocity = (Vector3.forward * zMov + Vector3.right * xMov) * _speed;

        _rb.MovePosition(_rb.position + _velocity * Time.deltaTime);
    }

    private void LookAround()
    {
        float yRot = Input.GetAxis("Mouse X");
        float xRot = Input.GetAxis("Mouse Y");

        Quaternion rotationY = Quaternion.Euler(0f, yRot * _sens, 0f);
        Vector3 rotationX = new Vector3(-xRot * _sens, 0f, 0f);

        _rb.MoveRotation(_rb.rotation * rotationY);
        _cam.transform.Rotate(rotationX);
    }
}
