using UnityEngine;
using UnityEditor;
using System;
using System.Collections;

namespace VoxelPlay {
				
	[CustomEditor(typeof(VoxelDefinition))]
	public class VoxelPlayVoxelDefinition : Editor {

		SerializedProperty renderType, textureTop, textureSide, textureBottom;
		SerializedProperty pickupSound, buildSound, footfalls, jumpSound, landingSound, impactSound, destructionSound;
		SerializedProperty resistancePoints, canBeCollected, navigatable, windAnimation, model, offset, scale, rotation, promotesTo;
		SerializedProperty spreads, spreadDelay, spreadDelayRandom, diveColor, height;
		SerializedProperty playerDamage, playerDamageDelay;

		Color titleColor;
		static GUIStyle titleLabelStyle;

		void OnEnable() {
			titleColor = EditorGUIUtility.isProSkin ? new Color(0.52f, 0.66f, 0.9f) : new Color(0.12f, 0.16f, 0.4f);
			renderType = serializedObject.FindProperty ("renderType");
			textureTop = serializedObject.FindProperty ("textureTop");
			textureSide = serializedObject.FindProperty ("textureSide");
			textureBottom = serializedObject.FindProperty ("textureBottom");

			pickupSound = serializedObject.FindProperty ("pickupSound");
			buildSound = serializedObject.FindProperty ("buildSound");
			footfalls = serializedObject.FindProperty ("footfalls");
			jumpSound = serializedObject.FindProperty ("jumpSound");
			landingSound = serializedObject.FindProperty ("landingSound");
			impactSound = serializedObject.FindProperty ("impactSound");
			destructionSound = serializedObject.FindProperty ("destructionSound");
			resistancePoints = serializedObject.FindProperty ("resistancePoints");

			canBeCollected = serializedObject.FindProperty ("canBeCollected");
			navigatable = serializedObject.FindProperty ("navigatable");
			windAnimation = serializedObject.FindProperty ("windAnimation");
			model = serializedObject.FindProperty ("model");
			offset = serializedObject.FindProperty ("offset");
			scale = serializedObject.FindProperty ("scale");
			rotation = serializedObject.FindProperty ("rotation");
			promotesTo = serializedObject.FindProperty ("promotesTo");

			spreads = serializedObject.FindProperty ("spreads");
			spreadDelay = serializedObject.FindProperty ("spreadDelay");
			spreadDelayRandom = serializedObject.FindProperty ("spreadDelayRandom");
			diveColor = serializedObject.FindProperty ("diveColor");
			height = serializedObject.FindProperty ("height");

			playerDamage = serializedObject.FindProperty ("playerDamage");
			playerDamageDelay = serializedObject.FindProperty ("playerDamageDelay");
		}


		public override void OnInspectorGUI() {
#if UNITY_5_6_OR_NEWER
			serializedObject.UpdateIfRequiredOrScript ();
#else
			serializedObject.UpdateIfDirtyOrScript();
#endif
			if (titleLabelStyle == null) {
				titleLabelStyle = new GUIStyle(EditorStyles.label);
			}
			titleLabelStyle.normal.textColor = titleColor;
			titleLabelStyle.fontStyle = FontStyle.Bold;
			EditorGUIUtility.labelWidth = 130;

			EditorGUILayout.Separator();

			GUILayout.Label("Rendering", titleLabelStyle);
			EditorGUILayout.PropertyField (renderType);
			if (renderType.intValue == (int)RenderType.Custom) {
				EditorGUILayout.PropertyField (model);
				EditorGUILayout.PropertyField (offset);
				EditorGUILayout.PropertyField (scale);
				EditorGUILayout.PropertyField (rotation);
				EditorGUILayout.PropertyField (textureSide, new GUIContent("Texture Sample", "Texture that represents the model. Used for sampling particle colors and inventory."));
				EditorGUILayout.PropertyField (promotesTo);
			} else {
				EditorGUILayout.PropertyField (textureTop);
				EditorGUILayout.PropertyField (textureSide);
				EditorGUILayout.PropertyField (textureBottom);
			}

			EditorGUILayout.Separator();

			GUILayout.Label("Sound Effects", titleLabelStyle);
			EditorGUILayout.PropertyField (pickupSound);
			EditorGUILayout.PropertyField (buildSound);
			if (renderType.intValue != (int)RenderType.Cutout && renderType.intValue != (int)RenderType.CutoutCross && renderType.intValue != (int)RenderType.Transparent) {
				EditorGUILayout.PropertyField (footfalls, true);
			}
			EditorGUILayout.PropertyField (jumpSound);
			EditorGUILayout.PropertyField (landingSound);
			EditorGUILayout.PropertyField (impactSound);
			EditorGUILayout.PropertyField (destructionSound);

			EditorGUILayout.Separator();

			GUILayout.Label("Other Attributes", titleLabelStyle);
			EditorGUILayout.PropertyField (resistancePoints);
			EditorGUILayout.PropertyField (canBeCollected);

			RenderType rt = (RenderType)renderType.intValue; 
			if (rt.supportsNavigation()) {
				EditorGUILayout.PropertyField (navigatable);
			}
			if (rt.supportsWindAnimation()) {
				EditorGUILayout.PropertyField (windAnimation);
			}
			if (rt == RenderType.Transparent) {
				EditorGUILayout.PropertyField (height);
				EditorGUILayout.PropertyField (diveColor);
				EditorGUILayout.PropertyField (spreads);
				GUI.enabled = spreads.boolValue;
				EditorGUILayout.PropertyField (spreadDelay);
				EditorGUILayout.PropertyField (spreadDelayRandom);
				GUI.enabled = true;
			}

			EditorGUILayout.PropertyField (playerDamage);
			GUI.enabled = playerDamage.intValue > 0;
			EditorGUILayout.PropertyField (playerDamageDelay);
			GUI.enabled = true;

			serializedObject.ApplyModifiedProperties ();

		}


				

	}

}
