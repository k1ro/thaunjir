using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Collections;

namespace VoxelPlay {
				
	[CustomEditor (typeof(VoxelPlayEnvironment))]
	public class VoxelPlayEnvironmentEditor : Editor {

		SerializedProperty world, enableBuildMode, buildMode, welcomeMessage, welcomeMessageDuration, cameraMain, renderInEditor, renderInEditorLowPriority, editorDraftMode, characterController;
		SerializedProperty enableConsole, showConsole, enableInventory, enableStatusBar, enableLoadingPanel, loadSavedGame, saveFilename, enableDebugWindow, showFPS;
		SerializedProperty globalIllumination, ambientLight, enableAmbientOcclusion, ambientOcclusionIntensity, enableFogSkyBlending, textureSize, useGeometryShaders, shadowsOnWater, enableShadows, enableTinting;
		SerializedProperty hqFiltering, mipMapBias;
		SerializedProperty maxChunks, prewarmChunksInEditor, visibleChunksDistance, forceChunkDistance, maxCPUTimePerFrame, maxChunksPerFrame, maxTreesPerFrame, maxBushesPerFrame, onlyGenerateInFrustum, onlyRenderInFrustum;
		#if !UNITY_WEBGL
		SerializedProperty multiThreadGeneration;
		#endif
		SerializedProperty enableColliders, enableTrees, denseTrees, enableVegetation, enableNavMesh;
		SerializedProperty sun, fogAmount, fogDistance, fogFalloff, enableClouds;
		SerializedProperty uiCanvasPrefab, crosshairPrefab, crosshairTexture, consoleBackgroundColor, statusBarBackgroundColor;
		SerializedProperty defaultBuildSound, defaultPickupSound, defaultImpactSound, defaultDestructionSound, defaultVoxel;
		SerializedProperty layerParticles, layerVoxels;

		VoxelPlayEnvironment env;
		WorldDefinition cachedWorld;
		VoxelPlayTerrainGenerator cachedTerrainGenerator;
		Editor cachedWorldEditor, cachedTerrainGeneratorEditor;
		static GUIStyle titleLabelStyle, boxStyle;
		static bool worldExpand, terrainGeneratorExpand;
		static int cookieIndex = -1;
		Color titleColor;
		static GUIStyle sectionHeaderStyle;
		static bool expandQualitySection, expandVoxelGenerationSection, expandSkySection, expandInGameSection, expandDefaultsSection;

		void OnEnable () {
			titleColor = EditorGUIUtility.isProSkin ? new Color (0.52f, 0.66f, 0.9f) : new Color (0.12f, 0.16f, 0.4f);
			world = serializedObject.FindProperty ("world");
			enableBuildMode = serializedObject.FindProperty ("enableBuildMode");
			buildMode = serializedObject.FindProperty ("buildMode");
			welcomeMessage = serializedObject.FindProperty ("welcomeMessage");
			welcomeMessageDuration = serializedObject.FindProperty ("welcomeMessageDuration");
			cameraMain = serializedObject.FindProperty ("cameraMain");
			renderInEditor = serializedObject.FindProperty ("renderInEditor");
			renderInEditorLowPriority = serializedObject.FindProperty ("renderInEditorLowPriority");
			editorDraftMode = serializedObject.FindProperty ("editorDraftMode");
			characterController = serializedObject.FindProperty ("characterController");
			enableConsole = serializedObject.FindProperty ("enableConsole");
			consoleBackgroundColor = serializedObject.FindProperty ("consoleBackgroundColor");
			showConsole = serializedObject.FindProperty ("showConsole");
			enableInventory = serializedObject.FindProperty ("enableInventory");
			prewarmChunksInEditor = serializedObject.FindProperty ("prewarmChunksInEditor");
			enableLoadingPanel = serializedObject.FindProperty ("enableLoadingPanel");
			loadSavedGame = serializedObject.FindProperty ("loadSavedGame");
			saveFilename = serializedObject.FindProperty ("saveFilename");
			enableDebugWindow = serializedObject.FindProperty ("enableDebugWindow");
			showFPS = serializedObject.FindProperty ("showFPS");

			globalIllumination = serializedObject.FindProperty ("globalIllumination");
			ambientLight = serializedObject.FindProperty ("ambientLight");
			enableAmbientOcclusion = serializedObject.FindProperty ("enableAmbientOcclusion");
			ambientOcclusionIntensity = serializedObject.FindProperty ("ambientOcclusionIntensity");
			enableFogSkyBlending = serializedObject.FindProperty ("enableFogSkyBlending");
			textureSize = serializedObject.FindProperty ("textureSize");
			useGeometryShaders = serializedObject.FindProperty ("useGeometryShaders");
			shadowsOnWater = serializedObject.FindProperty ("shadowsOnWater");
			enableShadows = serializedObject.FindProperty ("enableShadows");
			enableTinting = serializedObject.FindProperty ("enableTinting");
			hqFiltering = serializedObject.FindProperty ("hqFiltering");
			mipMapBias = serializedObject.FindProperty ("mipMapBias");
			maxChunks = serializedObject.FindProperty ("maxChunks");
			visibleChunksDistance = serializedObject.FindProperty ("_visibleChunksDistance");
			forceChunkDistance = serializedObject.FindProperty ("forceChunkDistance");
			maxCPUTimePerFrame = serializedObject.FindProperty ("maxCPUTimePerFrame");
			maxChunksPerFrame = serializedObject.FindProperty ("maxChunksPerFrame");
			maxTreesPerFrame = serializedObject.FindProperty ("maxTreesPerFrame");
			maxBushesPerFrame = serializedObject.FindProperty ("maxBushesPerFrame");
			#if !UNITY_WEBGL
			multiThreadGeneration = serializedObject.FindProperty ("multiThreadGeneration");
			#endif
			onlyGenerateInFrustum = serializedObject.FindProperty ("onlyGenerateInFrustum");
			onlyRenderInFrustum = serializedObject.FindProperty ("onlyRenderInFrustum");
			enableColliders = serializedObject.FindProperty ("enableColliders");
			enableNavMesh = serializedObject.FindProperty ("enableNavMesh");
			enableTrees = serializedObject.FindProperty ("enableTrees");
			denseTrees = serializedObject.FindProperty ("denseTrees");
			enableVegetation = serializedObject.FindProperty ("enableVegetation");

			sun = serializedObject.FindProperty ("sun");
			fogAmount = serializedObject.FindProperty ("fogAmount");
			fogDistance = serializedObject.FindProperty ("fogDistance");
			fogFalloff = serializedObject.FindProperty ("fogFalloff");
			enableClouds = serializedObject.FindProperty ("enableClouds");

			uiCanvasPrefab = serializedObject.FindProperty ("UICanvasPrefab");
			crosshairPrefab = serializedObject.FindProperty ("crosshairPrefab");
			crosshairTexture = serializedObject.FindProperty ("crosshairTexture");

			enableStatusBar = serializedObject.FindProperty ("enableStatusBar");
			statusBarBackgroundColor = serializedObject.FindProperty ("statusBarBackgroundColor");

			layerParticles = serializedObject.FindProperty ("layerParticles");
			layerVoxels = serializedObject.FindProperty ("layerVoxels");

			defaultBuildSound = serializedObject.FindProperty ("defaultBuildSound");
			defaultPickupSound = serializedObject.FindProperty ("defaultPickupSound");
			defaultImpactSound = serializedObject.FindProperty ("defaultImpactSound");
			defaultDestructionSound = serializedObject.FindProperty ("defaultDestructionSound");
			defaultVoxel = serializedObject.FindProperty ("defaultVoxel");

			env = (VoxelPlayEnvironment)target;
			if (!env.initialized) {
				env.Init ();
			}
			env.WantRepaintInspector += this.Repaint;

			worldExpand = EditorPrefs.GetBool ("VoxelPlayWorldSection", worldExpand);
			terrainGeneratorExpand = EditorPrefs.GetBool ("VoxelPlayTerrainGeneratorSection", terrainGeneratorExpand);

			expandQualitySection = EditorPrefs.GetBool ("VoxelPlayExpandQualitySection", false);
			expandVoxelGenerationSection = EditorPrefs.GetBool ("VoxelPlayVoxelGenerationSection", false);
			expandSkySection = EditorPrefs.GetBool ("VoxelPlaySkySection", false);
			expandInGameSection = EditorPrefs.GetBool ("VoxelPlayInGameSection", false);
			expandDefaultsSection = EditorPrefs.GetBool ("VoxelPlayDefaultsSection", false);
		}

		void OnDisable () {
			if (env != null) {
				env.WantRepaintInspector -= this.Repaint;
			}
			EditorPrefs.SetBool ("VoxelPlayExpandQualitySection", expandQualitySection);
			EditorPrefs.SetBool ("VoxelPlayVoxelGenerationSection", expandVoxelGenerationSection);
			EditorPrefs.SetBool ("VoxelPlaySkySection", expandSkySection);
			EditorPrefs.SetBool ("VoxelPlayInGameSection", expandInGameSection);
			EditorPrefs.SetBool ("VoxelPlayDefaultsSection", expandDefaultsSection);
		}


		public override void OnInspectorGUI () {
#if UNITY_5_6_OR_NEWER
			serializedObject.UpdateIfRequiredOrScript ();
#else
			serializedObject.UpdateIfDirtyOrScript();
#endif
			if (boxStyle == null) {
				boxStyle = new GUIStyle (GUI.skin.box);
				boxStyle.padding = new RectOffset (15, 10, 5, 5);
			}
			if (titleLabelStyle == null) {
				titleLabelStyle = new GUIStyle (EditorStyles.label);
			}
			titleLabelStyle.normal.textColor = titleColor;
			titleLabelStyle.fontStyle = FontStyle.Bold;
			EditorGUIUtility.labelWidth = 150;
			if (sectionHeaderStyle == null) {
				sectionHeaderStyle = new GUIStyle (EditorStyles.foldout);
			}
			sectionHeaderStyle.SetFoldoutColor ();

			if (cookieIndex >= 0) {
				EditorGUILayout.Separator ();
				EditorGUILayout.LabelField ("Help Cookie", titleLabelStyle);
				EditorGUILayout.HelpBox (VoxelPlayCookie.GetCookie (cookieIndex), MessageType.Info);
				EditorGUILayout.BeginHorizontal ();
				GUILayout.Label ("  ");
				ShowHelpButtons (true);
				EditorGUILayout.EndHorizontal ();
			}

			EditorGUILayout.Separator ();

			EditorGUILayout.BeginHorizontal ();
			GUILayout.Label ("General Settings", titleLabelStyle);
			if (cookieIndex < 0)
				ShowHelpButtons (false);
			EditorGUILayout.EndHorizontal ();

			bool rebuildWorld = false;
			bool refreshChunks = false;
			bool reloadWorldTextures = false;
			bool updateTintingMacro = false;

			// General settings
			EditorGUILayout.PropertyField (characterController, new GUIContent ("Character Controller", "A reference to the character controller script in the Voxel Play FPS Controller game object. It will be set automatically when you add the Voxel Play FPS Controller to the scene."));
			EditorGUILayout.BeginHorizontal ();
			WorldDefinition wd = (WorldDefinition)world.objectReferenceValue;
			EditorGUILayout.PropertyField (world, new GUIContent ("World", "The world definition asset. This asset contains the definition of biomes, voxels, items and other world-specific options."));
			if (wd != world.objectReferenceValue)
				rebuildWorld = true;
			if (GUILayout.Button ("Create", GUILayout.Width (50))) {
				CreateWorldDefinition ();
			}
			if (GUILayout.Button ("Locate", GUILayout.Width (50))) {
				Selection.activeObject = world.objectReferenceValue;
			}
			EditorGUILayout.EndHorizontal ();

			if (world.objectReferenceValue != null) {
				if (GUILayout.Button ("Expand/Collapse World Settings")) {
					worldExpand = !worldExpand;
					EditorPrefs.SetBool ("VoxelPlayWorldSection", worldExpand);
				}
				if (worldExpand) {
					if (cachedWorld != world.objectReferenceValue) {
						cachedWorldEditor = null;
					}
					if (cachedWorldEditor == null) {
						cachedWorld = (WorldDefinition)world.objectReferenceValue;
						cachedWorldEditor = Editor.CreateEditor (world.objectReferenceValue);
					}

					// Drawing the world editor
					EditorGUILayout.BeginVertical (boxStyle);
					cachedWorldEditor.OnInspectorGUI ();
					EditorGUILayout.EndVertical ();
					EditorGUILayout.Separator ();
				}

				VoxelPlayTerrainGenerator terrainGenerator = (VoxelPlayTerrainGenerator)((WorldDefinition)world.objectReferenceValue).terrainGenerator;
				if (terrainGenerator != null) {
					if (GUILayout.Button ("Expand/Collapse Generator Settings")) {
						terrainGeneratorExpand = !terrainGeneratorExpand;
						EditorPrefs.SetBool ("VoxelPlayTerrainGeneratorSection", terrainGeneratorExpand);
					}
					if (terrainGeneratorExpand) {
						if (terrainGenerator != cachedTerrainGenerator) {
							cachedTerrainGeneratorEditor = null;
						}
						if (cachedTerrainGeneratorEditor == null) {
							cachedTerrainGenerator = terrainGenerator;
							cachedTerrainGeneratorEditor = Editor.CreateEditor (terrainGenerator);
						}

						// Drawing the world editor
						EditorGUILayout.BeginVertical (boxStyle);
						cachedTerrainGeneratorEditor.OnInspectorGUI ();
						EditorGUILayout.EndVertical ();
						EditorGUILayout.Separator ();
					}
				}
												
			}
			EditorGUILayout.BeginHorizontal ();
			if (GUILayout.Button ("Regenerate Terrain")) {
				renderInEditor.boolValue = true;
				rebuildWorld = true;
			}
			if (GUILayout.Button ("Toggle Chunks")) {
				env.ChunksToggle ();
				SceneView.RepaintAll ();
			}
			if (GUILayout.Button ("Delete Chunks")) {
				renderInEditor.boolValue = false;
				env.DisposeAll ();
			}
			EditorGUILayout.EndHorizontal ();

			EditorGUILayout.PropertyField (renderInEditor, new GUIContent ("Render In Editor", "Enable world rendering in Editor. If disabled, world will only be visible during play mode."));
			if (!renderInEditor.boolValue)
				GUI.enabled = false;
			EditorGUILayout.PropertyField (renderInEditorLowPriority, new GUIContent ("   Low Priority", "When enabled, rendering in editor will only execute when scene camera is static."));
			if (wd != world.objectReferenceValue)
				rebuildWorld = true;

			bool prevBool = editorDraftMode.boolValue;
			EditorGUILayout.PropertyField (editorDraftMode, new GUIContent ("   Draft Mode", "When enabled, voxel detail will be reduced."));
			if (prevBool != editorDraftMode.boolValue) {
				rebuildWorld = true;
			}
			GUI.enabled = true;

			if (renderInEditor.boolValue) {
				EditorGUILayout.BeginVertical (boxStyle);

				EditorGUILayout.PropertyField (cameraMain, new GUIContent ("Main Camera", "Which camera to use for distance calculations and other stuff."));
				if (env.cameraMain != null) {
					env.cameraMain.transform.position = EditorGUILayout.Vector3Field ("Main Cam Pos", env.cameraMain.transform.position);
				}
				if (SceneView.lastActiveSceneView != null && SceneView.lastActiveSceneView.camera != null) {
					EditorGUILayout.Vector3Field ("Scene Cam Pos", SceneView.lastActiveSceneView.camera.transform.position);
				}
				EditorGUILayout.BeginHorizontal ();
				if (SceneView.lastActiveSceneView != null) {
					if (GUILayout.Button ("Scene Cam To Surface")) {
						Vector3 pos = Misc.vector3zero;
						if (env.characterController != null)
							pos = env.characterController.transform.position;
						pos.y = env.GetTerrainHeight (Vector3.zero, true);
						SceneView.lastActiveSceneView.LookAt (pos + new Vector3 (50, 50, 50));
					}
					if (env.cameraMain != null && GUILayout.Button ("Find Main Cam")) {
						Vector3 pos = env.cameraMain.transform.position + new Vector3 (50, 50, 50);
						Vector3 fwd = (env.cameraMain.transform.position - pos).normalized;
						SceneView.lastActiveSceneView.LookAt (pos, Quaternion.LookRotation (fwd));
					}
				}
				EditorGUILayout.EndHorizontal ();
				EditorGUILayout.EndVertical ();

			}

			// Quality and effects
			EditorGUILayout.Separator ();
			expandQualitySection = EditorGUILayout.Foldout (expandQualitySection, "Quality And Effects", sectionHeaderStyle);
			if (expandQualitySection) {

				EditorGUILayout.BeginHorizontal ();
				EditorGUILayout.LabelField ("Preset", GUILayout.Width (120));
				if (GUILayout.Button (new GUIContent ("All Features", "Enables all engine visual features available for the active platform."))) {
					globalIllumination.boolValue = true;
					enableShadows.boolValue = true;
					shadowsOnWater.boolValue = true;
					enableAmbientOcclusion.boolValue = true;
					enableFogSkyBlending.boolValue = true;
					denseTrees.boolValue = true;
					hqFiltering.boolValue = true;
					if (VoxelPlayFirstPersonController.instance != null) {
						VoxelPlayFirstPersonController.instance.autoInvertColors = true;
					}
					rebuildWorld = true;
				}
				if (GUILayout.Button (new GUIContent ("Medium", "Disables shadows to improve performance but keeps global illumination."))) {
					globalIllumination.boolValue = true;
					enableShadows.boolValue = false;
					shadowsOnWater.boolValue = false;
					enableAmbientOcclusion.boolValue = true;
					enableFogSkyBlending.boolValue = true;
					denseTrees.boolValue = true;
					hqFiltering.boolValue = true;
					if (VoxelPlayFirstPersonController.instance != null) {
						VoxelPlayFirstPersonController.instance.autoInvertColors = true;
					}
					rebuildWorld = true;
				}
				if (GUILayout.Button (new GUIContent ("Fastest", "Disables all effects to improve performance."))) {
					globalIllumination.boolValue = false;
					enableShadows.boolValue = false;
					shadowsOnWater.boolValue = false;
					enableAmbientOcclusion.boolValue = false;
					enableFogSkyBlending.boolValue = false;
					denseTrees.boolValue = false;
					hqFiltering.boolValue = false;
					onlyGenerateInFrustum.boolValue = true;
					onlyRenderInFrustum.boolValue = true;
					if (VoxelPlayFirstPersonController.instance != null) {
						VoxelPlayFirstPersonController.instance.autoInvertColors = false;
					}
					if (visibleChunksDistance.intValue > 6) {
						visibleChunksDistance.intValue = 6;
					}
					if (forceChunkDistance.intValue > 2) {
						forceChunkDistance.intValue = 2;
					}
					if (maxChunks.intValue > 5000) {
						maxChunks.intValue = 5000;
					}
					rebuildWorld = true;
				}
				EditorGUILayout.EndHorizontal ();

				prevBool = globalIllumination.boolValue;
				EditorGUILayout.PropertyField (globalIllumination, new GUIContent ("Global Illumination", "Enables Voxel Play's own lightmap computation. This option adds smooth shading and lighting in combination with Unity shadow system."));
				if (globalIllumination.boolValue != prevBool)
					refreshChunks = true;
				#if UNITY_WEBGL
				GUI.enabled = false;
				#endif
                if (env.isMobilePlatform || SystemInfo.graphicsDeviceType == UnityEngine.Rendering.GraphicsDeviceType.Metal) {
					GUI.enabled = false;
				}
				prevBool = useGeometryShaders.boolValue;
				if (!GUI.enabled) {
					EditorGUILayout.BeginHorizontal ();
					EditorGUILayout.LabelField ("Geometry Shaders", GUILayout.Width (EditorGUIUtility.labelWidth));
					EditorGUILayout.LabelField ("(Unsupported platform or graphics API)");
					EditorGUILayout.EndHorizontal ();
				} else {
					EditorGUILayout.PropertyField (useGeometryShaders, new GUIContent ("Geometry Shaders", "Uses geometry shaders to render voxels. Not supported on mobile nor WebGL."));
				}
				if (useGeometryShaders.boolValue != prevBool)
					rebuildWorld = true;
				GUI.enabled = true;
				prevBool = enableShadows.boolValue;
				EditorGUILayout.PropertyField (enableShadows, new GUIContent ("Enable Shadows", "Turns on/off shadow casting and receiving on voxels."));
				if (enableShadows.boolValue != prevBool)
					rebuildWorld = true;
				prevBool = shadowsOnWater.boolValue;
				EditorGUILayout.PropertyField (shadowsOnWater, new GUIContent ("Shadows On Water", "Enables shadow receiving on water surface (only available with Geometry Shaders)."));
				if (shadowsOnWater.boolValue != prevBool)
					rebuildWorld = true;

				prevBool = enableAmbientOcclusion.boolValue;
				EditorGUILayout.PropertyField (enableAmbientOcclusion, new GUIContent ("Ambient Occlusion", "Enables ambient occlusion effect on voxels."));
				if (enableAmbientOcclusion.boolValue != prevBool)
					refreshChunks = true;
				GUI.enabled = enableAmbientOcclusion.boolValue;
				float prevFloat = ambientOcclusionIntensity.floatValue;
				EditorGUILayout.PropertyField (ambientOcclusionIntensity, new GUIContent ("   Intensity", "Intensity of the ambient occlusion integrated effect."));
				if (ambientOcclusionIntensity.floatValue != prevFloat)
					refreshChunks = true;
				GUI.enabled = true;
				EditorGUILayout.PropertyField (ambientLight, new GUIContent ("Ambient Light", "Minimum amount of light in the scene affecting the voxels."));
				EditorGUILayout.PropertyField (textureSize, new GUIContent ("Texture Size", "Texture size should be a multiple of 2 (eg. 16, 32, 64, 128)"));
				prevBool = enableTinting.boolValue;
				EditorGUILayout.PropertyField (enableTinting, new GUIContent ("Enable Tinting", "Enables individual voxel tint color."));
				if (prevBool != enableTinting.boolValue) {
					refreshChunks = true;
					updateTintingMacro = true;
				}
				prevBool = hqFiltering.boolValue;
				EditorGUILayout.PropertyField (hqFiltering, new GUIContent ("HQ Filtering", "Enables mipmapping and intergrated texel antialiasing."));
				if (prevBool != hqFiltering.boolValue) {
					refreshChunks = true;
					reloadWorldTextures = true;
				}
				if (hqFiltering.boolValue) {
					prevFloat = mipMapBias.floatValue;
					EditorGUILayout.PropertyField (mipMapBias, new GUIContent ("   MipMap Bias", "Increase to reduce texture blurring."));
					if (mipMapBias.floatValue != prevFloat) {
						refreshChunks = true;
						reloadWorldTextures = true;
					}
				}
			}
			// Voxel Generation
			EditorGUILayout.Separator ();
			expandVoxelGenerationSection = EditorGUILayout.Foldout (expandVoxelGenerationSection, "Voxel Generation", sectionHeaderStyle);
			if (expandVoxelGenerationSection) {
				ShowProgressBar ("Chunk Rendering: Pending (" + env.chunksInRenderQueueCount + ") / Drawn (" + env.chunksDrawn + ")", (env.chunksDrawn + 1f) / (env.chunksDrawn + env.chunksInRenderQueueCount + 1f));
				if (env.enableTrees) {
					ShowProgressBar ("Tree Creation: Pending (" + env.treesInCreationQueueCount + ") / Created (" + env.treesCreated + ")", (env.treesCreated + 1f) / (env.treesCreated + env.treesInCreationQueueCount + 1f));
				} else {
					ShowProgressBar ("Tree Creation: ---", 1f);
				}
				if (env.enableVegetation) {
					ShowProgressBar ("Bush Creation: Pending (" + env.vegetationInCreationQueueCount + ") / Created (" + env.vegetationCreated + ")", (env.vegetationCreated + 1f) / (env.vegetationCreated + env.vegetationInCreationQueueCount + 1f));
				} else {
					ShowProgressBar ("Bush Creation: ---", 1f);
				}
				EditorGUILayout.LabelField ("Total Chunks Created", env.chunksCreated.ToString ());
				EditorGUILayout.LabelField ("Total Voxels Created", env.voxelsCreatedCount.ToString ());

				EditorGUILayout.PropertyField (maxChunks, new GUIContent ("Chunks Pool Size", "Number of total chunks allowed in memory."));
				EditorGUILayout.LabelField ("   Recommended >=", env.maxChunksRecommended.ToString ());
				EditorGUILayout.IntSlider (prewarmChunksInEditor, 1000, maxChunks.intValue, new GUIContent ("   Prewarm In Editor", "Number of chunks that will be reserved during start in Unity Editor before game starts. In the final build, all chunks are reserved before game starts to provide a smooth gameplay experience."));
				EditorGUILayout.PropertyField (enableLoadingPanel, new GUIContent ("   Loading Screen", "Shows a loading panel during start up while chunks are being reserved."));
				EditorGUILayout.LabelField ("   Used", env.chunksUsed.ToString () + " (" + (env.chunksUsed * 100f / env.maxChunks).ToString ("F1") + "% Pool)");
				EditorGUILayout.Separator ();
				EditorGUILayout.PropertyField (onlyGenerateInFrustum, new GUIContent ("Only Generate In Frustum", "When enabled, only chunks inside the camera frustum are generated."));
				EditorGUILayout.PropertyField (onlyRenderInFrustum, new GUIContent ("Only Render In Frustum", "When enabled, only chunks inside the camera frustum will be rendered."));
				#if UNITY_WEBGL
				GUI.enabled = false;
				EditorGUILayout.BeginHorizontal ();
				EditorGUILayout.LabelField ("Multi Thread Generation", GUILayout.Width (EditorGUIUtility.labelWidth));
				EditorGUILayout.LabelField ("(Unsupported platform)");
				EditorGUILayout.EndHorizontal ();
				GUI.enabled = true;
				#else
				EditorGUILayout.PropertyField (multiThreadGeneration, new GUIContent ("Multi Thread Generation", "When enabled, uses a dedicated background thread for chunk generation (only in build, deactivated while running inside Unity Editor)."));
				#endif
				EditorGUILayout.PropertyField (visibleChunksDistance, new GUIContent ("Visible Chunk Distance", "Measured in number of chunks."));
				EditorGUILayout.PropertyField (forceChunkDistance, new GUIContent ("Force Chunk Distance", "Distance measured in chunks that will be rendered completely before starting the game."));
				EditorGUILayout.PropertyField (maxCPUTimePerFrame, new GUIContent ("Max CPU Time Per Frame", "Maximum milliseconds that can be used by the CPU per frame to generate the world."));
				EditorGUILayout.PropertyField (maxChunksPerFrame, new GUIContent ("Max Chunks Per Frame", "Maximum number of chunks that can be generated in a single frame (0 = unlimited (limited only by the maxCPUTimePerFrame value)"));
				EditorGUILayout.PropertyField (maxTreesPerFrame, new GUIContent ("Max Trees Per Frame", "Maximum number of trees that can be generated in a single frame  (0 = unlimited (limited only by the maxCPUTimePerFrame value)"));
				EditorGUILayout.PropertyField (maxBushesPerFrame, new GUIContent ("Max Bushes Per Frame", "Maximum number of bushes that can be generated in a single frame  (0 = unlimited (limited only by the maxCPUTimePerFrame value)"));
				prevBool = enableColliders.boolValue;
				EditorGUILayout.PropertyField (enableColliders, new GUIContent ("Enable Colliders", "Enables/disables collider generation for opaque voxels."));
				if (enableColliders.boolValue != prevBool)
					rebuildWorld = true;
				prevBool = enableNavMesh.boolValue;
				EditorGUILayout.PropertyField (enableNavMesh, new GUIContent ("Enable NavMesh", "Enables/disables NavMesh generation."));
				if (enableNavMesh.boolValue) {
					EditorGUILayout.HelpBox ("Experimental feature: NavMesh generation is not yet optimized for big/complex worlds.", MessageType.Warning);
				}
				if (enableNavMesh.boolValue != prevBool)
					rebuildWorld = true;
				prevBool = enableTrees.boolValue;
				EditorGUILayout.PropertyField (enableTrees, new GUIContent ("Enable Trees", "Enables/disables tree generation."));
				if (enableTrees.boolValue != prevBool)
					rebuildWorld = true;
				if (enableTrees.boolValue) {
					prevBool = denseTrees.boolValue;
					EditorGUILayout.PropertyField (denseTrees, new GUIContent ("   Dense Trees", "If enabled, disables adjacent voxel occlusion making tree leaves cutout denser."));
					if (denseTrees.boolValue != prevBool)
						refreshChunks = true;
				}
				prevBool = enableVegetation.boolValue;
				EditorGUILayout.PropertyField (enableVegetation, new GUIContent ("Enable Vegetation", "Enables/disables bush generation."));
				if (enableVegetation.boolValue != prevBool)
					rebuildWorld = true;
			}

			// Sky Properties
			EditorGUILayout.Separator ();
			expandSkySection = EditorGUILayout.Foldout (expandSkySection, "Sky Properties", sectionHeaderStyle);
			if (expandSkySection) {
				EditorGUILayout.PropertyField (sun, new GUIContent ("Sun", "Assigns the directional light used as the Sun."));
				EditorGUILayout.PropertyField (enableFogSkyBlending, new GUIContent ("Enable Fog", "Enabled fog/sky blending."));
				GUI.enabled = enableFogSkyBlending.boolValue;
				EditorGUILayout.PropertyField (fogAmount, new GUIContent ("   Fog Amount", "Amount of fog."));
				EditorGUILayout.PropertyField (fogDistance, new GUIContent ("   Fog Distance", "Fog's distance factor"));
				EditorGUILayout.PropertyField (fogFalloff, new GUIContent ("   Fog FallOff", "Fog's falloff factor"));
				GUI.enabled = true;
				EditorGUILayout.PropertyField (enableClouds, new GUIContent ("Enable Clouds", "Clouds generation on/off"));
			}

			EditorGUILayout.Separator ();
			expandInGameSection = EditorGUILayout.Foldout (expandInGameSection, "UI & Game Features", sectionHeaderStyle);
			if (expandInGameSection) {
				EditorGUILayout.PropertyField (enableBuildMode, new GUIContent ("Enable Build Mode", "Enables entering Build Mode by pressing key B. In build mode, all world items are available in the inventory in unlimited amount and anything can be destroyed with a single hit. Player is also indestructible."));
				GUI.enabled = enableBuildMode.boolValue;
				EditorGUILayout.PropertyField (buildMode, new GUIContent ("   Build Mode ON", "Activates build mode."));
				GUI.enabled = true;
				EditorGUILayout.PropertyField (enableConsole, new GUIContent ("Enable Console", "Enables console system. Shows when pressing F1."));
				GUI.enabled = enableConsole.boolValue;
				EditorGUILayout.PropertyField (showConsole, new GUIContent ("   Visible", "Toggles console visibility on/off. The console shows useful data for debugging purposes."));
				EditorGUILayout.PropertyField (consoleBackgroundColor, new GUIContent ("   Background Color"));
				GUI.enabled = true;
				EditorGUILayout.PropertyField (enableStatusBar, new GUIContent ("Enable Status Bar"));
				GUI.enabled = enableStatusBar.boolValue;
				EditorGUILayout.PropertyField (statusBarBackgroundColor, new GUIContent ("   Status Bar Color"));
				GUI.enabled = true;
				EditorGUILayout.PropertyField (enableInventory, new GUIContent ("Enable Inventory", "Enables inventory UI when pressing Tab. Disable if you wish to provide your own interface."));
				EditorGUILayout.PropertyField (enableDebugWindow, new GUIContent ("Enable Debug Window", "Enables debug window toggling using F2."));
				EditorGUILayout.PropertyField (showFPS, new GUIContent ("Show FPS", "Shows FPS on top/right screen corner."));
				EditorGUILayout.PropertyField (loadSavedGame, new GUIContent ("Load Saved Game", "If Voxel Play should load a previously saved game at start up. Specify name of saved game in 'Save Filename' field."));
				EditorGUILayout.PropertyField (saveFilename, new GUIContent ("   Filename", "The current name for the saved game file. Used at runtime when pressing F3 to load or F4 to save. You can set a different save filename at runtime to support multiple save slots."));
				EditorGUILayout.PropertyField (welcomeMessage, new GUIContent ("Welcome Text", "Optional message shown when game starts"));
				EditorGUILayout.PropertyField (welcomeMessageDuration, new GUIContent ("Welcome Duration", "Duration for the welcome text"));
				EditorGUILayout.PropertyField (uiCanvasPrefab, new GUIContent ("UI Canvas Prefab", "The canvas prefab used for the game main interface. This interface has elements for inventory, selected item, crosshair and other information."));
				EditorGUILayout.PropertyField (crosshairPrefab, new GUIContent ("Crosshair Prefab", "The prefab used for the crosshair."));
				EditorGUILayout.PropertyField (crosshairTexture, new GUIContent ("Crosshair Texture", "The texture used for the crosshair."));
				layerParticles.intValue = EditorGUILayout.LayerField (new GUIContent ("Particles Layer", "The layer used for particles. Used to optimize physics and avoid particle collision between them."), layerParticles.intValue);
				layerVoxels.intValue = EditorGUILayout.LayerField (new GUIContent ("Voxels Layer", "The layer used for voxels. Used to optimize physics and avoid voxels collision between them."), layerVoxels.intValue);
			}

			EditorGUILayout.Separator ();
			expandDefaultsSection = EditorGUILayout.Foldout (expandDefaultsSection, "Default Assets", sectionHeaderStyle);
			if (expandDefaultsSection) {
				EditorGUILayout.PropertyField (defaultBuildSound, new GUIContent ("Build Sound", "Default sound played when an item or voxel is placed in the scene."));
				EditorGUILayout.PropertyField (defaultPickupSound, new GUIContent ("Pick Up Sound", "Default sound played when an item is collected."));
				EditorGUILayout.PropertyField (defaultImpactSound, new GUIContent ("Impact Sound", "Default sound played when a voxel is hit."));
				EditorGUILayout.PropertyField (defaultDestructionSound, new GUIContent ("Destruction Sound", "Default sound played when a voxel is destroyed."));
				EditorGUILayout.PropertyField (defaultVoxel, new GUIContent ("Default Voxel", "Assumed voxel when the voxel definition is missing or placing colors directly on the positions."));
			}

			EditorGUILayout.Separator ();

			if (GUILayout.Button ("Import Models...")) {
				VoxelPlayImportTools.ShowWindow ();
			}

			EditorGUILayout.Separator ();

			if (serializedObject.ApplyModifiedProperties () || rebuildWorld || (Event.current.type == EventType.ExecuteCommand &&
			    Event.current.commandName == "UndoRedoPerformed")) {
				if (updateTintingMacro) {
					if (enableTinting.boolValue) {
						Debug.Log ("Optimization: enabling tinting in Voxel.cs script...");
					} else {
						Debug.Log ("Optimization: disabling tinting in Voxel.cs script...");
					}
					env.UpdateTintingCodeMacro (enableTinting.boolValue);
				}
				if (rebuildWorld) {
					rebuildWorld = false;
					env.ReloadWorld ();

					// Check if scene camera is under terrain
					if (!Application.isPlaying && env.renderInEditor && SceneView.lastActiveSceneView != null) {
						Camera cam = SceneView.lastActiveSceneView.camera;
						if (cam != null) {
							Vector3 camPos = SceneView.lastActiveSceneView.pivot;
							float h = env.GetTerrainHeight (camPos, true);
							if (camPos.y < h + 2) {
								camPos.y = h + 2;
							} else if (camPos.y > h + 100) {
								camPos.y = h + 50f;
							}
							SceneView.lastActiveSceneView.LookAt (camPos);
						}
					}
				} else if (refreshChunks) {
					refreshChunks = false;
					env.Redraw (reloadWorldTextures);
				}
				env.UpdateMaterialProperties ();
				EditorApplication.update -= env.UpdateInEditor;
				if (renderInEditor.boolValue) {
					EditorApplication.update += env.UpdateInEditor;
				}
			}
		}

		void ShowHelpButtons (bool showHideButton) {
			if (showHideButton && GUILayout.Button ("Hide Cookie", GUILayout.Width (80))) {
				cookieIndex = -1;
				EditorGUIUtility.ExitGUI ();
			}
			if (GUILayout.Button ("New Cookie", GUILayout.Width (80))) {
				cookieIndex++;
			}
			if (GUILayout.Button ("Help", GUILayout.Width (40))) {
				if (!EditorUtility.DisplayDialog ("Voxel Play", "To learn more about a property in this inspector move the mouse over the label for a quick description (tooltip).\n\nPlease check the online Developer Guide on kronnect.com for more details and contact support by email or visiting our support forum on kronnect.com if you need help.\n\nIf you like Voxel Play, please rate it on the Asset Store.", "Close", "Visit Support Forum")) {
					Application.OpenURL ("http://kronnect.me/taptapgo/index.php/board,56.0.html");
				}
			}
		}


		void ShowProgressBar (string text, float progress) {
			Rect r = EditorGUILayout.BeginVertical ();
			EditorGUI.ProgressBar (r, progress, text);
			GUILayout.Space (18);
			EditorGUILayout.EndVertical ();
		}


		void CreateWorldDefinition () {
			WorldDefinition wd = ScriptableObject.CreateInstance<WorldDefinition> ();
			wd.name = "New World Definition";
			AssetDatabase.CreateAsset (wd, "Assets/" + wd.name + ".asset");
			AssetDatabase.SaveAssets ();
			world.objectReferenceValue = wd;
			EditorGUIUtility.PingObject (wd);
		}
				

	}

}
