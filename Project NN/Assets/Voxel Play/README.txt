************************************
*            Voxel Play          *
*     by Ramiro Oliva (Kronnect)   * 
*            README FILE           *
************************************


What's Voxel Play?
--------------------

Voxel Play is a voxelized environment for your game. It aims to provide a complete solution for terrain, sky, water, UI, inventory and character interaction.


How to use this asset
---------------------
Firstly, you should run the Demo scenes to get an idea of the overall functionality.
Then, please take a look at the online documentation to learn how to use all the features that Voxel Play can offer.

Documentation/API reference
---------------------------
The user manual is available online:
https://kronnect.freshdesk.com/support/home

You can find internal development notes in the Documentation folder.


Support
-------
Please read the documentation and browse/play with the demo scene and sample source code included before contacting us for support :-)

* E-mail support: contact@kronnect.me
* Support forum: http://kronnect.me
* Twitter: @KronnectGames


Future updates
--------------

All our assets follow an incremental development process by which a few beta releases are published on our support forum (kronnect.com).
We encourage you to signup and engage our forum. The forum is the primary support and feature discussions medium.

Of course, all updates of Voxel Play will be eventually available on the Asset Store.


Version history
---------------

0.19 b3
- [Fix] Fixed issue on WebGL

0.19
- Optimization (mesh generation): detection of fully occluded chunks. Chunks fully surrounded by other chunks skip mesh generation phase.
- Optimization (memory): chunk memory requirement down by 47% (without voxel tinting support) or 24% (with voxel tinting support)

0.18 b2
- Added constructor size parameter to FPSController inspector
- Water can now be poured from any height

0.18
- API: VoxelGetDynamic, VoxelIsDynamic
- Improved custom-type voxel rendering. Color and lighting now baked in mesh reducing draw calls.
- Added offset, rotation and scale to voxel definitions

0.17 b4
- Improved load/save format (new save file format 3.0, see: https://kronnect.freshdesk.com/solution/articles/42000036928-saved-games-file-format) 
- API: added LoadGameBinary, GetSaveGameBinary, SaveGameBinary, SaveGameToByteArray, LoadGameFromByteArray

0.17 b3
- Added hit damage / hit delay to ItemDefinition class
- Added VoxelThrow method (key G)

0.17
- New water system
- New voxel: lava
- Voxel Definition: new properties (spread/spread delay, contact damage)
- API: new events: OnVoxelBeforeSpread, OnVoxelAfterSpread
- API: new player events: OnPlayerGetDamage, OnPlayerIsKilled
- API: added VoxelDamage method

0.16 b3
- New skybox style: Earth + Day + Night cubemap (assign your HDR cubemaps in Voxel Play Environment)
- API: added GetChunks method

0.16
- New skybox style: Earth + Night cubemap (assign your HDR cubemap in Voxel Play Environment)
- API: added event VoxelPlayPlayer.OnSelectedItemChange
- [Fix] Fixes transparent voxel under leaves when placed over certain solid voxels

0.15
- Optimized/reduced CPU load on chunk thread generation (good for mobile)
- Unity point lights support
- Torches now cast a point light at position (customize point light color and behaviour in torch prefab itself)
- Trees no longer respawn over loaded chunks from saved games
- FPS character carries a light. Press L to toggle it on/off (customize light properties in FirstPersonCharacter gameobject scripts).
- API: added AddVoxelDefinition method
- API: added ModelPlace method overload to place an array of voxel definitions with optional colors
- API: ModelPlace now can return the list of all vixible voxels in the model
- API: added GetVoxelIndices: returns a list of visible voxel indices inside a given 3D box

0.14
- Unity terrain/vegetation/trees to Voxel converter/generator

0.13
- Added custom voxel definition types (eg. half blocks)
- Added VPModelTextureAlpha shader which allows transparency on models

0.12
- Added HQ filtering option
- Integrated mobile touch controls (automatically enabled when running on mobile)
- Improved reliability on mobile
- Added FPS counter (hotkey F8)
- New simplified skybox for mobile (set it in World definition; picked automatically when running on mobile)
- Added windAnimation property to Voxel Definitions
- Added Cactus tree models

0.11
- Customizable water in-block height (water block height property in World definition)
- Added greedy meshing for any geometry without ambient occlusion (clouds or terrain voxels when AO is disabled)

0.10
- Added NavMesh support (preview)
- Greedy meshing for colliders and NavMesh
- Added texture size parameter to Voxel Play Environment component
- Added bedrock voxel option to world definition
- Added minHeight parameter to terrain generator
- Added navigatable property to Voxel Definition
- New demo scene 3: Simple Flat Terrain with Simple Foes
- API: added DamageRadius to player and RayHit methods
- Player now can run/climb over voxels without jumping everytime
- Added crouching to first person controller (key C)

0.9.2 
- Added new demo scene (custom flat terrain generator)
- [Fix] Fixed casting issue on World inspector when using custom terrain generators

0.9
- Random tree rotation
- Detail spawners: villages and caves
- New debug window (F2)
- Octree implementation for faster chunk culling prepass

0.8
- Revamped collider system, now uses standard Unity colliders - now compatible with other assets!
- Add multithread chunk generation at runtime
- Improve pixel shader performance
- Smoother ambient occlusion
- Updated demo scene Colorizer
- API: Added OnVoxelBeforeDestroyed event

0.7
- Import tools: option to create prefab
- New shaders for in-game models supporting fog and smooth lighting
- Improved demo scene Earth - press 1,2,3 to create ball, deer, brick
- Improved performance of water style with no shadows
- Added flash option to status bar messages

0.6
- New demo scene: supercube
- Qubicle import tool (binary format only at this moment)
- Constructor: added instructions panel on top/right corner
- Constructor: added new options: pressing Control + AWSDQE displaces model
- Voxels now support tint color (new Enable Tinting option in inspector)
- FPS Controller: added orbit mode
- Ability to disable build mode (EnableBuildMode property)
- Ability to disable console UI (EnableConsole property)
- Ability to disable inventory UI (EnableInventory property)
- API: Added ModelPlace(posiiton, model)
- API: Added ModelPlace(posiiton, color array)
- API: Added OnVoxelBeforeDropItem, OnVoxelClick events
- API: Added SaveGameToText()
- Models are no longer limited to 16x16x16 in size and can include an optional center offset
- More internal changes and optimizations

0.5 b2
- New Scriptable Object: TerrainGenerator. Enables creation of user-defined terrain heightmap/biome providers (see documentation)
- Added terrain generator section in Voxel Play Environment inspector
- Added /unstuck console command
- FPS Controller: added fly speed parameter
- Editor: added "Scene Camera To Surface" button -> repositions Scene camera on top of terrain
- Editor: added "Find Main Camera" button -> repositions Scene camera on top of main camera
- Lot of internal changes and optimizations

0.5 b1
- Added option to disable ambient occlusion
- Added option to disable shadows
- Added 3 performance preset buttons

0.4
- New inspector option: geometry shaders (on/off; off = support for Shader Model 3.5 so rendering should work on most modern devices)
- New inspector option: water receives shadows (only geometry shader)
- New inspector option: dense trees (default = yes, off = less geometry on contiguous tree voxels)
- Improved vegetation and tree leaves shading (more color variation and ambient occlusion)
- Added 2 new bushes and denser folliage to grassland and forest

0.3
- Added "/visibleDistance" console command
- New voxel definitions can be used at runtime when calling VoxelPlace method (no need to add them all to world definition)

0.3 b1 Current Release
- Rivers. New world definition properties.

0.2 b9 12-FEB-2018
- Reusable chunk pool (control max memory usage using Chunk Pool Size property)

0.2 b7 5-FEB-2018
- Improvements to water flood system
  - Fixed water not visible under certain positions/angles
  - New flood range setting in World settings
- Added "Help Cookies" to inspector
- Fixed conflict between special keys shift, alt, Fire1 and inventory selection

0.2 1-FEB-2018
Second beta:
- Added inventory system

0.1 January/2018
First Beta

