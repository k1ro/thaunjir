﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelPlay {

	public enum VOXELPLAY_SKYBOX {
		UserDefined = 0,
		Earth = 1,
		Space = 2,
		EarthSimplified = 3,
		EarthNightCubemap = 4,
		EarthDayNightCubemap = 5
	}


	[CreateAssetMenu (menuName = "Voxel Play/World Definition", fileName = "WorldDefinition", order = 103)]
	public class WorldDefinition : ScriptableObject {

		public int seed;

		public BiomeDefinition[] biomes;

		[Header ("Terrain")]
		public VoxelPlayTerrainGenerator terrainGenerator;

		[Tooltip ("Used by terrain generator to set a hard limit in chunks at minimum height")]
		public VoxelDefinition bedrockVoxel;

		[Header ("Detail")]
		public VoxelPlayDetailGenerator[] detailGenerators;

		[Header ("Sky")]
		public VOXELPLAY_SKYBOX skyboxDesktop = VOXELPLAY_SKYBOX.Earth;
		public VOXELPLAY_SKYBOX skyboxMobile = VOXELPLAY_SKYBOX.EarthSimplified;
		public Texture skyboxDayCubemap, skyboxNightCubemap;

		[Range (-10, 10)]
		public float dayCycleSpeed = 1f;

		[Range (0, 2f)]
		public float exposure = 1f;

		[Tooltip ("Used to create clouds")]
		public VoxelDefinition cloudVoxel;

		[Range (0, 255)]
		public int cloudCoverage = 110;

		[Range (0, 255)]
		public int cloudAltitude = 150;

		public Color skyTint = new Color (0.52f, 0.5f, 1f);

		[Header ("FX")]
		public Texture2D[] voxelDamageTextures;
		public float gravity = -9.8f;

		[Header ("Additional Objects")]
		public VoxelDefinition[] moreVoxels;
		public ItemDefinition[] items;

	}

}