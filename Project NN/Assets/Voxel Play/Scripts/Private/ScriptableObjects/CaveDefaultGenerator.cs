﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelPlay {

	[CreateAssetMenu(menuName = "Voxel Play/Detail Generators/Cave Generator", fileName = "CaveGenerator", order = 101)]
	public class CaveDefaultGenerator : VoxelPlayDetailGenerator {

		struct HoleWorm {
			public Vector3 head;
			public int life;
			public uint lastX, lastY, lastZ;
			public float ax, ay, az;
		}

		List<HoleWorm> worms;

		VoxelPlayEnvironment env;
		Dictionary<Vector3, bool> wormBorn;
		float[] noiseValues;
		uint texSize;
		int caveBorder;

		/// <summary>
		/// Initialization method. Called by Voxel Play at startup.
		/// </summary>
		public override void Init() {
			env = VoxelPlayEnvironment.instance;
			wormBorn = new Dictionary<Vector3, bool>(100);
			texSize = 1024;
			noiseValues = new float[texSize];
			const int octaves = 4;
			for (int o = 1; o <= octaves; o++) {
				float v = 0;
				for (int k = 0; k < texSize; k++) {
					v += (Random.value - 0.5f) * o / 10f;
					noiseValues[k] = (noiseValues[k] + v) * 0.5f;
				}
			}
			// Clamp
			for (int k = 0; k < texSize; k++) {
				if (noiseValues[k] < -0.5f) noiseValues[k] = -0.5f;
				else if (noiseValues[k] > 0.5f) noiseValues[k] = 0.5f;
			}
			worms = new List<HoleWorm>(100);
		}


		/// <summary>
		/// Called by Voxel Play to inform that player has moved onto another chunk so new detail can start generating
		/// </summary>
		/// <param name="currentPosition">Current player position.</param>
		/// <param name="checkOnlyBorders">True means the player has moved to next chunk. False means player position is completely new and all chunks in
		/// range should be checked for detail in this call.</param>
		/// <param name="position">Position.</param>
		public override void ExploreArea(Vector3 position, bool checkOnlyBorders) {
            int explorationRange = env.visibleChunksDistance + 10;
			int minz = -explorationRange;
			int maxz = +explorationRange;
			int minx = -explorationRange;
			int maxx = +explorationRange;
			HoleWorm worm;
			Vector3 pos = position;
			for (int z = minz; z <= maxz; z++) {
				for (int x = minx; x < maxx; x++) {
					if (checkOnlyBorders && z > minz && z < maxz && x > minx && x < maxx) continue;
					pos.x = position.x + x * 16;
					pos.z = position.z + z * 16;
					pos = env.GetChunkPosition(pos);
					if (WorldRand.GetValue(pos) > 0.98f) {
						bool born;
						pos.y = env.GetTerrainHeight(pos);
						if (pos.y > env.waterLevel && !wormBorn.TryGetValue(pos, out born)) {
							if (!born) {
								worm.head = pos;
								worm.life = 2000;
								worm.lastX = worm.lastY = worm.lastZ = uint.MinValue;
								worm.ax = worm.ay = worm.az = 0;
								worms.Add(worm);
							}
							wormBorn[pos] = true;
						}
					}
				}
			}

			if (!checkOnlyBorders) {
				for (int k = 0; k < 1000; k++) {
					if (!DoWork(long.MaxValue)) break;
				}
			}
		}


		/// <summary>
		/// Move worms
		/// </summary>
		public override bool DoWork(long endTime) {

			int count = worms.Count;
			if (count == 0) return false;

			for (int k = 0; k < count; k++) {
				env.STAGE = 3000;
				HoleWorm worm = worms[k];
				uint xx = (uint)worm.head.x % texSize;
				worm.ax += noiseValues[xx];
				if (worm.ax > 1f) worm.ax = 1f;
				else if (worm.ax < -1f) worm.ax = -1f;
				worm.head.x += worm.ax;
                uint yy = (uint)worm.head.y % texSize;
				worm.ay += noiseValues[yy];
				if (worm.ay > 1f) worm.ay = 1f;
				else if (worm.ay < -1f) worm.ay = -1f;
				worm.head.y += worm.ay;
                uint zz = (uint)worm.head.z % texSize;
				worm.az += noiseValues[zz];
				if (worm.az > 1f) worm.az = 1f;
				else if (worm.az < -1f) worm.az = -1f;
				worm.head.z += worm.az;
				uint ix = (uint)worm.head.x;
				uint iy = (uint)worm.head.y;
				uint iz = (uint)worm.head.z;
				env.STAGE = 3001;
				if (ix != worm.lastX || iy != worm.lastY || iz != worm.lastZ) {
					worm.lastX = ix;
					worm.lastY = iy;
					worm.lastZ = iz;
					Vector3 lastChunkPos = Misc.vector3zero;
					int minx = -(caveBorder++ & 5);
					int miny = -(caveBorder++ & 5);
					int maxx = caveBorder++ & 5;
					int minz = -(caveBorder++ & 5);
					int maxy = caveBorder++ & 5;
					int maxz = caveBorder++ & 5;

					VoxelChunk chunk = null;
					Vector3 position;
					for (int y = miny; y < maxy; y++) {
						position.y = worm.head.y + y;
						for (int z = minz; z < maxz; z++) {
							position.z = worm.head.z + z;
							for (int x = minx; x < maxx; x++) {
								Vector3 chunkPos;
								int voxelIndex;
								position.x = worm.head.x + x;
								env.STAGE = 3002;
								env.GetVoxelIndex(position, out chunkPos, out voxelIndex);
								env.STAGE = 3003;
								if (chunkPos.x != lastChunkPos.x || chunkPos.y != lastChunkPos.y || chunkPos.z != lastChunkPos.z) {
									lastChunkPos = chunkPos;
									env.STAGE = 3004;
									chunk = env.GetChunkUnpopulated(position);
									env.STAGE = 3005;
									if (chunk.isPopulated) {
										worm.life = 0;
										y = maxy;
										z = maxz;
										break;
									}
								}
								env.STAGE = 3006;
								chunk.voxels[voxelIndex].hasContent = 2;
							}
						}
					}
					worm.life--;
					if (worm.life <= 0) {
						env.STAGE = 3007;
						worms.RemoveAt(k);
						env.STAGE = 0;
						return true;
					}
				}
				worms[k] = worm;

                    long elapsed = env.stopWatch.ElapsedMilliseconds;
                    if (elapsed >= endTime)
                        break;
                }
			env.STAGE = 0;
			return true;
		}

	}

}