﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelPlay {

	public enum RenderType : byte {
		Opaque = 0,
		Cutout = 1,
		Transparent = 2,
		CutoutCross = 3,
		OpaqueNoAO = 4,
		Custom = 5
	}

	public static class RenderTypeExtensions {
		public static bool supportsNavigation(this RenderType o) {
			return o == RenderType.Opaque || o == RenderType.Cutout || o == RenderType.OpaqueNoAO;
		}
		public static bool supportsWindAnimation(this RenderType o) {
			return o == RenderType.Cutout;
		}
	}

	[CreateAssetMenu (menuName = "Voxel Play/Voxel Definition", fileName = "VoxelDefinition", order = 101)]
	public class VoxelDefinition : ScriptableObject {
								
		public RenderType renderType = RenderType.Opaque;

		[Tooltip ("Texture of the voxel top side")]
		public Texture2D textureTop;

		[Tooltip ("Texture of voxel sides")]
		public Texture2D textureSide;

		[Tooltip ("Texture of the voxel bottom side")]
		public Texture2D textureBottom;

		[HideInInspector]
		public int textureIndexSide, textureIndexTop, textureIndexBottom;

		[Tooltip ("Sound played when voxel is picked up")]
		public AudioClip pickupSound;

		[Tooltip ("Sound produced when this voxel is placed in the scene")]
		public AudioClip buildSound;

		[Tooltip ("Sounds of the footsteps")]
		public AudioClip[] footfalls;

		[Tooltip ("Sound produced when player jumps from this voxel")]
		public AudioClip jumpSound;

		[Tooltip ("Sound produced after jumping and landing over this voxel")]
		public AudioClip landingSound;

		[Tooltip ("Voxel hit sound")]
		public AudioClip impactSound;

		[Tooltip ("Voxel destruction sound")]
		public AudioClip destructionSound;

		[Range (0, 255)]
		[Tooltip ("Resistance points. 0 means it cannot be hit. 255 means it's indestructible.")]
		public byte resistancePoints = 15;

		[NonSerialized, HideInInspector]
		public Color sampleColor;
		// used for vegetation particles; extracted dynamically from texture

		[Tooltip ("If this voxel type can be collected by the user after breaking it.")]
		public bool canBeCollected = true;

		[Tooltip ("If this voxel type can be included in NavMesh navigation. All opaque voxels are by default included in any generated NavMesh but you can exclude this voxel type by setting this property to false.")]
		public bool navigatable = true;

		[Tooltip ("Cutout voxel types can be animated if this property is set to true.")]
		public bool windAnimation = true;

		[Tooltip("The model to render in this voxel space.")]
		public GameObject model;

		[Tooltip("Optional displacement of the voxel.")]
		public Vector3 offset;

		[Tooltip("Optional scale of the voxel. Note that this scale is multiplied by the gameobject transform scale.")]
		public Vector3 scale = Misc.vector3one;

		[Tooltip("Optional rotation of the voxel in degrees.")]
		public Vector3 rotation;

		[Tooltip("Voxel definition to use when this voxel is placed twice on the same position.")]
		public VoxelDefinition promotesTo;

		[Tooltip("If this voxel propagates (eg. water)")]
		public bool spreads;

		[Tooltip("Delay for propagation in seconds")]
		public float spreadDelay;

		[Tooltip("Additional random delay for propagation")]
		public float spreadDelayRandom;

		[ColorUsage (true)]
		public Color diveColor = new Color (0, 0.41f, 0.63f, 0.83f);

		[Range(0,15), Tooltip("Default block level")]
		public byte height = 13;

		[Range(0,255), Tooltip("Damage caused to the player")]
		public byte playerDamage;

		[Range(0,255), Tooltip("Time interval in seconds between damage caused to the player")]
		public float playerDamageDelay = 2f;

		[NonSerialized]
		public VoxelDefinition dynamicDefinition;

		[NonSerialized]
		public short index; // index in voxelDefinitions list when it's added

	}

}