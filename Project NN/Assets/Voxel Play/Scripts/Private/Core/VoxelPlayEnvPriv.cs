﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.IO;
using UnityEngine;
using UnityEngine.Rendering;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace VoxelPlay {

	public delegate void VoxelHitEvent (VoxelChunk chunk, int voxelIndex, ref int damage);
	public delegate void VoxelEvent (VoxelChunk chunk, int voxelIndex);
	public delegate void VoxelDropItemEvent (VoxelChunk chunk, int voxelIndex, out bool canBeCollected);
	public delegate void VoxelClickEvent (VoxelChunk chunk, int voxelIndex, int buttonIndex);
	public delegate void VoxelChunkBeforeCreationEvent (Vector3 chunkCenter, out bool overrideDefaultContents, Voxel[] voxels, out bool isAboveSurface);
	public delegate void VoxelChunkEvent (VoxelChunk chunk);
	public delegate void VoxelTorchEvent (VoxelChunk chunk, LightSource lightSource);
	public delegate void VoxelPlayEvent ();
	public delegate void RepaintAction ();

	[ExecuteInEditMode]
	public partial class VoxelPlayEnvironment : MonoBehaviour {

		public delegate void AfterInitCallback ();

		[NonSerialized]
		public bool initialized, applicationIsPlaying;

		#if UNITY_EDITOR
		public event RepaintAction WantRepaintInspector;

		long lastCameraMoveTime, lastInspectorUpdateTime;
		#endif

		const string SKW_VOXELPLAY_USE_AO = "VOXELPLAY_USE_AO";
		const string SKW_VOXELPLAY_USE_TINTING = "VOXELPLAY_USE_TINTING";
		const string SKW_VOXELPLAY_GLOBAL_USE_FOG = "VOXELPLAY_GLOBAL_USE_FOG";
		const string SKW_VOXELPLAY_AA_TEXELS = "VOXELPLAY_USE_AA";

		[NonSerialized]
		public System.Diagnostics.Stopwatch stopWatch;
		Vector3 lastCamPos, currentCamPos;
		Quaternion lastCamRot, currentCamRot;
		bool _cameraHasMoved, shouldCheckChunksInFrustum;
		Material skyboxEarth, skyboxEarthSimplified, skyboxSpace, skyboxEarthNightCube, skyboxEarthDayNightCube, skyboxMaterial;
		Camera sceneCam;

		/// <summary>
		/// Stores the last message send to ShowMessage() method
		/// </summary>
		[NonSerialized]
		public string lastMessage = "";

		[NonSerialized]
		public bool isMobilePlatform;

		[NonSerialized]
		internal bool draftModeActive;

		[NonSerialized] public int STAGE = 0;

		List<VoxelChunk> tempChunks;



		#region Gameloop events

		void OnEnable () {
			#if UNITY_EDITOR
			CheckTintingScriptingSupport();
			#endif
			BootEngine ();
		}

		void Update () {
			if (initialized) {
				input.Update ();
			}
		}


		void LateUpdate () {
			if (applicationIsPlaying && initialized) {
				DoWork ();
				ProcessThreadMessages ();
			}
		}

		void OnDisable () {
			generationThreadRunning = false;
		}

		void OnDestroy () {
			#if UNITY_EDITOR
			EditorApplication.update -= UpdateInEditor;
			#endif
			DisposeAll ();
		}

		#endregion


		#region Initialization and disposal

		void BootEngine () {
			Init (MayLoadGame);
		}

		void MayLoadGame () {
			if (applicationIsPlaying || (!applicationIsPlaying && renderInEditor)) {
				if (loadSavedGame)
					LoadGameBinary (true);
			}
		}


		public void Init (AfterInitCallback callback = null) {
			initialized = false;
			sceneCam = null;
			applicationIsPlaying = Application.isPlaying;
			draftModeActive = !applicationIsPlaying && editorDraftMode;
			tempChunks = new List<VoxelChunk> ();
			InitMainThreading ();

#if UNITY_ANDROID || UNITY_IOS
			isMobilePlatform = true;
#elif UNITY_WEBGL
			isMobilePlatform = false;
			#if UNITY_EDITOR
			if (PlayerSettings.WebGL.memorySize<2000) PlayerSettings.WebGL.memorySize = 2000;
			#endif
#else
			isMobilePlatform = Application.isMobilePlatform;
			#endif

#if UNITY_WEBGL
            effectiveUseGeometryShaders = false;
            effectiveMultithreadGeneration = false;
#else
            effectiveUseGeometryShaders = useGeometryShaders && !isMobilePlatform && SystemInfo.graphicsDeviceType != UnityEngine.Rendering.GraphicsDeviceType.Metal;
            effectiveMultithreadGeneration = multiThreadGeneration;
#endif

			// Init camera and Sun references
			if (cameraMain == null) {
				cameraMain = Camera.main;
				if (cameraMain == null) {
					cameraMain = FindObjectOfType<Camera> ();
				}
				if (cameraMain == null) {
					Debug.LogError ("Voxel Play: No camera found!");
					return;
				}
			}

			#if UNITY_EDITOR
			if (cameraMain.actualRenderingPath != RenderingPath.Forward) {
				Debug.LogWarning ("Voxel Play works better with Forward Rendering path.");
			}
			if (!isMobilePlatform && QualitySettings.antiAliasing < 2) {
				Debug.LogWarning ("Voxel Play looks better with MSAA enabled (x2 minimum to enable crosshair).");
			}
			#endif

			if (isMobilePlatform && Application.isPlaying) {
				cameraMain.farClipPlane = Mathf.Min (400, _visibleChunksDistance * 16);
			}

			if (UICanvasPrefab == null) {
				UICanvasPrefab = Resources.Load<GameObject> ("VoxelPlay/UI/Voxel Play UI Canvas");
			}
			if (crosshairPrefab == null) {
				crosshairPrefab = Resources.Load<GameObject> ("VoxelPlay/UI/crosshair");
			}
			if (crosshairTexture == null) {
				crosshairTexture = Resources.Load<Texture2D> ("VoxelPlay/UI/crosshairTexture");
			}


			#if UNITY_EDITOR
			input = new KeyboardMouseController ();
			#else
			if (isMobilePlatform) {
			input = new DualTouchController ();
			} else {
			input = new KeyboardMouseController ();
			}
			#endif

			stopWatch = new System.Diagnostics.Stopwatch ();

			#if UNITY_EDITOR
			lastInspectorUpdateTime = 0;
			lastCameraMoveTime = 0;
			#endif

			if (!enableBuildMode)
				buildMode = false;

			if (applicationIsPlaying || (!applicationIsPlaying && renderInEditor)) {
				stopWatch.Start ();
				if (cachedChunks == null || chunksPool == null) {
					LoadWorldInt ();
					if (applicationIsPlaying) {
						StartCoroutine (WarmChunks (callback));
					} else {
						WarmChunksEditor (callback);
					}
				}
			}
		}

		IEnumerator WarmChunks (AfterInitCallback callback) {
			WaitForEndOfFrame w = new WaitForEndOfFrame ();
			int required = maxChunks;
#if UNITY_EDITOR
			required = prewarmChunksInEditor;
#endif

			while (chunksPoolLoadIndex < maxChunks) {
				for (int k = 0; k < 100; k++) {
					ReserveChunkMemory ();
				}
				if (enableLoadingPanel) {
					float progress = (float)(chunksPoolLoadIndex + 1) / required;
					VoxelPlayUI.instance.ToggleInitializationPanel (true, progress);
				}
				yield return w;
				if (chunksPoolLoadIndex > required)
					break;
			}

			InitEnd (callback);
		}

		void WarmChunksEditor (AfterInitCallback callback) {
			int required = 1000;
			while (chunksPoolLoadIndex < maxChunks) {
				for (int k = 0; k < 100; k++) {
					ReserveChunkMemory ();
				}
				if (chunksPoolLoadIndex > required)
					break;
			}
			InitEnd (callback);
		}



		void InitEnd (AfterInitCallback callback) {
			initialized = true;

			if (applicationIsPlaying) {
				GC.Collect ();
			}

			input.Init ();

			ComputeFirstReusableChunk ();

			if (applicationIsPlaying) {
				if (OnInitialized != null) {
					OnInitialized ();
				}
			}

			#if UNITY_EDITOR
			EditorApplication.update -= UpdateInEditor;
			if (renderInEditor && !applicationIsPlaying) {
				EditorApplication.update += UpdateInEditor;
			}
			#endif

			VoxelPlayUI.instance.ToggleInitializationPanel (false);

			ShowMessage (welcomeMessage, welcomeMessageDuration, true);

			if (callback != null) {
				callback ();
			}
		}

		/// <summary>
		/// Destroyes everything and initializes world
		/// </summary>
		void LoadWorldInt () {
			DisposeAll ();
			if (world == null)
				return;
			WorldRand.Randomize (world.seed);
			Physics.gravity = new Vector3 (0, world.gravity, 0);
			InitWater ();
			InitSky ();
			InitRenderer ();
			InitTrees ();
			InitVegetation ();
			LoadWorldTextures ();
			InitItems ();
			InitNavMesh ();
			InitChunkManager ();
			lastCamPos.y += 0.0001f; // forces check chunks in frustum
			InitClouds ();
			InitPhysics ();
			UpdateMaterialProperties ();
			SetBuildMode (buildMode);
		}


		#endregion

		#region Master rendering

		void DoWork () {
			try {
				STAGE = 1;
				CheckCamera ();

#if UNITY_EDITOR
				if (WantRepaintInspector != null) { // update inspector stats
					long elapsed = stopWatch.ElapsedMilliseconds - lastInspectorUpdateTime;
					if (elapsed > 1000) {
						WantRepaintInspector ();
						lastInspectorUpdateTime = stopWatch.ElapsedMilliseconds;
					}
				}
				if (!applicationIsPlaying) {
					if (renderInEditorLowPriority) {
						if (_cameraHasMoved) {
							lastCameraMoveTime = stopWatch.ElapsedMilliseconds;
						}
						long elapsed = stopWatch.ElapsedMilliseconds - lastCameraMoveTime;
						if (elapsed < 1000) {
							return;
						} else {
							// After 1 second of inactivity, rendering resumes but we need to inform that the camera probably has moved so the frustum planes get recalculated
							_cameraHasMoved = true;
							shouldCheckChunksInFrustum = true;
						}
					}
				}
#endif

				// main world creation & rendering cycle
				long currentTime = stopWatch.ElapsedMilliseconds;
				#if UNITY_EDITOR
				long availableTime = (!applicationIsPlaying && renderInEditorLowPriority) ? (long)(maxCPUTimePerFrame * 0.7f) : maxCPUTimePerFrame;
				long creationMaxTime = currentTime + (long)(availableTime * 0.8f); // Max 80% of frame time to populate stuff
				#else
				long availableTime = maxCPUTimePerFrame;
				long creationMaxTime = (long)(currentTime + availableTime * 0.8f);
				#endif

				if (applicationIsPlaying) {
					STAGE = 10;
					UpdateNavMesh ();
					DayCycle ();
					CheckSystemKeys ();

					if (requireTextureArrayUpdate) {
						STAGE = 11;
						LoadWorldTextures ();
					}
				}

				// Content generators - only process new chunks if there's no GPU upload or collider creation jobs
				if (meshJobMeshUploadIndex == meshJobMeshDataGenerationReadyIndex) {
					STAGE = 3;
					CheckChunksInRange (creationMaxTime);
					STAGE = 4;
					CheckTreeRequests (creationMaxTime);
					STAGE = 5;
					CheckVegetationRequests (creationMaxTime);
					STAGE = 21;
					DoDetailWork (creationMaxTime);
					if (!effectiveMultithreadGeneration) {
						STAGE = 22;
						GenerateChunkMeshDataInMainThread (currentTime + availableTime);
					}
					STAGE = 23;
					UpdateWaterFlood();
				}

				// Check which chunks need to be refreshed (either lightmap or content)
				STAGE = 9;
				CheckRenderChunkQueue (currentTime + availableTime);

				STAGE = 31;
				// Signal which chunk meshes need to be rebuilt, send them to generation thread and upload any ready mesh
				UpdateAndNotifyChunkChanges ();

				// After chunk mesh upload has completed, process any pending light update for particles
				UpdateParticlesLight ();

				// end cycle
			} catch (Exception ex) {
				ShowExceptionMessage (ex);
			}
			STAGE = 0;
		}

		void ShowExceptionMessage (Exception ex) {
			string msg = "<color=yellow>Critical (STAGE=" + STAGE + ")</color>: " + ex.Message + "\n" + ex.StackTrace;
			ShowMessage (msg);
			Debug.LogError (msg);
		}

		Camera currentCamera {
			get {
				if (applicationIsPlaying) {
					return cameraMain;
				}

#if UNITY_EDITOR
				// In Editor camera (SceneCam)
				if (sceneCam == null) {
					Camera[] cam = SceneView.GetAllSceneCameras ();
					if (cam != null) {
						for (int k = 0; k < cam.Length; k++) {
							if (cam [k] == Camera.current) {
								sceneCam = Camera.current;
								break;
							}
						}
					}
				}

#endif

				if (Camera.current == null) {
					if (sceneCam == null) {
						return cameraMain;
					} else {
						return sceneCam;
					}
				} else {
					return Camera.current;
				}
			}
		}

		void CheckCamera () {

			Camera cam = currentCamera;

			if (cam == null)
				return;

			currentCamPos = cam.transform.position;
			currentCamRot = cam.transform.rotation;
			_cameraHasMoved = (lastCamPos != currentCamPos || lastCamRot != currentCamRot);
			if (_cameraHasMoved) {
				lastCamPos = currentCamPos;
				lastCamRot = currentCamRot;
				if (frustumPlanes == null) {
					frustumPlanes = new Plane[6];
					frustumPlanesDistances = new float[6];
					frustumPlanesNormals = new Vector3[6];
				}
#if UNITY_IOS || UNITY_WEBGL
				#if UNITY_2017_3_OR_NEWER
                GeometryUtility.CalculateFrustumPlanes(cam.projectionMatrix * cam.worldToCameraMatrix, frustumPlanes);
				#else
				frustumPlanes = GeometryUtility.CalculateFrustumPlanes (cam.projectionMatrix * cam.worldToCameraMatrix);
				#endif
#else
                GeometryUtilityNonAlloc.CalculateFrustumPlanes (frustumPlanes, cam.projectionMatrix * cam.worldToCameraMatrix);
#endif
				for (int k = 0; k < 6; k++) {
					frustumPlanesDistances [k] = frustumPlanes [k].distance;
					frustumPlanesNormals [k] = frustumPlanes [k].normal;
				}

				if (frustumCorners == null || frustumCorners.Length != 4) {
					frustumCorners = new Vector3[4];
				}
				cam.CalculateFrustumCorners (Misc.rectFullViewport, cam.farClipPlane, Camera.MonoOrStereoscopicEye.Mono, frustumCorners);
				for (int i = 0; i < 4; i++) {
					frustumCorners [i] = cam.transform.position + cam.transform.TransformVector (frustumCorners [i]);
				}
				shouldCheckChunksInFrustum = true;
			}
		}

		void DayCycle () {
			if (world.dayCycleSpeed == 0 || sun == null)
				return;
			Transform t = sun.transform;
			if (applicationIsPlaying) {
				t.Rotate (new Vector3 (1f, 0.2f, 0) * Time.deltaTime * world.dayCycleSpeed);
			}
		}

		public void UpdateInEditor () {
			if (!applicationIsPlaying) {
				if (!initialized) {
					BootEngine ();
				}
				DoWork ();
			}
		}

		void UpdateParticlesLight () {
			if (particlePool == null)
				return;
			for (int k = 0; k < particlePool.Length; k++) {
				if (!particlePool [k].used)
					continue;
				Renderer renderer = particlePool [k].renderer;
				if (Time.time > particlePool [k].destructionTime || renderer == null) {
					ReleaseParticle (k);
					continue;
				}
				if (!globalIllumination)
					continue;
				Vector3 currentPos = renderer.transform.position;
				int cx = (int)currentPos.x;
				int cy = (int)currentPos.y;
				int cz = (int)currentPos.z;
				if (shouldUpdateParticlesLighting || cx != particlePool [k].lastX || cy != particlePool [k].lastY || cz != particlePool [k].lastZ) {
					float voxelLight = GetVoxelLight (currentPos);
					renderer.sharedMaterial.SetFloat ("_VoxelLight", voxelLight);
					particlePool [k].lastX = cx;
					particlePool [k].lastY = cy;
					particlePool [k].lastZ = cz;
				}
			}
			shouldUpdateParticlesLighting = false;
		}


		void ReleaseParticle (int k) {
			particlePool [k].used = false;
			if (particlePool [k].renderer != null) {
				particlePool [k].rigidBody.isKinematic = true;
				particlePool [k].renderer.enabled = false;
				particlePool [k].renderer.transform.position += new Vector3 (1000, 1000, 1000);
			}
			particlePool [k].lastX = int.MinValue;
		}

		#endregion


		#region Special keys handling

		void CheckSystemKeys () {
			if (enableConsole) {
				if (Input.GetKeyDown (KeyCode.F3)) {
					LoadGameBinary (false);
				} else if (Input.GetKeyDown (KeyCode.F4)) {
					SaveGameBinary ();
				}
			}
		}

		#endregion


		#region Editor helpers

		#if UNITY_EDITOR
		void CheckEditorTintColor () {
			if (!enableTinting) {
				Debug.Log ("Option enableTinting is disabled. To use colored voxels, please enable the option in the VoxelPlayEnvironment component inspector.");
			}
		}

		void CheckTintingScriptingSupport() {
			if (Application.isPlaying)
				return;

			// Do not execute if gameobject is prefab
			if (PrefabUtility.GetPrefabType(gameObject) == PrefabType.Prefab) return;
			if (PrefabUtility.GetCorrespondingObjectFromSource (gameObject) == null && PrefabUtility.GetPrefabObject (gameObject) != null)
				return;

			if ((Voxel.supportsTinting && !enableTinting) || (!Voxel.supportsTinting && enableTinting)) {
				UpdateTintingCodeMacro (enableTinting);
			}
		}

		public void UpdateTintingCodeMacro(bool enableTinting) {
			string[] res = Directory.GetFiles(Application.dataPath, "Voxel.cs", SearchOption.AllDirectories);
			string path = null;
			for (int k = 0; k < res.Length; k++) {
				if (res [k].Contains ("Voxel Play")) {
					path = res [k];
					break;
				}
			}
			if (path == null) {
				Debug.LogError ("Voxel.cs could not be found!");
				return;
			}

			string[] code = File.ReadAllLines (path);
			bool found = false;
			for (int k = 0; k < code.Length; k++) {
				if (code [k].Contains ("USES_TINTING")) {
					found = true;
					if (enableTinting) {
						code [k] = "#define USES_TINTING";
					} else {
						code [k] = "//#define USES_TINTING";
					}
					break;
				}
			}

			if (!found) {
				Debug.LogWarning ("USES_TINTING macro not found in Voxel.cs!");
				return;
			}

			File.WriteAllLines (path, code);
			AssetDatabase.Refresh ();
		}


		#endif

		#endregion

	}




}
