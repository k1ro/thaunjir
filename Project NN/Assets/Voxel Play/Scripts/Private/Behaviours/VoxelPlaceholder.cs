﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelPlay {

	public class VoxelPlaceholder : MonoBehaviour {

		[NonSerialized]
		public int resistancePointsLeft;

		[NonSerialized]
		public Renderer damageIndicator;

		[NonSerialized]
		public VoxelChunk chunk;

		[NonSerialized]
		public int voxelIndex;

		[NonSerialized]
		public GameObject modelTemplate;

		[NonSerialized]
		public GameObject modelInstance;

		[NonSerialized]
		public Bounds bounds;

		[NonSerialized]
		public MeshFilter modelMeshFilter;


		float recoveryTime;


		public void StartHealthRecovery (VoxelChunk chunk, int voxelIndex) {
			this.chunk = chunk;
			this.voxelIndex = voxelIndex;
			recoveryTime = Time.time + 3f;
			CancelInvoke ("Recover");
			Invoke ("Recover", 3.1f);
		}

		void Recover () {
			float time = Time.time;
			if (time >= recoveryTime) {
				if (chunk != null && chunk.voxels [voxelIndex].typeIndex != 0) {
					resistancePointsLeft = chunk.voxels [voxelIndex].type.resistancePoints;
				}
				if (damageIndicator != null) {
					damageIndicator.enabled = false;
				}
			}
		}

	}


}