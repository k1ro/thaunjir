﻿using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace VoxelPlay {


	public delegate bool OnVoxelSpreadBeforeEvent (VoxelDefinition voxeltype, Vector3 newPosition, VoxelDefinition voxelOnNewPosition);
	public delegate void OnVoxelSpreadAfterEvent (VoxelDefinition voxeltype, Vector3 newPosition);

	public partial class VoxelPlayEnvironment : MonoBehaviour {

		public OnVoxelSpreadBeforeEvent OnVoxelBeforeSpread;
		public OnVoxelSpreadAfterEvent OnVoxelAfterSpread;

		WaterFloodList waterFloodSources;

		void InitWater () {
			if (waterFloodSources == null) {
				waterFloodSources = new WaterFloodList ();
			} else {
				waterFloodSources.Clear ();
			}
		}


		/// <summary>
		/// Checks if there's water surrounding a given voxel. If it's, make that water expand.
		/// </summary>
		/// <param name="chunk">Chunk.</param>
		/// <param name="voxelIndex">Voxel index.</param>
		void MakeSurroundingWaterExpand (VoxelChunk chunk, int voxelIndex, Vector3 position, int lifeTime = 16) {
			Vector3 pos = position;
			VoxelChunk otherChunk;
			int otherVoxelIndex;

			for (int y = 0; y <= 1; y++) {
				pos.y = position.y + y;
				for (int z = -1; z <= 1; z++) {
					pos.z = position.z + z;
					for (int x = -1; x <= 1; x++) {
						if (x != 0 && z != 0)
							continue; // avoid water crossing corners
						if (x == 0 && y == 0 && z == 0)
							continue;
						pos.x = position.x + x;
						if (GetVoxelIndex (pos, out otherChunk, out otherVoxelIndex, false)) {
							byte otherWaterLevel = otherChunk.voxels [otherVoxelIndex].waterLevel;
							if (otherWaterLevel > 0) {
								AddWaterFlood (ref pos, voxelDefinitions[otherChunk.voxels [otherVoxelIndex].typeIndex], lifeTime);
							}
						}
					}
				}
			}
		}


		/// <summary>
		/// Start flooding at a given position
		/// </summary>
		/// <param name="position">Position.</param>
		void AddWaterFlood (ref Vector3 position, VoxelDefinition waterVoxel, int lifeTime = 16) {
			if (lifeTime > 0 && waterVoxel != null) {
				waterFloodSources.Add (ref position, waterVoxel, lifeTime);
			}
		}

		/// <summary>
		/// Manages flooding
		/// </summary>
		void UpdateWaterFlood () {
			int last = waterFloodSources.last;
			int index = waterFloodSources.root;
			tempChunks.Clear ();

			while (index != -1) {
				if (Time.time >= waterFloodSources.nodes [index].spreadNext) {
					waterFloodSources.SetNextSpreadTime (index);
					waterFloodSources.nodes [index].lifeTime--;
					if (waterFloodSources.nodes [index].lifeTime <= 0) {
						index = waterFloodSources.RemoveAt (index);
						continue;
					} else {
						TryWaterSpread (waterFloodSources.nodes [index].position, index);
						if (index == last)
							break;
					}
				}
				index = waterFloodSources.nodes [index].next;
			}

			int chunksToRefreshCount = tempChunks.Count;
			for (int k = 0; k < chunksToRefreshCount; k++) {
				VoxelChunk chunk = tempChunks [k];
				RefreshNineChunksMeshes (chunk);

				// Triggers event
				if (OnChunkChanged != null) {
					OnChunkChanged (chunk);
				}
			}
		}


		void RefreshNineChunksMeshes (VoxelChunk chunk) {
			Vector3 position = chunk.position;
			int chunkX = FastMath.FloorToInt (position.x / 16f);
			int chunkY = FastMath.FloorToInt (position.y / 16f);
			int chunkZ = FastMath.FloorToInt (position.z / 16f);

			VoxelChunk neighbour;
			for (int y = -1; y <= 1; y++) {
				for (int z = -1; z <= 1; z++) {
					for (int x = -1; x <= 1; x++) {
						if (y == 0 && x == 0 && z == 0)
							continue;
						GetChunkFast (chunkX + x, chunkY + y, chunkZ + z, out neighbour);
						if (neighbour != null && neighbour != chunk) {
							ChunkRequestRefresh (neighbour, false, true);
						}
					}
				}
			}
			ChunkRequestRefresh (chunk, false, true);
		}

		/// <summary>
		/// Updates water voxel at a given position. 
		/// </summary>
		/// <returns>Returns the affected chunk and voxel index</returns>
		/// <param name="position">Position.</param>
		/// <param name="voxel">Voxel.</param>
		void WaterUpdateLevelFast (VoxelChunk chunk, int voxelIndex, byte waterLevel, VoxelDefinition vd) {
			if (waterLevel > 15)
				waterLevel = 15;
			if (chunk.voxels [voxelIndex].waterLevel == waterLevel)
				return;
			if (waterLevel == 0) {
				chunk.voxels [voxelIndex].Clear (chunk.voxels [voxelIndex].light);
			} else {
				if (chunk.voxels [voxelIndex].waterLevel == 0) {
					chunk.voxels [voxelIndex].Set (vd, Misc.color32White);
				}
				chunk.voxels [voxelIndex].waterLevel = waterLevel;
			}

			chunk.modified = true;

		}

		/// <summary>
		/// Check surrounding voxels - if one if empty add one water voxel and extend flood
		/// </summary>
		/// <returns>The water spread.</returns>
		/// <param name="waterPos">Water position.</param>
		void TryWaterSpread (Vector3 waterPos, int index) {

			VoxelChunk chunk;
			int voxelIndex;
			GetVoxelIndex (waterPos, out chunk, out voxelIndex, false);
			if (chunk == null || chunk.voxels [voxelIndex].hasContent != 1) {
				waterFloodSources.RemoveAt (index);
			}
			
			VoxelChunk otherChunk;
			int vIndex;
			byte waterLevel = chunk.voxels [voxelIndex].waterLevel;
			if (waterLevel == 0) {
				waterFloodSources.RemoveAt (index);
				MakeSurroundingWaterExpand (chunk, voxelIndex, waterPos);
				return;
			}

			// Check first beneath, if empty water falls down
			Vector3 down = new Vector3 (waterPos.x, waterPos.y - 1f, waterPos.z);
			if (GetVoxelIndex (down, out otherChunk, out vIndex)) {
				VoxelDefinition vdOther = voxelDefinitions[otherChunk.voxels [vIndex].typeIndex];
				bool canFillWithWater = otherChunk.voxels [vIndex].hasContent != 1 || vdOther.renderType == RenderType.CutoutCross || vdOther.renderType == RenderType.Transparent;
				if (canFillWithWater) {
					byte otherWaterLevel = otherChunk.voxels [vIndex].waterLevel;
					if (otherWaterLevel < 15) {
						if (OnVoxelBeforeSpread != null && !OnVoxelBeforeSpread (chunk.voxels [voxelIndex].type, down, otherChunk.voxels [vIndex].type)) {
							return;
						}
						otherWaterLevel++;
						WaterUpdateLevelFast (otherChunk, vIndex, otherWaterLevel, waterFloodSources.nodes [index].waterVoxel);
						waterLevel--;
						WaterUpdateLevelFast (chunk, voxelIndex, waterLevel, waterFloodSources.nodes [index].waterVoxel);
						if (waterLevel == 0) {
							waterFloodSources.RemoveAt (index);
							MakeSurroundingWaterExpand (chunk, voxelIndex, waterPos);
						} else {
							AddWaterFlood (ref down, waterFloodSources.nodes [index].waterVoxel);
						}
						if (!tempChunks.Contains (chunk))
							tempChunks.Add (chunk);
						if (OnVoxelAfterSpread != null) {
							OnVoxelAfterSpread (chunk.voxels [voxelIndex].type, down);
						}
						return;
					}
				}
			}

			// If not, try to flood horizontally
			Vector3 otherPos = waterPos;
			byte prevWaterLevel = waterLevel;
			for (int z = -1; z <= 1; z++) {
				otherPos.z = waterPos.z + z;
				for (int x = -1; x <= 1; x++) {
					// avoid water crossing corners
					if ((x != 0 && z != 0) || (x == 0 && z == 0))
						continue;
					otherPos.x = waterPos.x + x;
					if (GetVoxelIndex (otherPos, out otherChunk, out vIndex, false)) {
						VoxelDefinition vdOther = voxelDefinitions[otherChunk.voxels [vIndex].typeIndex];
						if (otherChunk.voxels [vIndex].hasContent != 1 || vdOther.renderType == RenderType.CutoutCross || vdOther.renderType == RenderType.Transparent) {
							byte otherWaterLevel = otherChunk.voxels [vIndex].waterLevel;
							if (otherWaterLevel < waterLevel) {
								if (otherWaterLevel < waterLevel - 1) {
									waterLevel--;
								}
								if (waterLevel > 1) {
									if (OnVoxelBeforeSpread != null && !OnVoxelBeforeSpread (chunk.voxels [voxelIndex].type, otherPos, otherChunk.voxels [vIndex].type)) {
										continue;
									}
									otherWaterLevel++;
									AddWaterFlood (ref otherPos, waterFloodSources.nodes [index].waterVoxel, waterFloodSources.nodes [index].lifeTime - 1);
									WaterUpdateLevelFast (otherChunk, vIndex, otherWaterLevel, waterFloodSources.nodes [index].waterVoxel);
									if (!tempChunks.Contains (otherChunk)) {
										tempChunks.Add (otherChunk);
									}
									if (OnVoxelAfterSpread != null) {
										OnVoxelAfterSpread (chunk.voxels [voxelIndex].type, otherPos);
									}
								}
							} else if (otherWaterLevel > waterLevel) {
								otherWaterLevel--;
								vdOther = voxelDefinitions [otherChunk.voxels [voxelIndex].typeIndex];
								AddWaterFlood (ref otherPos, vdOther, waterFloodSources.nodes [index].lifeTime - 1);
								WaterUpdateLevelFast (otherChunk, vIndex, otherWaterLevel, vdOther);
								if (!tempChunks.Contains (otherChunk)) {
									tempChunks.Add (otherChunk);
								}
								if (otherWaterLevel > waterLevel) {
									if (OnVoxelBeforeSpread != null && !OnVoxelBeforeSpread (otherChunk.voxels [voxelIndex].type, otherPos, chunk.voxels [voxelIndex].type)) {
										continue;
									}
									waterLevel++;
									if (OnVoxelAfterSpread != null) {
										OnVoxelAfterSpread (otherChunk.voxels [voxelIndex].type, otherPos);
									}
								}
							}
						}
					}
				}
			}
			if (prevWaterLevel != waterLevel) {
				WaterUpdateLevelFast (chunk, voxelIndex, waterLevel, waterFloodSources.nodes [index].waterVoxel);
				if (!tempChunks.Contains (chunk)) {
					tempChunks.Add (chunk);
				}
			}
			if (waterLevel == 0 || prevWaterLevel == waterLevel) {
				waterFloodSources.RemoveAt (index);
				MakeSurroundingWaterExpand (chunk, voxelIndex, waterPos, waterFloodSources.nodes [index].lifeTime - 1);
			}
		}

					
	}



}
