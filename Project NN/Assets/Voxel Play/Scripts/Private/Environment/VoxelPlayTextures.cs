﻿using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace VoxelPlay {

	public partial class VoxelPlayEnvironment : MonoBehaviour {

		/// <summary>
		/// List containing all world textures availables
		/// </summary>
		List<Texture2D> worldTextures;

		/// <summary>
		/// Dictionary lookup for the voxel definition by name
		/// </summary>
		Dictionary<string, VoxelDefinition> voxelDefinitionsDict;

		/// <summary>
		/// Set to true if the texture array needs to be recreated (ie. new voxel definitions have been added)
		/// </summary>
		bool requireTextureArrayUpdate;

		/// <summary>
		/// Temporary/session voxels added by users at runtime
		/// </summary>
		List<VoxelDefinition> sessionUserVoxels;

			
		void AddVoxelTextures (VoxelDefinition vd) {

			if (vd == null)
				return;

			if (vd.index > 0)
				return; // already added

			// Resize voxel definitions array?
			if (voxelDefinitionsCount >= voxelDefinitions.Length) {
				VoxelDefinition[] resized = new VoxelDefinition[voxelDefinitions.Length * 2];
				for (int k = 0; k < voxelDefinitionsCount; k++) {
					resized [k] = voxelDefinitions [k];
				}
				voxelDefinitions = resized;
			}

			voxelDefinitions [voxelDefinitionsCount] = vd;
			vd.index = (short)voxelDefinitionsCount;
			voxelDefinitionsCount++;
			voxelDefinitionsDict [vd.name] = vd;

			int index = 0;
			if (vd.textureBottom != null) {
				index = worldTextures.IndexOf (vd.textureBottom);
				if (index < 0) {
					worldTextures.Add (vd.textureBottom);
					index = worldTextures.Count - 1;
				}
				vd.textureIndexBottom = index;
			}
			if (vd.textureSide != null) {
				if (vd.textureSide != vd.textureBottom) {
					index = worldTextures.IndexOf (vd.textureSide);
					if (index < 0) {
						worldTextures.Add (vd.textureSide);
						index = worldTextures.Count - 1;
					}
				}
				vd.textureIndexSide = index;
			}
			if (vd.textureTop != null) {
				if (vd.textureTop != vd.textureSide) {
					index = worldTextures.IndexOf (vd.textureTop);
					if (index < 0) {
						worldTextures.Add (vd.textureTop);
						index = worldTextures.Count - 1;
					}
				}
				vd.textureIndexTop = index;
			}

			if (vd.renderType == RenderType.CutoutCross && vd.sampleColor.a == 0) {
				GetSampleTextureColor (vd, vd.textureSide);
			}
		}

		void GetSampleTextureColor (VoxelDefinition voxel, Texture2D tex) {
			Color[] colors = tex.GetPixels ();
			int pos = 4 * tex.width + tex.width * 3 / 4;
			if (pos >= colors.Length)
				pos = colors.Length - 1;
			for (int k = pos; k > 0; k--) {
				if (colors [k].a > 0.5f) {
					voxel.sampleColor = colors [k];
					return;
				}
			}
		}


		void LoadWorldTextures () {

			requireTextureArrayUpdate = false;

			// Load textures
			if (worldTextures == null) {
				worldTextures = new List<Texture2D> ();
			} else {
				worldTextures.Clear ();
			}
			if (voxelDefinitions == null) {
				voxelDefinitions = new VoxelDefinition[128];
			}
			voxelDefinitionsCount = 0;
			if (voxelDefinitionsDict == null) {
				voxelDefinitionsDict = new Dictionary<string, VoxelDefinition> ();
			} else {
				voxelDefinitionsDict.Clear ();
			}
			if (sessionUserVoxels == null) {
				sessionUserVoxels = new List<VoxelDefinition> ();
			}
			// The null voxel definition
			VoxelDefinition nullVoxelDefinition = ScriptableObject.CreateInstance<VoxelDefinition>();
			nullVoxelDefinition.canBeCollected = false;
			nullVoxelDefinition.name = "Null";
			AddVoxelTextures (nullVoxelDefinition);

			// Check default voxel
			if (defaultVoxel == null) {
				defaultVoxel = Resources.Load<VoxelDefinition> ("VoxelPlay/Defaults/DefaultVoxel");
			}
			AddVoxelTextures (defaultVoxel);

			// Add all biome textures
			for (int k = 0; k < world.biomes.Length; k++) {
				BiomeDefinition biome = world.biomes [k];
				AddVoxelTextures (biome.voxelTop);
				AddVoxelTextures (biome.voxelDirt);
				if (biome.vegetation != null) {
					for (int v = 0; v < biome.vegetation.Length; v++) {
						AddVoxelTextures (biome.vegetation [v].vegetation);
					}
				}
				if (biome.trees != null) {
					for (int t = 0; t < biome.trees.Length; t++) {
						ModelDefinition tree = biome.trees [t].tree;
						if (tree == null)
							continue;
						for (int b = 0; b < tree.bits.Length; b++) {
							AddVoxelTextures (tree.bits [b].voxelDefinition);
						}
					}
				}
			}

			// Special voxels
			if (world.cloudVoxel == null) {
				world.cloudVoxel = Resources.Load<VoxelDefinition> ("VoxelPlay/Defaults/VoxelCloud");
			}
			AddVoxelTextures (world.cloudVoxel);
			AddVoxelTextures (world.bedrockVoxel);

			// Add additional world voxels
			for (int k = 0; k < world.moreVoxels.Length; k++) {
				AddVoxelTextures (world.moreVoxels [k]);
			}

			// Add any other voxel found inside Defaults
			VoxelDefinition[] vdd = Resources.LoadAll<VoxelDefinition> ("VoxelPlay/Defaults");
			for (int k = 0; k < vdd.Length; k++) {
				AddVoxelTextures (vdd [k]);
			}

			// Add any other voxel found inside World directory
			vdd = Resources.LoadAll<VoxelDefinition> ("Worlds/" + world.name);
			for (int k = 0; k < vdd.Length; k++) {
				AddVoxelTextures (vdd [k]);
			}

			// Add any other voxel found inside a resource directory with same name of world
			vdd = Resources.LoadAll<VoxelDefinition> (world.name);
			for (int k = 0; k < vdd.Length; k++) {
				AddVoxelTextures (vdd [k]);
			}

			// Add user provided voxels during playtime
			int count = sessionUserVoxels.Count;
			for (int k = 0; k < count; k++) {
				AddVoxelTextures (sessionUserVoxels [k]);
			}

			int textureCount = worldTextures.Count;
			if (textureCount > 0) {
				Texture2DArray pointFilterTextureArray = new Texture2DArray (textureSize, textureSize, textureCount, TextureFormat.ARGB32, hqFiltering);
				pointFilterTextureArray.wrapMode = TextureWrapMode.Clamp; // required to avoid artifacts on borders
				pointFilterTextureArray.filterMode = hqFiltering ? FilterMode.Bilinear : FilterMode.Point;
				pointFilterTextureArray.mipMapBias = -mipMapBias;
				for (int k = 0; k < textureCount; k++) {
					if (worldTextures [k].width != textureSize || worldTextures [k].height != textureSize) {
						worldTextures [k] = Instantiate (worldTextures [k]) as Texture2D;
						worldTextures [k].hideFlags = HideFlags.DontSave;
						TextureTools.Scale (worldTextures [k], textureSize, textureSize, FilterMode.Point);
					}
					Color32[] colors = worldTextures [k].GetPixels32 ();
					pointFilterTextureArray.SetPixels32 (colors, k); // 0 is for empty texture
				}
				pointFilterTextureArray.Apply (hqFiltering, true);
				if (matTerrainOpaque != null) {
					matTerrainOpaque.SetTexture ("_MainTex", pointFilterTextureArray);
				}
				if (matTerrainCutout != null) {
					matTerrainCutout.SetTexture ("_MainTex", pointFilterTextureArray);
				}
				if (matTerrainWater != null) {
					matTerrainWater.SetTexture ("_MainTex", pointFilterTextureArray);
				}
				if (matTerrainCutxss != null) {
					matTerrainCutxss.SetTexture ("_MainTex", pointFilterTextureArray);
				}
				if (matTerrainOpNoAO != null) {
					matTerrainOpNoAO.SetTexture ("_MainTex", pointFilterTextureArray);
				}
				if (matTriangleOpaque != null) {
					matTriangleOpaque.SetTexture ("_MainTex", pointFilterTextureArray);
				}
				if (matTriangleCutout != null) {
					matTriangleCutout.SetTexture ("_MainTex", pointFilterTextureArray);
				}
			}
		}

		VoxelDefinition GetVoxelDefinitionByName (string name) {
			VoxelDefinition vd;
			voxelDefinitionsDict.TryGetValue (name, out vd);
			return vd;
		}
				

	}


}
