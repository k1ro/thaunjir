﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.Rendering;

namespace VoxelPlay {

	public partial class VoxelPlayEnvironment : MonoBehaviour {

		const byte FULL_OPAQUE = 15;
		[NonSerialized]
		public VoxelDefinition[] voxelDefinitions;
		public int voxelDefinitionsCount;

		List<VoxelChunk> voxelPlaceFastAffectedChunks;


		#region Voxel functions

		void VoxelDestroyFast (VoxelChunk chunk, int voxelIndex) {

			// Ensure there's content on this position
			if (chunk.voxels [voxelIndex].hasContent != 1)
				return;

			if (OnVoxelBeforeDestroyed != null) {
				OnVoxelBeforeDestroyed (chunk, voxelIndex);
			}

			// 1) Remove placeholder if exists
			VoxelPlaceholderDestroy (chunk, voxelIndex);

			// 2) Clears voxel
			chunk.voxels [voxelIndex].Clear (effectiveGlobalIllumination ? (byte)0 : (byte)15);

			chunk.modified = true;

			// Update lightmap and renderers
			RefreshNineChunks (chunk);

			// Force rebuild neighbour meshes if destroyed voxel is on a border
			int px, py, pz;
			GetVoxelChunkCoordinates (voxelIndex, out px, out py, out pz);
			if (px == 0 && chunk.left != null)
				ChunkRequestRefresh (chunk.left, false, true);
			else if (px == 15 && chunk.right != null)
				ChunkRequestRefresh (chunk.right, false, true);
			if (py == 0 && chunk.bottom != null)
				ChunkRequestRefresh (chunk.bottom, false, true);
			else if (py == 15 && chunk.top != null)
				ChunkRequestRefresh (chunk.top, false, true);
			if (pz == 0 && chunk.back != null)
				ChunkRequestRefresh (chunk.back, false, true);
			else if (pz == 15 && chunk.forward != null)
				ChunkRequestRefresh (chunk.forward, false, true);

			if (OnChunkChanged != null) {
				OnChunkChanged (chunk);
			}
			if (OnVoxelDestroyed != null) {
				OnVoxelDestroyed (chunk, voxelIndex);
			}

			// Check if it was surrounded by water. If it was, add water expander
			MakeSurroundingWaterExpand (chunk, voxelIndex, GetVoxelPosition (chunk, voxelIndex));

		}


		/// <summary>
		/// Puts a voxel in the given position. Takes care of informing neighbour chunks.
		/// </summary>
		/// <returns>Returns the affected chunk and voxel index</returns>
		/// <param name="position">Position.</param>
		/// <param name="voxel">Voxel.</param>
		void VoxelPlaceFast (Vector3 position, VoxelDefinition voxelType, out VoxelChunk chunk, out int voxelIndex, Color32 tintColor) {
			VoxelSingleSet (position, voxelType, out chunk, out voxelIndex, tintColor);

			// If it's water, add flood
			if (voxelType.spreads) {
				chunk.voxels [voxelIndex].waterLevel = 15;
				AddWaterFlood (ref position, voxelType);
			}

			chunk.modified = true;

			// Update neighbours
			RefreshNineChunks (chunk);

			// Triggers event
			if (OnChunkChanged != null) {
				OnChunkChanged (chunk);
			}
		}


		/// <summary>
		/// Puts lots of voxels in the given positions. Takes care of informing neighbour chunks.
		/// </summary>
		void VoxelPlaceFast (List<Vector3> positions, VoxelDefinition voxelType, List<VoxelChunk> chunks) {

			VoxelChunk chunk, lastChunk = null;
			int voxelIndex;
			int count = positions.Count;

			if (voxelPlaceFastAffectedChunks == null) {
				voxelPlaceFastAffectedChunks = new List<VoxelChunk> ();
			} else {
				voxelPlaceFastAffectedChunks.Clear ();
			}

			for (int k = 0; k < count; k++) {
				VoxelSingleSet (positions [k], voxelType, out chunk, out voxelIndex, Misc.color32White);
				if (lastChunk != chunk) {
					lastChunk = chunk;
					if (!voxelPlaceFastAffectedChunks.Contains (chunk)) {
						voxelPlaceFastAffectedChunks.Add (chunk);
					}
				}
			}

			int mainChunksCount = voxelPlaceFastAffectedChunks.Count;
			for (int k = 0; k < mainChunksCount; k++) {
				ChunkRequestRefresh (voxelPlaceFastAffectedChunks [k], true, true);
				// Triggers event
				if (OnChunkChanged != null) {
					OnChunkChanged (voxelPlaceFastAffectedChunks [k]);
				}
			}

			if (chunks != null) {
				chunks.AddRange (voxelPlaceFastAffectedChunks);
			}
		}

		/// <summary>
		/// Places a model inside a given chunk replacing it completely
		/// </summary>
		/// <param name="">.</param>
		void VoxelPlaceModel (VoxelChunk chunk, ModelDefinition model) {

			for (int k = 0; k < model.bits.Length; k++) {
				int voxelIndex = model.bits [k].voxelIndex;
				VoxelDefinition voxelType = model.bits [k].voxelDefinition ?? defaultVoxel;
				chunk.voxels [voxelIndex].Set (voxelType, Misc.color32White);
			}

			UpdateChunkRR (chunk);

			// Triggers event
			if (OnChunkChanged != null) {
				OnChunkChanged (chunk);
			}
		}

		/// <summary>
		/// Internal method that puts a voxel in a given position. This method does not inform to neighbours. Only used by non-contiguous structures, like trees or vegetation.
		/// For terrain or large scale buildings, use VoxelPlaceFast.
		/// </summary>
		/// <param name="position">Position.</param>
		/// <param name="voxelType">Voxel type.</param>
		/// <param name="chunk">Chunk.</param>
		/// <param name="voxelIndex">Voxel index.</param>
		/// <returns>True if voxel was places</returns>
		void VoxelSingleSet (Vector3 position, VoxelDefinition voxelType, out VoxelChunk chunk, out int voxelIndex) {
			VoxelSingleSet (position, voxelType, out chunk, out voxelIndex, Misc.color32White);
		}

		/// <summary>
		/// Internal method that puts a voxel in a given position. This method does not inform to neighbours. Only used by non-contiguous structures, like trees or vegetation.
		/// For terrain or large scale buildings, use VoxelPlaceFast.
		/// </summary>
		/// <param name="position">Position.</param>
		/// <param name="voxelType">Voxel type.</param>
		/// <param name="chunk">Chunk.</param>
		/// <param name="voxelIndex">Voxel index.</param>
		/// <returns>True if voxel was places</returns>
		void VoxelSingleSet (Vector3 position, VoxelDefinition voxelType, out VoxelChunk chunk, out int voxelIndex, Color32 tintColor) {
			if (GetVoxelIndex (position, out chunk, out voxelIndex)) {
				chunk.voxels [voxelIndex].Set (voxelType, tintColor);
			}
		}



		#endregion

	}



}
