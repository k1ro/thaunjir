﻿using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.Rendering;
using System;
using System.IO;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;

namespace VoxelPlay {

	public partial class VoxelPlayEnvironment : MonoBehaviour {

		/// <summary>
		/// True if the current game has been loaded from a savefile.
		/// </summary>
		[NonSerialized]
		public bool saveFileIsLoaded;


		const byte SAVE_FILE_CURRENT_FORMAT = 3;
		bool isLoadingGame;

		public bool LoadGameBinary (bool firstLoad, bool preservePlayerPosition = false) {

			saveFileIsLoaded = false;

			if (firstLoad) {
				if (string.IsNullOrEmpty (saveFilename))
					return false;
			} else {
				// If LoadGame is called during a game, initializes everything first
				DestroyAllVoxels ();
				if (!CheckGameFilename ())
					return false;
			}

			bool result = true;
			try {
				byte[] saveGameData = GetSaveGameDataBinary ();
				if (saveGameData == null) {
					return false;
				}

				// get version
				isLoadingGame = true;
				BinaryReader br = new BinaryReader (new MemoryStream (saveGameData), Encoding.UTF8);
				int version = br.ReadByte ();
				switch (version) {
				case 3: 
					LoadSaveBinaryFileFormat_3 (br);
					break;
				default:
					throw new ApplicationException ("LoadGame() does not support this file format.");
				}
				br.Close ();
				isLoadingGame = false;
				saveFileIsLoaded = true;
				if (!firstLoad) {
					VoxelPlayUI.instance.ToggleConsoleVisibility (false);
					ShowMessage ("<color=green>Game loaded successfully!</color>");
				}
				if (OnGameLoaded != null) {
					OnGameLoaded ();
				}
			} catch (Exception ex) {
				ShowMessage ("<color=red>Load error:</color> <color=orange>" + ex.Message + "</color><color=white>" + ex.StackTrace + "</color>");
				result = false;
			}

			isLoadingGame = false;
			return result;
		}

		string GetFullFilename () {
			#if UNITY_EDITOR
			string path = AssetDatabase.GetAssetPath (world);
			path = Path.GetDirectoryName (path) + "/SavedGames";
			Directory.CreateDirectory (path);
			path += "/" + saveFilename + ".txt";
			return path;
			#else
												string path = Application.persistentDataPath + "/VoxelPlay";
												Directory.CreateDirectory (path);
												string fullName = path + "/" + saveFilename + ".txt";
												return fullName;
			#endif
		}


		byte[] GetSaveGameDataBinary () {
			#if UNITY_EDITOR
			// In Editor, always load saved game from Resources/Worlds/<name of world>/SavedGames folder
			string path = AssetDatabase.GetAssetPath (world);
			path = Path.GetDirectoryName (path) + "/SavedGames/" + saveFilename + ".txt";
			if (File.Exists (path)) {
				return File.ReadAllBytes (path);
			} else {
				return null;
			}
			#else
												// In Build, try to load the saved game from application data path. If there's none, try to load a default saved game from Resources.
												string path = Application.persistentDataPath + "/VoxelPlay/" + saveFilename + ".txt";
												if (File.Exists(path)) {
												return File.ReadAllBytes(path);

												} else {
																string resource = "VoxelPlay/Worlds/" + world.name + "/SavedGames/" + saveFilename;
												TextAsset ta = Resources.Load<TextAsset>(resource);
												if (ta!=null) {
												return ta.bytes;
												} else {
												return null;
												}
												}
			#endif
		}


		bool CheckGameFilename () {
			if (string.IsNullOrEmpty (saveFilename)) {
				ShowMessage ("<color=orange>Set a file name for the game to load/save first.</color>");
				return false;
			}
			return true;
		}

		public bool SaveGameBinary () {
			if (!CheckGameFilename ())
				return false;

			bool success = true;
			try {
				string filename = GetFullFilename ();
				FileStream fs = new FileStream (filename, FileMode.Create);
				BinaryWriter bw = new BinaryWriter (fs);
				SaveGameBinaryFormat_3 (bw);
				bw.Close ();
				fs.Close ();
				ShowMessage ("<color=green>Game saved successfully in </color><color=yellow>" + filename + "</color>");
			} catch (Exception ex) {
				ShowMessage ("<color=red>Error:</color> <color=orange>" + ex.Message + "</color>");
				success = false;
			}
			return success;
		}

		/// <summary>
		/// Returns the world encoded in a string
		/// </summary>
		/// <returns>The game to text.</returns>
		public byte[] SaveGameToByteArray () {
			MemoryStream ms = new MemoryStream ();
			BinaryWriter bw = new BinaryWriter (ms);
			SaveGameBinaryFormat_3 (bw);
			bw.Close ();
			return ms.ToArray ();
		}

		/// <summary>
		/// Loads game world from a string
		/// </summary>
		/// <returns>True if saveGameData was loaded successfully.</returns>
		/// <param name="preservePlayerPosition">If set to <c>true</c> preserve player position.</param>
		public bool LoadGameFromByteArray (byte[] saveGameData, bool preservePlayerPosition) {

			bool result = false;
			DestroyAllVoxels ();

			try {
				if (saveGameData == null) {
					return false;
				}

				// get version
				isLoadingGame = true;
				BinaryReader br = new BinaryReader (new MemoryStream (saveGameData), Encoding.UTF8);
				byte version = br.ReadByte ();
				switch (version) {
				case 3:
					LoadSaveBinaryFileFormat_3 (br, preservePlayerPosition);
					break;
				default:
					throw new ApplicationException ("LoadGameFromArray() does not support this file format.");
				}
				br.Close ();
				isLoadingGame = false;
				saveFileIsLoaded = true;
				result = true;
			} catch (Exception ex) {
				Debug.LogError ("Voxel Play: " + ex.Message);
				result = false;
			}

			isLoadingGame = false;
			return result;

		}


	}



}
