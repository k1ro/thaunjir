//#define USES_TINTING

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelPlay {

	public struct Voxel {

		/// <summary>
		/// Voxel definition index in voxelDefinitions list
		/// </summary>
		public short typeIndex;

		public bool isEmpty {
			get { return typeIndex == 0; }
		}

		public VoxelDefinition type {
			get {
				return VoxelPlayEnvironment.instance.voxelDefinitions[typeIndex];
			}
		}

		/// <summary>
		/// If this voxel has content. 1 = has content, 0 or other = empty, 2 = hole (temporarily used by cave generator)
		/// </summary>
		public byte hasContent;

		/// <summary>
		/// If this voxel cast ambient occlusion, occludes = 1
		/// </summary>
		public byte occludes;

		/// <summary>
		/// If this voxel lets light pass through, opaque = 0, otherwise this is the light intensity reduction factor
		/// </summary>
		public byte opaque;

		/// <summary>
		/// Current light value of this voxel. Light is the intensity of light that crosses the voxel.
		/// This value will fluctuate during a lightmap update. Use lightMesh to get the last light value sent to the mesh vertices.
		/// </summary>
		public byte light;

		#if USES_TINTING
		public byte red, green, blue;
		#else
		public byte red { get { return 255; } }
		public byte green { get { return 255; } }
		public byte blue { get { return 255; } }
		#endif

		/// <summary>
		/// Returns the 
		/// </summary>
		/// <value>The color of the tint.</value>
		public Color32 color {
			get {
				return new Color32 (red, green, blue, 255);
			}
			set {
				#if USES_TINTING
				red = value.r;
				green = value.g;
				blue = value.b;
				#endif
			}
		}

		/// <summary>
		/// Returns true if this voxel has a custom color set
		/// </summary>
		public bool isColored {
			get {
				return red != 255 || green != 255 || blue != 255;
			}
		}

		/// <summary>
		/// Last light value sent to the mesh vertex. This is the real visible light intensity.
		/// </summary>
		public byte lightMesh;

		/// <summary>
		/// The amount of water in this voxel (0-15)
		/// </summary>
		/// <value>The height.</value>
		public byte waterLevel;


		public Voxel (VoxelDefinition type) {
			if ((object)type != null) {
				this.hasContent = (byte)1;
				switch (type.renderType) {
				case RenderType.Opaque:
					this.opaque = 15;
					this.occludes = 1;
					this.waterLevel = 0;
					break;
				case RenderType.Cutout:
					this.opaque = 2;
					this.occludes = 1;
					this.waterLevel = 0;
					break;
				case RenderType.Transparent:
					this.opaque = 2;
					this.occludes = 0;
					this.waterLevel = 15;
					break;
				case RenderType.OpaqueNoAO:
					this.opaque = 15;
					this.occludes = 1;
					this.waterLevel = 0;
					break;
				default:
					this.opaque = 0;
					this.occludes = 0;
					this.waterLevel = 0;
					break;
				}
				this.typeIndex = (byte)type.index;
			} else {
				this.hasContent = (byte)0;
				this.opaque = (byte)0;
				this.occludes = 0;
				this.typeIndex = 0;
				this.waterLevel = 0;
			}
			this.light = 0;
			this.lightMesh = 0;

			#if USES_TINTING
			this.red = this.green = this.blue = 255;
			#endif
		}


		public void Clear (byte light) {
			typeIndex = 0;
			hasContent = 0;
			opaque = 0;
			occludes = 0;
			this.light = light;
			this.waterLevel = 0;
		}

		public static void Clear (Voxel[] voxels, byte light) {
			for (int k = 0; k < voxels.Length; k++) {
				if (voxels [k].hasContent == 1) {
					voxels [k].typeIndex = 0;
					voxels [k].hasContent = 0;
					voxels [k].opaque = 0;
					voxels [k].occludes = 0;
					voxels [k].light = light;
					voxels [k].waterLevel = 0;
				} else {
					voxels [k].light = light;
				}
			}
		}

		public void Set (VoxelDefinition type, Color32 tintColor) {
			#if USES_TINTING
			this.red = tintColor.r;
			this.green = tintColor.g;
			this.blue = tintColor.b;
			#endif

			this.typeIndex = type.index;
			this.hasContent = (byte)1;
			switch (type.renderType) {
			case RenderType.Opaque:
				this.opaque = 15;
				this.occludes = 1;
				this.waterLevel = 0;
				break;
			case RenderType.Cutout:
				this.opaque = 2;
				this.occludes = 1;
				this.waterLevel = 0;
				break;
			case RenderType.Transparent:
				this.opaque = 2;
				this.occludes = 0;
				this.waterLevel = type.height;
				break;
			case RenderType.OpaqueNoAO:
				this.opaque = 15;
				this.occludes = 1;
				this.waterLevel = 0;
				break;
			default:
				this.opaque = 0;
				this.occludes = 0;
				this.waterLevel = 0;
				break;
			}
		}

		public void SetFast (VoxelDefinition type, byte opaque, byte occludes, Color32 tintColor) {
			#if USES_TINTING
			this.red = tintColor.r;
			this.green = tintColor.g;
			this.blue = tintColor.b;
			#endif
			this.typeIndex = type.index;
			//this.type = type;
			this.hasContent = (byte)1;
			this.opaque = opaque;
			this.occludes = occludes;
			this.waterLevel = 0;
		}


		public static Voxel Empty = new Voxel (null);

		public static bool supportsTinting {
			get {
				#if USES_TINTING
				return true;
				#else
				return false;
				#endif
			}
		}

			
	}

}
