﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.Rendering;


namespace VoxelPlay {

	public partial class VoxelPlayEnvironment : MonoBehaviour {

		/// <summary>
		/// Generates chunk mesh. Also computes lightmap if needed.
		/// </summary>
		/// <param name="chunk">Chunk.</param>
		/// <param name="forceClearLightmap">If set to <c>true</c> force clear lightmap.</param>
		void GenerateMeshData_Triangle (int jobIndex) {

			VoxelChunk chunk = meshJobs [jobIndex].chunk;
			tempChunkVertices = meshJobs [jobIndex].vertices;
			tempChunkUV0 = meshJobs [jobIndex].uv0;
			tempChunkColors32 = meshJobs [jobIndex].colors;
			tempChunkNormals = meshJobs [jobIndex].normals;
			meshColliderVertices = meshJobs [jobIndex].colliderVertices;
			meshColliderIndices = meshJobs [jobIndex].colliderIndices;
			opaqueIndices = meshJobs [jobIndex].opaqueIndices;
			transpIndices = meshJobs [jobIndex].transpIndices;
			cutoutIndices = meshJobs [jobIndex].cutoutIndices;
			cutxssIndices = meshJobs [jobIndex].cutxssIndices;
			navMeshVertices = meshJobs [jobIndex].navMeshVertices;
			navMeshIndices = meshJobs [jobIndex].navMeshIndices;
			mivs = meshJobs [jobIndex].mivs;

			opaqueIndices.Clear ();
			cutoutIndices.Clear ();
			transpIndices.Clear ();
			cutxssIndices.Clear ();
			tempChunkVertices.Clear ();
			tempChunkUV0.Clear ();
			tempChunkColors32.Clear ();
			tempChunkNormals.Clear ();
			mivs.Clear ();

			if (enableColliders) {
				meshColliderIndices.Clear ();
				meshColliderVertices.Clear ();
				if (enableNavMesh) {
					navMeshIndices.Clear ();
					navMeshVertices.Clear ();
				}
			}

			int opaqueVoxelsCount = 0;
			int cutoutVoxelsCount = 0;
			int transpVoxelsCount = 0;
			int cutxssVoxelsCount = 0;

			int chunkVoxelCount = 0;
			Color32 tintColor = Misc.color32White;
			Vector3 pos = Misc.vector3zero;

			Voxel[] voxels = chunk.voxels;

			const int V_ONE_Y_ROW = 18 * 18;
			const int V_ONE_Z_ROW = 18;
			ModelInVoxel miv = new ModelInVoxel ();

			int voxelSignature = 0;
			int voxelIndex = 0;
			for (int y = 0; y < 16; y++) {
				int vy = (y + 1) * 18 * 18;
				for (int z = 0; z < 16; z++) {
					int vyz = vy + (z + 1) * 18;
					for (int x = 0; x < 16; x++, voxelIndex++) {
						voxels [voxelIndex].lightMesh = voxels [voxelIndex].light;
						if (voxels [voxelIndex].hasContent == 1) {
							// If voxel is surrounded by material, don't render
							int vxyz = vyz + x + 1;

							int vindex = vxyz - 1;
							Voxel[] chunk_middle_middle_left = chunk9 [virtualChunk [vindex].chunk9Index];
							int middle_middle_left = virtualChunk [vindex].voxelIndex;

							vindex = vxyz + 1;
							Voxel[] chunk_middle_middle_right = chunk9 [virtualChunk [vindex].chunk9Index];
							int middle_middle_right = virtualChunk [vindex].voxelIndex;

							vindex = vxyz + V_ONE_Y_ROW;
							Voxel[] chunk_top_middle_middle = chunk9 [virtualChunk [vindex].chunk9Index];
							int top_middle_middle = virtualChunk [vindex].voxelIndex;

							vindex = vxyz - V_ONE_Y_ROW;
							Voxel[] chunk_bottom_middle_middle = chunk9 [virtualChunk [vindex].chunk9Index];
							int bottom_middle_middle = virtualChunk [vindex].voxelIndex;

							vindex = vxyz + V_ONE_Z_ROW;
							Voxel[] chunk_middle_forward_middle = chunk9 [virtualChunk [vindex].chunk9Index];
							int middle_forward_middle = virtualChunk [vindex].voxelIndex;

							vindex = vxyz - V_ONE_Z_ROW;
							Voxel[] chunk_middle_back_middle = chunk9 [virtualChunk [vindex].chunk9Index];
							int middle_back_middle = virtualChunk [vindex].voxelIndex;

							// If voxel is surrounded by material, don't render
							bool empty = false;
							if (chunk_middle_middle_left [middle_middle_left].opaque < FULL_OPAQUE)
								empty = true;
							else if (chunk_middle_middle_right [middle_middle_right].opaque < FULL_OPAQUE)
								empty = true;
							else if (chunk_top_middle_middle [top_middle_middle].opaque < FULL_OPAQUE)
								empty = true;
							else if (chunk_bottom_middle_middle [bottom_middle_middle].opaque < FULL_OPAQUE)
								empty = true;
							else if (chunk_middle_forward_middle [middle_forward_middle].opaque < FULL_OPAQUE)
								empty = true;
							else if (chunk_middle_back_middle [middle_back_middle].opaque < FULL_OPAQUE)
								empty = true;
							if (!empty)
								continue;

							// top
							vindex = vxyz + V_ONE_Y_ROW + V_ONE_Z_ROW - 1;
							Voxel[] chunk_top_forward_left = chunk9 [virtualChunk [vindex].chunk9Index];
							int top_forward_left = virtualChunk [vindex].voxelIndex;

							vindex++;
							Voxel[] chunk_top_forward_middle = chunk9 [virtualChunk [vindex].chunk9Index];
							int top_forward_middle = virtualChunk [vindex].voxelIndex;

							vindex++;
							Voxel[] chunk_top_forward_right = chunk9 [virtualChunk [vindex].chunk9Index];
							int top_forward_right = virtualChunk [vindex].voxelIndex;

							vindex = vxyz + V_ONE_Y_ROW - 1;
							Voxel[] chunk_top_middle_left = chunk9 [virtualChunk [vindex].chunk9Index];
							int top_middle_left = virtualChunk [vindex].voxelIndex;

							vindex += 2;
							Voxel[] chunk_top_middle_right = chunk9 [virtualChunk [vindex].chunk9Index];
							int top_middle_right = virtualChunk [vindex].voxelIndex;

							vindex = vxyz + V_ONE_Y_ROW - V_ONE_Z_ROW - 1;
							Voxel[] chunk_top_back_left = chunk9 [virtualChunk [vindex].chunk9Index];
							int top_back_left = virtualChunk [vindex].voxelIndex;

							vindex++;
							Voxel[] chunk_top_back_middle = chunk9 [virtualChunk [vindex].chunk9Index];
							int top_back_middle = virtualChunk [vindex].voxelIndex;

							vindex++;
							Voxel[] chunk_top_back_right = chunk9 [virtualChunk [vindex].chunk9Index];
							int top_back_right = virtualChunk [vindex].voxelIndex;

							// middle
							vindex = vxyz + V_ONE_Z_ROW - 1;
							Voxel[] chunk_middle_forward_left = chunk9 [virtualChunk [vindex].chunk9Index];
							int middle_forward_left = virtualChunk [vindex].voxelIndex;

							vindex += 2;
							Voxel[] chunk_middle_forward_right = chunk9 [virtualChunk [vindex].chunk9Index];
							int middle_forward_right = virtualChunk [vindex].voxelIndex;

							vindex = vxyz - V_ONE_Z_ROW - 1;
							Voxel[] chunk_middle_back_left = chunk9 [virtualChunk [vindex].chunk9Index];
							int middle_back_left = virtualChunk [vindex].voxelIndex;

							vindex += 2;
							Voxel[] chunk_middle_back_right = chunk9 [virtualChunk [vindex].chunk9Index];
							int middle_back_right = virtualChunk [vindex].voxelIndex;

							// bottom
							vindex = vxyz - V_ONE_Y_ROW + V_ONE_Z_ROW - 1;
							Voxel[] chunk_bottom_forward_left = chunk9 [virtualChunk [vindex].chunk9Index];
							int bottom_forward_left = virtualChunk [vindex].voxelIndex;

							vindex++;
							Voxel[] chunk_bottom_forward_middle = chunk9 [virtualChunk [vindex].chunk9Index];
							int bottom_forward_middle = virtualChunk [vindex].voxelIndex;

							vindex++;
							Voxel[] chunk_bottom_forward_right = chunk9 [virtualChunk [vindex].chunk9Index];
							int bottom_forward_right = virtualChunk [vindex].voxelIndex;

							vindex = vxyz - V_ONE_Y_ROW - 1;
							Voxel[] chunk_bottom_middle_left = chunk9 [virtualChunk [vindex].chunk9Index];
							int bottom_middle_left = virtualChunk [vindex].voxelIndex;

							vindex += 2;
							Voxel[] chunk_bottom_middle_right = chunk9 [virtualChunk [vindex].chunk9Index];
							int bottom_middle_right = virtualChunk [vindex].voxelIndex;

							vindex = vxyz - V_ONE_Y_ROW - V_ONE_Z_ROW - 1;
							Voxel[] chunk_bottom_back_left = chunk9 [virtualChunk [vindex].chunk9Index];
							int bottom_back_left = virtualChunk [vindex].voxelIndex;

							vindex++;
							Voxel[] chunk_bottom_back_middle = chunk9 [virtualChunk [vindex].chunk9Index];
							int bottom_back_middle = virtualChunk [vindex].voxelIndex;

							vindex++;
							Voxel[] chunk_bottom_back_right = chunk9 [virtualChunk [vindex].chunk9Index];
							int bottom_back_right = virtualChunk [vindex].voxelIndex;


							pos.x = x - 7.5f;
							pos.y = y - 7.5f;
							pos.z = z - 7.5f;

							chunkVoxelCount++;
							voxelSignature += voxelIndex;

							VoxelDefinition type = voxelDefinitions [voxels [voxelIndex].typeIndex];
							tintColor.r = voxels [voxelIndex].red;
							tintColor.g = voxels [voxelIndex].green;
							tintColor.b = voxels [voxelIndex].blue;

							switch (type.renderType) {
							case RenderType.Transparent:
								{
									int occ;
									int foam = 0;
									const int noflow = (1 << 8); // vertical flow
									transpVoxelsCount++;

									// Get corners heights
									int light = voxels [voxelIndex].light << 4;
									int flow = noflow;
									int hf = chunk_middle_forward_middle [middle_forward_middle].waterLevel;
									int hb = chunk_middle_back_middle [middle_back_middle].waterLevel;
									int hr = chunk_middle_middle_right [middle_middle_right].waterLevel;
									int hl = chunk_middle_middle_left [middle_middle_left].waterLevel;

									int hfr, hbr, hfl, hbl;
									// If there's water on top, full size
									if (chunk_top_middle_middle [top_middle_middle].waterLevel > 0) {
										hfr = hbr = hfl = hbl = 15;
									} else {
										int h = voxels [voxelIndex].waterLevel;
										hfr = chunk_middle_forward_right [middle_forward_right].waterLevel;
										hbr = chunk_middle_back_right [middle_back_right].waterLevel;
										hbl = chunk_middle_back_left [middle_back_left].waterLevel;
										hfl = chunk_middle_forward_left [middle_forward_left].waterLevel;

										int tf = chunk_top_forward_middle [top_forward_middle].waterLevel;
										int tfr = chunk_top_forward_right [top_forward_right].waterLevel;
										int tr = chunk_top_middle_right [top_middle_right].waterLevel;
										int tbr = chunk_top_back_right [top_back_right].waterLevel;
										int tb = chunk_top_back_middle [top_back_middle].waterLevel;
										int tbl = chunk_top_back_left [top_back_left].waterLevel;
										int tl = chunk_top_middle_left [top_middle_left].waterLevel;
										int tfl = chunk_top_forward_left [top_forward_left].waterLevel;

										// forward right corner
										if (tf * hf + tfr * hfr + tr * hr > 0) {
											hfr = 15;
										} else {
											hfr = h > hfr ? h : hfr;
											if (hf > hfr)
												hfr = hf;
											if (hr > hfr)
												hfr = hr;
										}
										// bottom right corner
										if (tr * hr + tbr * hbr + tb * hb > 0) {
											hbr = 15;
										} else {
											hbr = h > hbr ? h : hbr;
											if (hr > hbr)
												hbr = hr;
											if (hb > hbr)
												hbr = hb;
										}
										// bottom left corner
										if (tb * hb + tbl * hbl + tl * hl > 0) {
											hbl = 15;
										} else {
											hbl = h > hbl ? h : hbl;
											if (hb > hbl)
												hbl = hb;
											if (hl > hbl)
												hbl = hl;
										}
										// forward left corner
										if (tl * hl + tfl * hfl + tf * hf > 0) {
											hfl = 15;
										} else {
											hfl = h > hfl ? h : hfl;
											if (hl > hfl)
												hfl = hl;
											if (hf > hfl)
												hfl = hf;
										}

										// flow
										int fx = hfr + hbr - hfl - hbl;
										if (fx < 0)
											flow = 2 << 10;
										else if (fx == 0)
											flow = 1 << 10;
										else
											flow = 0;
									
										int fz = hfl + hfr - hbl - hbr;
										if (fz > 0)
											flow += 2 << 8;
										else if (fz == 0)
											flow += 1 << 8;
									}
									pos.y -= 0.5f;

									// back face
									occ = chunk_middle_back_middle [middle_back_middle].hasContent;
									if (occ != 0) {
										// occlusionX bit = 0 means that face is hidden; this occurs if next voxel is water BUT voxel below that neighour voxel has contents, otherwise it needs to be
										// visible because water voxels are slightly displaced down
										if (hb > 0 && chunk_bottom_back_middle [bottom_back_middle].hasContent != 0) {
										} else {
											occ = 0;
										}
										if (hb == 0) {
											foam = 1;
										}
									}
									if (occ != 1) {
										AddFaceWater (faceVerticesBack, normalsBack, pos, transpIndices, type.textureIndexSide, light + noflow, 0, hbl, 0, hbr, tintColor);
									}

									// front face
									occ = chunk_middle_forward_middle [middle_forward_middle].hasContent;
									if (occ != 0) {
										if (hf > 0 && chunk_bottom_forward_middle [bottom_forward_middle].hasContent != 0) {
										} else {
											occ = 0;
										}
										if (hf == 0) {
											foam += 2;
										}
									}
									if (occ != 1) {
										AddFaceWater (faceVerticesForward, normalsForward, pos, transpIndices, type.textureIndexSide, light + noflow, 0, hfr, 0, hfl, tintColor);
									}

									// left face
									occ = chunk_middle_middle_left [middle_middle_left].hasContent;
									if (occ != 0) {
										if (hl > 0 && chunk_bottom_middle_left [bottom_middle_left].hasContent != 0) {
										} else {
											occ = 0;
										}
										if (hl == 0) {
											foam += 4;
										}
									}
									if (occ != 1) {
										AddFaceWater (faceVerticesLeft, normalsLeft, pos, transpIndices, type.textureIndexSide, light + noflow, 0, hfl, 0, hbl, tintColor);
									}

									// right face
									occ = chunk_middle_middle_right [middle_middle_right].hasContent;
									if (occ != 0) {
										if (hr > 0 && chunk_bottom_middle_right [bottom_middle_right].hasContent != 0) {
										} else {
											occ = 0;
										}
										if (hr == 0) {
											foam += 8;
										}
									}
									if (occ != 1) {
										AddFaceWater (faceVerticesRight, normalsRight, pos, transpIndices, type.textureIndexSide, light + noflow, 0, hbr, 0, hfr, tintColor);
									}

									// top
									occ = chunk_top_middle_middle [top_middle_middle].hasContent;
									if (occ != 1) {
										if (chunk_top_back_middle [top_back_middle].hasContent != 1 || chunk_top_forward_middle [top_forward_middle].hasContent != 1 ||
										    chunk_top_middle_left [top_middle_left].hasContent != 1 || chunk_top_middle_right [top_middle_right].hasContent != 1) {
											AddFaceWater (faceVerticesTop, normalsUp, pos, transpIndices, type.textureIndexSide, light + foam + flow, hfl, hfr, hbl, hbr, tintColor);
										}
									}

									// bottom
									occ = chunk_bottom_middle_middle [bottom_middle_middle].hasContent;
									if (occ != 1) {
										AddFaceWater (faceVerticesBottom, normalsDown, pos, transpIndices, type.textureIndexSide, light + noflow, 0, 0, 0, 0, tintColor);
									}
								}
								break;
							case RenderType.CutoutCross:
								{
									cutxssVoxelsCount++;
									float light = voxels [voxelIndex].light / 15f;
									float random = WorldRand.GetValue (pos);
									light *= 0.8f + random * 0.3f;  // adds color variation
									AddFaceVegetation (faceVerticesCross1, pos, cutxssIndices, type.textureIndexSide, light, tintColor);
									AddFaceVegetation (faceVerticesCross2, pos, cutxssIndices, type.textureIndexSide, light, tintColor);
								}
								break;
							case RenderType.OpaqueNoAO:
								{
									int v1u = chunk_top_middle_middle [top_middle_middle].occludes;
									int v1f = chunk_middle_forward_middle [middle_forward_middle].occludes;
									int v1b = chunk_middle_back_middle [middle_back_middle].occludes;
									int v1l = chunk_middle_middle_left [middle_middle_left].occludes;
									int v1r = chunk_middle_middle_right [middle_middle_right].occludes;
									int v1d = chunk_bottom_middle_middle [bottom_middle_middle].occludes;
									opaqueVoxelsCount++;

									// back face
									if (v1b == 0) {
										greedyNoAO.AddQuad (FACE_DIRECTION.Back, x, y, z, ref Misc.vector3back, ref tintColor, 1f, type.textureIndexSide);
									}
									// forward face
									if (v1f == 0) {
										greedyNoAO.AddQuad (FACE_DIRECTION.Forward, x, y, z, ref Misc.vector3forward, ref tintColor, 1f, type.textureIndexSide);
									}
									// left face
									if (v1l == 0) {
										greedyNoAO.AddQuad (FACE_DIRECTION.Left, z, y, x, ref Misc.vector3left, ref tintColor, 1f, type.textureIndexSide);
									}
									// right face
									if (v1r == 0) {
										greedyNoAO.AddQuad (FACE_DIRECTION.Right, z, y, x, ref Misc.vector3right, ref tintColor, 1f, type.textureIndexSide);
									}
									// top face
									if (v1u == 0) {
										greedyNoAO.AddQuad (FACE_DIRECTION.Top, x, z, y, ref Misc.vector3up, ref tintColor, 1f, type.textureIndexTop);
									}
									// bottom face
									if (v1d == 0) {
										greedyNoAO.AddQuad (FACE_DIRECTION.Bottom, x, z, y, ref Misc.vector3down, ref tintColor, 1f, type.textureIndexBottom);
									}
								}
								break;
							case RenderType.Custom:
								miv.vd = type;
								miv.voxelIndex = voxelIndex;
								mivs.Add (miv);
								break;
							default:
#if UNITY_EDITOR
								if (enableAmbientOcclusion && !draftModeActive) {
#else
																																if (enableAmbientOcclusion) {
#endif
									// Opaque / Cutout with AO

									int v2r = chunk_top_middle_right [top_middle_right].occludes;
									int v2br = chunk_top_back_right [top_back_right].occludes;
									int v2b = chunk_top_back_middle [top_back_middle].occludes;
									int v2bl = chunk_top_back_left [top_back_left].occludes;
									int v2l = chunk_top_middle_left [top_middle_left].occludes;
									int v2fl = chunk_top_forward_left [top_forward_left].occludes;
									int v2f = chunk_top_forward_middle [top_forward_middle].occludes;
									int v2fr = chunk_top_forward_right [top_forward_right].occludes;

									int v1fr = chunk_middle_forward_right [middle_forward_right].occludes;
									int v1br = chunk_middle_back_right [middle_back_right].occludes;
									int v1bl = chunk_middle_back_left [middle_back_left].occludes;
									int v1fl = chunk_middle_forward_left [middle_forward_left].occludes;

									int v0r = chunk_bottom_middle_right [bottom_middle_right].occludes;
									int v0br = chunk_bottom_back_right [bottom_back_right].occludes;
									int v0b = chunk_bottom_back_middle [bottom_back_middle].occludes;
									int v0bl = chunk_bottom_back_left [bottom_back_left].occludes;
									int v0l = chunk_bottom_middle_left [bottom_middle_left].occludes;
									int v0fl = chunk_bottom_forward_left [bottom_forward_left].occludes;
									int v0f = chunk_bottom_forward_middle [bottom_forward_middle].occludes;
									int v0fr = chunk_bottom_forward_right [bottom_forward_right].occludes;

//									int v1u = chunk_top_middle_middle [top_middle_middle].occludes;
//									int v1f = chunk_middle_forward_middle [middle_forward_middle].occludes;
//									int v1b = chunk_middle_back_middle [middle_back_middle].occludes;
//									int v1l = chunk_middle_middle_left [middle_middle_left].occludes;
//									int v1r = chunk_middle_middle_right [middle_middle_right].occludes;
//									int v1d = chunk_bottom_middle_middle [bottom_middle_middle].occludes;

									float occ, l0, l1, l2, l3;

									List<int> indices;
									bool denseTreeCheck = false;
									byte leavesLight = 0;
									int windAnimation = 0;
									float aoBase = 1f / 15f;
									if (type.renderType == RenderType.Opaque) {
										opaqueVoxelsCount++;
										indices = opaqueIndices;
									} else {
										cutoutVoxelsCount++;
										indices = cutoutIndices;
										denseTreeCheck = denseTrees;
										if (denseTrees)
											leavesLight = voxels [voxelIndex].light;
										float random = WorldRand.GetValue (pos);
										aoBase *= 0.8f + random * 0.4f;  // adds color variation
										if (type.windAnimation)
											windAnimation = 65536;
									}
									bool addCollider = enableColliders && voxels [voxelIndex].opaque > 5;

									// back face
									if (chunk_middle_back_middle [middle_back_middle].opaque < FULL_OPAQUE || denseTreeCheck) {
										float backFaceGI = (chunk_middle_back_middle [middle_back_middle].light | leavesLight) * aoBase;
										if (backFaceGI > 0) {
											// Vertex 0
											occ = v0b + v0bl + v1bl;
											l0 = backFaceGI / (1f + occ * occ * ambientOcclusionIntensity);
											// Vertex 2
											occ = v1bl + v2bl + v2b;
											l1 = backFaceGI / (1f + occ * occ * ambientOcclusionIntensity);
											// Vertex 1
											occ = v0b + v0br + v1br;
											l2 = backFaceGI / (1f + occ * occ * ambientOcclusionIntensity);
											// Vertex 3
											occ = v2b + v2br + v1br;
											l3 = backFaceGI / (1f + occ * occ * ambientOcclusionIntensity);
										} else {
											l0 = 0;
											l1 = 0;
											l2 = 0;
											l3 = 0;
										}
										AddFaceWithAO (faceVerticesBack, normalsBack, pos, indices, type.textureIndexSide + windAnimation, l0, l1, l2, l3, true, tintColor);
										if (addCollider) {
											greedyCollider.AddQuad (FACE_DIRECTION.Back, x, y, z);
											if (enableNavMesh && type.navigatable) {
												greedyNavMesh.AddQuad (FACE_DIRECTION.Back, x, y, z);
											}
										}
									}
									// forward face
									if (chunk_middle_forward_middle [middle_forward_middle].opaque < FULL_OPAQUE || denseTreeCheck) {
										float frontFaceGI = (chunk_middle_forward_middle [middle_forward_middle].light | leavesLight) * aoBase;
										if (frontFaceGI > 0) {
											// Vertex 5
											occ = v0f + v0fr + v1fr;
											l0 = frontFaceGI / (1f + occ * occ * ambientOcclusionIntensity);
											// Vertex 6
											occ = v1fr + v2fr + v2f;
											l1 = frontFaceGI / (1f + occ * occ * ambientOcclusionIntensity);
											// Vertex 4
											occ = v0f + v0fl + v1fl;
											l2 = frontFaceGI / (1f + occ * occ * ambientOcclusionIntensity);
											// Vertex 7
											occ = v2f + v2fl + v1fl;
											l3 = frontFaceGI / (1f + occ * occ * ambientOcclusionIntensity);
										} else {
											l0 = 0;
											l1 = 0;
											l2 = 0;
											l3 = 0;
										}
										AddFaceWithAO (faceVerticesForward, normalsForward, pos, indices, type.textureIndexSide + windAnimation, l0, l1, l2, l3, true, tintColor);
										if (addCollider) {
											greedyCollider.AddQuad (FACE_DIRECTION.Forward, x, y, z);
											if (enableNavMesh && type.navigatable) {
												greedyNavMesh.AddQuad (FACE_DIRECTION.Forward, x, y, z);
											}
										}
									}
									// left face
									if (chunk_middle_middle_left [middle_middle_left].opaque < FULL_OPAQUE || denseTreeCheck) {
										float leftFaceGI = (chunk_middle_middle_left [middle_middle_left].light | leavesLight) * aoBase;
										if (leftFaceGI > 0) {
											// Vertex 4
											occ = v1fl + v0l + v0fl;
											l0 = leftFaceGI / (1f + occ * occ * ambientOcclusionIntensity);
											// Vertex 7
											occ = v2l + v2fl + v1fl;
											l1 = leftFaceGI / (1f + occ * occ * ambientOcclusionIntensity);
											// Vertex 0
											occ = v1bl + v0l + v0bl;
											l2 = leftFaceGI / (1f + occ * occ * ambientOcclusionIntensity);
											// Vertex 2
											occ = v2l + v2bl + v1bl;
											l3 = leftFaceGI / (1f + occ * occ * ambientOcclusionIntensity);
										} else {
											l0 = 0;
											l1 = 0;
											l2 = 0;
											l3 = 0;
										}
										AddFaceWithAO (faceVerticesLeft, normalsLeft, pos, indices, type.textureIndexSide + windAnimation, l0, l1, l2, l3, true, tintColor);
										if (addCollider) {
											greedyCollider.AddQuad (FACE_DIRECTION.Left, z, y, x);
											if (enableNavMesh && type.navigatable) {
												greedyNavMesh.AddQuad (FACE_DIRECTION.Left, z, y, x);
											}
										}
									}
									// right face
									if (chunk_middle_middle_right [middle_middle_right].opaque < FULL_OPAQUE || denseTreeCheck) {
										float rightFaceGI = (chunk_middle_middle_right [middle_middle_right].light | leavesLight) * aoBase;
										if (rightFaceGI > 0) {
											// Vertex 1
											occ = v1br + v0r + v0br;
											l0 = rightFaceGI / (1f + occ * occ * ambientOcclusionIntensity);
											// Vertex 3
											occ = v2r + v2br + v1br;
											l1 = rightFaceGI / (1f + occ * occ * ambientOcclusionIntensity);
											// Vertex 5
											occ = v1fr + v0r + v0fr;
											l2 = rightFaceGI / (1f + occ * occ * ambientOcclusionIntensity);
											// Vertex 6
											occ = v2r + v2fr + v1fr;
											l3 = rightFaceGI / (1f + occ * occ * ambientOcclusionIntensity);
										} else {
											l0 = 0;
											l1 = 0;
											l2 = 0;
											l3 = 0;
										}
										AddFaceWithAO (faceVerticesRight, normalsRight, pos, indices, type.textureIndexSide + windAnimation, l0, l1, l2, l3, true, tintColor);
										if (addCollider) {
											greedyCollider.AddQuad (FACE_DIRECTION.Right, z, y, x);
											if (enableNavMesh && type.navigatable) {
												greedyNavMesh.AddQuad (FACE_DIRECTION.Right, z, y, x);
											}
										}
									}
									// top face
									if (chunk_top_middle_middle [top_middle_middle].opaque < FULL_OPAQUE || denseTreeCheck) {
										// Top face
										float topFaceGI = (chunk_top_middle_middle [top_middle_middle].light | leavesLight) * aoBase;
										if (topFaceGI > 0) {
											// Vertex 7
											occ = v2l + v2fl + v2f;
											l0 = topFaceGI / (1f + occ * occ * ambientOcclusionIntensity);
											// Vertex 6
											occ = v2r + v2fr + v2f;
											l1 = topFaceGI / (1f + occ * occ * ambientOcclusionIntensity);
											// Vertex 2
											occ = v2b + v2bl + v2l;
											l2 = topFaceGI / (1f + occ * occ * ambientOcclusionIntensity);
											// Vvertex 3
											occ = v2b + v2br + v2r;
											l3 = topFaceGI / (1f + occ * occ * ambientOcclusionIntensity);
										} else {
											l0 = 0;
											l1 = 0;
											l2 = 0;
											l3 = 0;
										}
										AddFaceWithAO (faceVerticesTop, normalsUp, pos, indices, type.textureIndexTop + windAnimation, l0, l1, l2, l3, true, tintColor);
										if (addCollider) {
											greedyCollider.AddQuad (FACE_DIRECTION.Top, x, z, y);
											if (enableNavMesh && type.navigatable) {
												greedyNavMesh.AddQuad (FACE_DIRECTION.Top, x, z, y);
											}
										}
									}
									// bottom face
									if (chunk_bottom_middle_middle [bottom_middle_middle].opaque < FULL_OPAQUE || denseTreeCheck) {
										float bottomFaceGI = (chunk_bottom_middle_middle [bottom_middle_middle].light | leavesLight) * aoBase;
										if (bottomFaceGI > 0) {
											// Vertex 0
											occ = v0b + v0l + v0bl;
											l0 = bottomFaceGI / (1f + occ * occ * ambientOcclusionIntensity);
											// Vertex 1
											occ = v0b + v0r + v0br;
											l1 = bottomFaceGI / (1f + occ * occ * ambientOcclusionIntensity);
											// Vertex 4
											occ = v0f + v0l + v0fl;
											l2 = bottomFaceGI / (1f + occ * occ * ambientOcclusionIntensity);
											// Vertex 5
											occ = v0f + v0r + v0fr;
											l3 = bottomFaceGI / (1f + occ * occ * ambientOcclusionIntensity);
										} else {
											l0 = 0;
											l1 = 0;
											l2 = 0;
											l3 = 0;
										}
										AddFaceWithAO (faceVerticesBottom, normalsDown, pos, indices, type.textureIndexBottom + windAnimation, l0, l1, l2, l3, true, tintColor);
										if (addCollider) {
											greedyCollider.AddQuad (FACE_DIRECTION.Bottom, x, z, y);
											// no NavMesh for bottom faces
										}
									}
								} else {
									// Opaque / Cutout without AO
									int v1b = chunk_middle_back_middle [middle_back_middle].opaque;
									int v1f = chunk_middle_forward_middle [middle_forward_middle].opaque;
									int v1u = chunk_top_middle_middle [top_middle_middle].opaque;
									int v1d = chunk_bottom_middle_middle [bottom_middle_middle].opaque;
									int v1l = chunk_middle_middle_left [middle_middle_left].opaque;
									int v1r = chunk_middle_middle_right [middle_middle_right].opaque;

									bool denseTreeCheck = false;
									float aoBase = 1f / 15f;
									bool isOpaqueType = type.renderType == RenderType.Opaque;
									byte leavesLight = 0;
									int windAnimation = 0;
									if (isOpaqueType) {
										opaqueVoxelsCount++;
									} else {
										cutoutVoxelsCount++;
										denseTreeCheck = denseTrees;
										if (denseTrees)
											leavesLight = voxels [voxelIndex].light;
										if (type.windAnimation)
											windAnimation = 65536;
										float random = WorldRand.GetValue (pos);
										aoBase *= 0.8f + random * 0.4f;  // adds color variation
									}
									bool addCollider = enableColliders && voxels [voxelIndex].opaque > 5;

									// back face
									if (v1b < FULL_OPAQUE || denseTreeCheck) {												
										float backFaceGI = (chunk_middle_back_middle [middle_back_middle].light | leavesLight) * aoBase;
										if (isOpaqueType) {
											greedyNoAO.AddQuad (FACE_DIRECTION.Back, x, y, z, ref Misc.vector3back, ref tintColor, backFaceGI, type.textureIndexSide + windAnimation);
										} else {
											greedyCutout.AddQuad (FACE_DIRECTION.Back, x, y, z, ref Misc.vector3back, ref tintColor, backFaceGI, type.textureIndexSide + windAnimation);
										}
										if (addCollider) {
											greedyCollider.AddQuad (FACE_DIRECTION.Back, x, y, z);
											if (enableNavMesh && type.navigatable) {
												greedyNavMesh.AddQuad (FACE_DIRECTION.Back, x, y, z);
											}
										}
									}
									// forward face
									if (v1f < FULL_OPAQUE || denseTreeCheck) {
										float frontFaceGI = (chunk_middle_forward_middle [middle_forward_middle].light | leavesLight) * aoBase;
										if (isOpaqueType) {
											greedyNoAO.AddQuad (FACE_DIRECTION.Forward, x, y, z, ref Misc.vector3forward, ref tintColor, frontFaceGI, type.textureIndexSide + windAnimation);
										} else {
											greedyCutout.AddQuad (FACE_DIRECTION.Forward, x, y, z, ref Misc.vector3forward, ref tintColor, frontFaceGI, type.textureIndexSide + windAnimation);
										}
										if (addCollider) {
											greedyCollider.AddQuad (FACE_DIRECTION.Forward, x, y, z);
											if (enableNavMesh && type.navigatable) {
												greedyNavMesh.AddQuad (FACE_DIRECTION.Forward, x, y, z);
											}
										}
									}
									// left face
									if (v1l < FULL_OPAQUE || denseTreeCheck) {
										float leftFaceGI = (chunk_middle_middle_left [middle_middle_left].light | leavesLight) * aoBase;
										if (isOpaqueType) {
											greedyNoAO.AddQuad (FACE_DIRECTION.Left, z, y, x, ref Misc.vector3left, ref tintColor, leftFaceGI, type.textureIndexSide + windAnimation);
										} else {
											greedyCutout.AddQuad (FACE_DIRECTION.Left, z, y, x, ref Misc.vector3left, ref tintColor, leftFaceGI, type.textureIndexSide);
										}
										if (addCollider) {
											greedyCollider.AddQuad (FACE_DIRECTION.Left, z, y, x);
											if (enableNavMesh && type.navigatable) {
												greedyNavMesh.AddQuad (FACE_DIRECTION.Left, z, y, y);
											}
										}
									}
									// right face
									if (v1r < FULL_OPAQUE || denseTreeCheck) {
										float rightFaceGI = (chunk_middle_middle_right [middle_middle_right].light | leavesLight) * aoBase;
										if (isOpaqueType) {
											greedyNoAO.AddQuad (FACE_DIRECTION.Right, z, y, x, ref Misc.vector3right, ref tintColor, rightFaceGI, type.textureIndexSide + windAnimation);
										} else {
											greedyCutout.AddQuad (FACE_DIRECTION.Right, z, y, x, ref Misc.vector3right, ref tintColor, rightFaceGI, type.textureIndexSide + windAnimation);
										}
										if (addCollider) {
											greedyCollider.AddQuad (FACE_DIRECTION.Right, z, y, x);
											if (enableNavMesh && type.navigatable) {
												greedyNavMesh.AddQuad (FACE_DIRECTION.Right, z, y, y);
											}
										}
									}
									// top face
									if (v1u < FULL_OPAQUE || denseTreeCheck) {
										// Top face
										float topFaceGI = (chunk_top_middle_middle [top_middle_middle].light | leavesLight) * aoBase;
										if (isOpaqueType) {
											greedyNoAO.AddQuad (FACE_DIRECTION.Top, x, z, y, ref Misc.vector3up, ref tintColor, topFaceGI, type.textureIndexTop + windAnimation);
										} else {
											greedyCutout.AddQuad (FACE_DIRECTION.Top, x, z, y, ref Misc.vector3up, ref tintColor, topFaceGI, type.textureIndexTop + windAnimation);
										}
										if (addCollider) {
											greedyCollider.AddQuad (FACE_DIRECTION.Top, x, z, y);
											if (enableNavMesh && type.navigatable) {
												greedyNavMesh.AddQuad (FACE_DIRECTION.Top, x, z, y);
											}
										}
									}
									// bottom face
									if (v1d < FULL_OPAQUE || denseTreeCheck) {
										float bottomFaceGI = (chunk_bottom_middle_middle [bottom_middle_middle].light | leavesLight) * aoBase;
										if (isOpaqueType) {
											greedyNoAO.AddQuad (FACE_DIRECTION.Bottom, x, z, y, ref Misc.vector3down, ref tintColor, bottomFaceGI, type.textureIndexBottom + windAnimation);
										} else {
											greedyCutout.AddQuad (FACE_DIRECTION.Bottom, x, z, y, ref Misc.vector3down, ref tintColor, bottomFaceGI, type.textureIndexBottom + windAnimation);
										}
										if (addCollider) {
											greedyCollider.AddQuad (FACE_DIRECTION.Bottom, x, z, y);
										}
									}
								}
								break;
							}
						}
					}
				}
			}

			meshJobs [jobIndex].chunk = chunk;
			meshJobs [jobIndex].totalVisibleVoxels = chunkVoxelCount;
			if (chunkVoxelCount == 0) {
				return;
			}

			voxelSignature++;
			chunk.needsColliderRebuild = voxelSignature != chunk.voxelSignature;
			chunk.voxelSignature = voxelSignature;

			if (enableColliders) {
				if (chunk.needsColliderRebuild) {
					greedyCollider.FlushTriangles (meshColliderVertices, meshColliderIndices);
					if (enableNavMesh) {
						greedyNavMesh.FlushTriangles (navMeshVertices, navMeshIndices);
					}
				} else {
					greedyCollider.Clear ();
					greedyNavMesh.Clear ();
				}
			}

			greedyNoAO.FlushTriangles (tempChunkVertices, opaqueIndices, tempChunkUV0, tempChunkNormals, enableTinting ? tempChunkColors32 : null);
			greedyCutout.FlushTriangles (tempChunkVertices, cutoutIndices, tempChunkUV0, tempChunkNormals, enableTinting ? tempChunkColors32 : null);

			meshJobs [jobIndex].vertices = tempChunkVertices;
			meshJobs [jobIndex].uv0 = tempChunkUV0;
			meshJobs [jobIndex].colors = tempChunkColors32;
			meshJobs [jobIndex].normals = tempChunkNormals;

			int matVoxelIndex = 0;
			int subMeshCount = 0;
			meshJobs [jobIndex].opaqueVoxelsCount = opaqueVoxelsCount;
			if (opaqueVoxelsCount > 0) {
				subMeshCount++;
				matVoxelIndex++;
			}
			meshJobs [jobIndex].cutoutVoxelsCount = cutoutVoxelsCount;
			if (cutoutVoxelsCount > 0) {
				subMeshCount++;
				matVoxelIndex += 2;
			}
			meshJobs [jobIndex].transpVoxelsCount = transpVoxelsCount;
			if (transpVoxelsCount > 0) {
				subMeshCount++;
				matVoxelIndex += 4;
			}
			meshJobs [jobIndex].cutxssVoxelsCount = cutxssVoxelsCount;
			if (cutxssVoxelsCount > 0) {
				subMeshCount++;
				matVoxelIndex += 8;
			}
			meshJobs [jobIndex].matVoxelIndex = matVoxelIndex;
			meshJobs [jobIndex].subMeshCount = subMeshCount;

			meshJobs [jobIndex].colliderVertices = meshColliderVertices;
			meshJobs [jobIndex].colliderIndices = meshColliderIndices;

			meshJobs [jobIndex].navMeshVertices = navMeshVertices;
			meshJobs [jobIndex].navMeshIndices = navMeshIndices;

			meshJobs [jobIndex].mivs = mivs;
		}

		void AddFaceWithAO (Vector3[] faceVertices, Vector3[] normals, Vector3 pos, List<int> indices, int textureIndex, float w0, float w1, float w2, float w3, bool flipAO, Color32 tintColor) {
			int index = tempChunkVertices.Count;
			Vector3 vertPos;
			for (int v = 0; v < 4; v++) {
				vertPos.x = faceVertices [v].x + pos.x;
				vertPos.y = faceVertices [v].y + pos.y;
				vertPos.z = faceVertices [v].z + pos.z;
				tempChunkVertices.Add (vertPos);
				tempChunkNormals.Add (normals [v]);
			}

			if (flipAO && w0 + w3 > w1 + w2) {
				indices.Add (index);
				indices.Add (index + 1);
				indices.Add (index + 3);
				indices.Add (index + 3);
				indices.Add (index + 2);
				indices.Add (index + 0);
			} else {
				indices.Add (index);
				indices.Add (index + 1);
				indices.Add (index + 2);
				indices.Add (index + 3);
				indices.Add (index + 2);
				indices.Add (index + 1);
			}

			Vector4 v4 = new Vector4 (0, 0, textureIndex, w0);
			tempChunkUV0.Add (v4);
			v4.y = 1f;
			v4.w = w1;
			tempChunkUV0.Add (v4);
			v4.x = 1f;
			v4.y = 0;
			v4.w = w2;
			tempChunkUV0.Add (v4);
			v4.y = 1f;
			v4.w = w3;
			tempChunkUV0.Add (v4);
			if (enableTinting) {
				tempChunkColors32.Add (tintColor);
				tempChunkColors32.Add (tintColor);
				tempChunkColors32.Add (tintColor);
				tempChunkColors32.Add (tintColor);
			}
		}

		void AddFaceWater (Vector3[] faceVertices, Vector3[] normals, Vector3 pos, List<int> indices, int textureIndex, int w, int h0, int h1, int h2, int h3, Color32 tintColor) {
			int index = tempChunkVertices.Count;
			Vector3 vertPos;
			// vertices
			vertPos.x = faceVertices [0].x + pos.x;
			vertPos.y = h0 / 15f + pos.y;
			vertPos.z = faceVertices [0].z + pos.z;
			tempChunkVertices.Add (vertPos);
			tempChunkNormals.Add (normals [0]);
			vertPos.x = faceVertices [1].x + pos.x;
			vertPos.y = h1 / 15f + pos.y;
			vertPos.z = faceVertices [1].z + pos.z;
			tempChunkVertices.Add (vertPos);
			tempChunkNormals.Add (normals [1]);
			vertPos.x = faceVertices [2].x + pos.x;
			vertPos.y = h2 / 15f + pos.y;
			vertPos.z = faceVertices [2].z + pos.z;
			tempChunkVertices.Add (vertPos);
			tempChunkNormals.Add (normals [2]);
			vertPos.x = faceVertices [3].x + pos.x;
			vertPos.y = h3 / 15f + pos.y;
			vertPos.z = faceVertices [3].z + pos.z;
			tempChunkVertices.Add (vertPos);
			tempChunkNormals.Add (normals [3]);
			// indices
			indices.Add (index);
			indices.Add (index + 1);
			indices.Add (index + 2);
			indices.Add (index + 3);
			indices.Add (index + 2);
			indices.Add (index + 1);
			Vector4 v4 = new Vector4 (0, 0, textureIndex, w);
			tempChunkUV0.Add (v4); //new Vector4 (0, 0, textureIndex, w));
			v4.y = 1f;
			tempChunkUV0.Add (v4); //new Vector4 (0, 1, textureIndex, w));
			v4.x = 1f;
			v4.y = 0f;
			tempChunkUV0.Add (v4); //new Vector4 (1, 0, textureIndex, w));
			v4.y = 1f;
			tempChunkUV0.Add (v4); //new Vector4 (1, 1, textureIndex, w));
			if (enableTinting) {
				tempChunkColors32.Add (tintColor);
				tempChunkColors32.Add (tintColor);
				tempChunkColors32.Add (tintColor);
				tempChunkColors32.Add (tintColor);
			}
		}

		void AddFaceVegetation (Vector3[] faceVertices, Vector3 pos, List<int> indices, int textureIndex, float w, Color32 tintColor) {
			int index = tempChunkVertices.Count;

			// Add random displacement and elevation
			Vector3 aux = pos;
			float random = WorldRand.GetValue (aux);
			pos.x += random * 0.5f - 0.25f;
			aux.x += 1f;
			random = WorldRand.GetValue (aux);
			pos.z += random * 0.5f - 0.25f;
			pos.y -= random * 0.1f;
			for (int v = 0; v < 4; v++) {
				aux.x = faceVertices [v].x + pos.x;
				aux.y = faceVertices [v].y + pos.y;
				aux.z = faceVertices [v].z + pos.z;
				tempChunkVertices.Add (aux);
				tempChunkNormals.Add (Misc.vector3zero);
			}
			indices.Add (index);
			indices.Add (index + 1);
			indices.Add (index + 2);
			indices.Add (index + 3);
			indices.Add (index + 2);
			indices.Add (index + 1);
			Vector4 v4 = new Vector4 (0, 0, textureIndex, w);
			tempChunkUV0.Add (v4); //new Vector4 (0, 0, textureIndex, w));
			v4.y = 1f; // new Vector4 (0, 1, textureIndex, w)
			tempChunkUV0.Add (v4); //new Vector4 (0, 1, textureIndex, w));
			v4.x = 1f; // new Vector4 (1, 0, textureIndex, w)
			v4.y = 0f; // new Vector4 (1, 1, textureIndex, w)
			tempChunkUV0.Add (v4); //new Vector4 (1, 0, textureIndex, w));
			v4.y = 1f;
			tempChunkUV0.Add (v4);
			if (enableTinting) {
				tempChunkColors32.Add (tintColor);
				tempChunkColors32.Add (tintColor);
				tempChunkColors32.Add (tintColor);
				tempChunkColors32.Add (tintColor);
			}
		}


		/// <summary>
		/// Creates a gameobject with geometry and materials based on the triangle renderer but lmited to one voxel
		/// </summary>
		/// <param name="chunk">Chunk.</param>
		/// <param name="voxelIndex">Voxel index.</param>
		GameObject MakeDynamicCubeFromVoxel (VoxelChunk chunk, int voxelIndex) {
			tempVertices.Clear ();
			tempNormals.Clear ();
			tempUVs.Clear ();
			tempColors.Clear ();
			tempIndicesPos = 0;

			VoxelDefinition type = voxelDefinitions[chunk.voxels [voxelIndex].typeIndex];
			Color32 tintColor = chunk.voxels [voxelIndex].color;

			AddFace (faceVerticesBack, normalsBack, type.textureIndexSide, tintColor);
			AddFace (faceVerticesForward, normalsForward, type.textureIndexSide, tintColor);
			AddFace (faceVerticesLeft, normalsLeft, type.textureIndexSide, tintColor);
			AddFace (faceVerticesRight, normalsRight, type.textureIndexSide, tintColor);
			AddFace (faceVerticesTop, normalsUp, type.textureIndexTop, tintColor);
			AddFace (faceVerticesBottom, normalsDown, type.textureIndexBottom, tintColor);

			GameObject obj = new GameObject ("DynamicVoxel");
			obj.SetActive (false);

			MeshFilter mf = obj.AddComponent<MeshFilter> ();
			Mesh mesh = new Mesh ();
			mesh.SetVertices (tempVertices);
			mesh.SetUVs (0, tempUVs);
			mesh.SetNormals (tempNormals);
			if (enableTinting) {
				mesh.SetColors (tempColors);
			}
			mesh.triangles = tempIndices;
			mf.mesh = mesh;

			MeshRenderer mr = obj.AddComponent<MeshRenderer> ();
			mr.sharedMaterial = type.renderType == RenderType.Cutout ? matTriangleCutout : matTriangleOpaque;

			obj.AddComponent<BoxCollider> ();

			obj.transform.SetParent (transform, false);

			return obj;

		}



		void AddFace (Vector3[] faceVertices, Vector3[] normals, int textureIndex, Color32 tintColor) {
			int index = tempVertices.Count;
			for (int v = 0; v < 4; v++) {
				tempVertices.Add (faceVertices [v]);
				tempNormals.Add (normals [v]);
			}

			tempIndices [tempIndicesPos++] = index;
			tempIndices [tempIndicesPos++] = index + 1;
			tempIndices [tempIndicesPos++] = index + 3;
			tempIndices [tempIndicesPos++] = index + 3;
			tempIndices [tempIndicesPos++] = index + 2;
			tempIndices [tempIndicesPos++] = index + 0;

			Vector4 v4 = new Vector4 (0, 0, textureIndex, 1f);
			tempUVs.Add (v4);
			v4.y = 1f;
			tempUVs.Add (v4);
			v4.x = 1f;
			v4.y = 0;
			tempUVs.Add (v4);
			v4.y = 1f;
			tempUVs.Add (v4);
			if (enableTinting) {
				tempColors.Add (tintColor);
				tempColors.Add (tintColor);
				tempColors.Add (tintColor);
				tempColors.Add (tintColor);
			}
		}


	}
}
