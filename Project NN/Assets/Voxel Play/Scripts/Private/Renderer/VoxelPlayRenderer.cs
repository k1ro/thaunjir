﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.Rendering;

namespace VoxelPlay {

	public partial class VoxelPlayEnvironment : MonoBehaviour {

		struct VirtualVoxel {
			public int chunk9Index;
			public int voxelIndex;
		}

		struct MeshJobData {
			public VoxelChunk chunk;
			public int totalVisibleVoxels;

			public List<Vector3> vertices;
			public List<Vector4> uv0;
			public List<Vector4> uv2;
			public List<Color32> colors;
			public List<Vector3> normals;

			public int opaqueVoxelsCount;
			public int[] opaqueIndicesArray;
			public List<int> opaqueIndices;

			public int cutoutVoxelsCount;
			public int[] cutoutIndicesArray;
			public List<int> cutoutIndices;

			public int transpVoxelsCount;
			public int[] transpIndicesArray;
			public List<int> transpIndices;

			public int cutxssVoxelsCount;
			public int[] cutxssIndicesArray;
			public List<int> cutxssIndices;

			public int opNoAOVoxelsCount;
			public int[] opNoAOIndicesArray;

			public int subMeshCount;
			public int matVoxelIndex;
			public List<Vector3> colliderVertices;
			public List<int> colliderIndices;

			public List<int> navMeshIndices;
			public List<Vector3> navMeshVertices;

			public List<ModelInVoxel> mivs;
			// models in voxels
		}


		struct ModelInVoxel {
			public VoxelDefinition vd;
			public int voxelIndex;
		}


		const int MESH_JOBS_POOL_SIZE = 4000;

		/* cube coords
		
		7+------+6
		/.   3 /|
		2+------+ |
		|4.....|.+5
		|/     |/
		0+------+1
		
		*/

		// Chunk Creation helpers for non-geometry shaders
		static Vector3[] faceVerticesForward = new Vector3[] {
			new Vector3 (0.5f, -0.5f, 0.5f),
			new Vector3 (0.5f, 0.5f, 0.5f),
			new Vector3 (-0.5f, -0.5f, 0.5f),
			new Vector3 (-0.5f, 0.5f, 0.5f)
		};
		static Vector3[] faceVerticesBack = new Vector3[] {
			new Vector3 (-0.5f, -0.5f, -0.5f),
			new Vector3 (-0.5f, 0.5f, -0.5f),
			new Vector3 (0.5f, -0.5f, -0.5f),
			new Vector3 (0.5f, 0.5f, -0.5f)
		};
		static Vector3[] faceVerticesLeft = new Vector3[] {
			new Vector3 (-0.5f, -0.5f, 0.5f),
			new Vector3 (-0.5f, 0.5f, 0.5f),
			new Vector3 (-0.5f, -0.5f, -0.5f),
			new Vector3 (-0.5f, 0.5f, -0.5f)
		};
		static Vector3[] faceVerticesRight = new Vector3[] {
			new Vector3 (0.5f, -0.5f, -0.5f),
			new Vector3 (0.5f, 0.5f, -0.5f),
			new Vector3 (0.5f, -0.5f, 0.5f),
			new Vector3 (0.5f, 0.5f, 0.5f)
		};
		static Vector3[] faceVerticesTop = new Vector3[] {
			new Vector3 (-0.5f, 0.5f, 0.5f),
			new Vector3 (0.5f, 0.5f, 0.5f),
			new Vector3 (-0.5f, 0.5f, -0.5f),
			new Vector3 (0.5f, 0.5f, -0.5f)
		};
		static Vector3[] faceVerticesBottom = new Vector3[] {
			new Vector3 (-0.5f, -0.5f, -0.5f),
			new Vector3 (0.5f, -0.5f, -0.5f),
			new Vector3 (-0.5f, -0.5f, 0.5f),
			new Vector3 (0.5f, -0.5f, 0.5f)
		};
		static Vector3[] faceVerticesCross1 = new Vector3[] {
			new Vector3 (-0.5f, -0.5f, 0.5f),
			new Vector3 (-0.5f, 0.5f, 0.5f),
			new Vector3 (0.5f, -0.5f, -0.5f),
			new Vector3 (0.5f, 0.5f, -0.5f)
		};
		static Vector3[] faceVerticesCross2 = new Vector3[] {
			new Vector3 (-0.5f, -0.5f, -0.5f),
			new Vector3 (-0.5f, 0.5f, -0.5f),
			new Vector3 (0.5f, -0.5f, 0.5f),
			new Vector3 (0.5f, 0.5f, 0.5f)
		};
		static Vector3[] normalsBack = new Vector3[] {
			Misc.vector3back, Misc.vector3back, Misc.vector3back, Misc.vector3back
		};
		static Vector3[] normalsForward = new Vector3[] {
			Misc.vector3forward, Misc.vector3forward, Misc.vector3forward, Misc.vector3forward
		};
		static Vector3[] normalsLeft = new Vector3[] {
			Misc.vector3left, Misc.vector3left, Misc.vector3left, Misc.vector3left
		};
		static Vector3[] normalsRight = new Vector3[] {
			Misc.vector3right, Misc.vector3right, Misc.vector3right, Misc.vector3right
		};
		static Vector3[] normalsUp = new Vector3[] {
			Misc.vector3up, Misc.vector3up, Misc.vector3up, Misc.vector3up
		};
		static Vector3[] normalsDown = new Vector3[] {
			Misc.vector3down, Misc.vector3down, Misc.vector3down, Misc.vector3down
		};
		VoxelPlayGreedyMesher greedyCollider, greedyNavMesh;
		VoxelPlayGreedyMesher greedyNoAO, greedyCutout;
		MeshJobData[] meshJobs;
		volatile int meshJobMeshLastIndex;
		volatile int meshJobMeshDataGenerationIndex;
		volatile int meshJobMeshDataGenerationReadyIndex;
		volatile int meshJobMeshUploadIndex;

		List<int> opaqueIndices;
		List<int> cutoutIndices;
		List<int> transpIndices;
		List<int> cutxssIndices;
		List<int> opNoAOIndices;

		// Chunk Rendering
		bool effectiveUseGeometryShaders, effectiveMultithreadGeneration;
		int chunkRequestLast;
		Voxel[][] chunk9;
		VirtualVoxel[] virtualChunk;
		Voxel[] emptyChunkUnderground, emptyChunkAboveTerrain;

		// Voxels
		[NonSerialized]
		List<Vector3> tempChunkVertices;
		List<Vector4> tempChunkUV0;
		List<Vector4> tempChunkUV2;
		List<Color32> tempChunkColors32;
		List<Vector3> tempChunkNormals;

		// Collider support
		List<int> meshColliderIndices;
		List<Vector3> meshColliderVertices;

		// Navmesh support
		List<int> navMeshIndices;
		List<Vector3> navMeshVertices;

		// Model-in-voxel support
		List<ModelInVoxel> mivs;

		// Materials
		Material matTerrainOpaque, matTerrainCutout, matTerrainWater, matTerrainCutxss, matTerrainOpNoAO;
		Material[][] matTerrainArray;
		Material matTriangleOpaque, matTriangleCutout;

		// Multi-thread support
		bool generationThreadRunning;
		AutoResetEvent waitEvent;
		Thread meshGenerationThread;
		object indicesUpdating = new object ();

		// Model-in-Voxel support
		List<Color32> modelMeshColors;

		// Dynamic Voxel mesh generation support
		List<Vector3> tempVertices;
		List<Vector3> tempNormals;
		int[] tempIndices;
		int tempIndicesPos;
		List<Vector4> tempUVs;
		List<Color32> tempColors;

		#region Renderer initialization

		void InitRenderer () {
			// Init materials
			// triangle opaque & cutout must always be loaded to support custom voxel types
			matTriangleOpaque = Instantiate<Material> (Resources.Load<Material> ("VoxelPlay/Materials/VP Voxel Triangle Opaque"));
			matTriangleCutout = Instantiate<Material> (Resources.Load<Material> ("VoxelPlay/Materials/VP Voxel Triangle Cutout"));
			if (effectiveUseGeometryShaders) {
				matTerrainOpaque = Instantiate<Material> (Resources.Load<Material> ("VoxelPlay/Materials/VP Voxel Geo Opaque"));
				matTerrainCutout = Instantiate<Material> (Resources.Load<Material> ("VoxelPlay/Materials/VP Voxel Geo Cutout"));
				if (shadowsOnWater && !draftModeActive) {
					matTerrainWater = Instantiate<Material> (Resources.Load<Material> ("VoxelPlay/Materials/VP Voxel Geo Water"));
				} else {
					matTerrainWater = Instantiate<Material> (Resources.Load<Material> ("VoxelPlay/Materials/VP Voxel Geo Water No Shadows"));
				}
				matTerrainCutxss = Instantiate<Material> (Resources.Load<Material> ("VoxelPlay/Materials/VP Voxel Geo Cutout Cross"));
				matTerrainOpNoAO = Instantiate<Material> (Resources.Load<Material> ("VoxelPlay/Materials/VP Voxel Geo Opaque No AO"));
			} else {
				matTerrainOpaque = matTriangleOpaque;
				matTerrainCutout = matTriangleCutout;
				if (shadowsOnWater && !draftModeActive) {
					matTerrainWater = Instantiate<Material> (Resources.Load<Material> ("VoxelPlay/Materials/VP Voxel Triangle Water"));
				} else {
					matTerrainWater = Instantiate<Material> (Resources.Load<Material> ("VoxelPlay/Materials/VP Voxel Triangle Water No Shadows"));
				}
				matTerrainCutxss = Instantiate<Material> (Resources.Load<Material> ("VoxelPlay/Materials/VP Voxel Triangle Cutout Cross"));
				matTerrainOpNoAO = matTerrainOpaque;
			}

			matTerrainArray = new Material[32][];
			// Bit  Type
			// 1    Opaque
			// 2    Cutout
			// 3    Transp
			// 4    Cutout Cross
			// 5    Opaque No AO (ie. clouds)
			matTerrainArray [1] = new Material[] { matTerrainOpaque };
			matTerrainArray [2] = new Material[] { matTerrainCutout };
			matTerrainArray [3] = new Material[] {
				matTerrainOpaque,
				matTerrainCutout
			};
			matTerrainArray [4] = new Material[] { matTerrainWater };
			matTerrainArray [5] = new Material[] {
				matTerrainOpaque,
				matTerrainWater
			};
			matTerrainArray [6] = new Material[] {
				matTerrainCutout,
				matTerrainWater
			};
			matTerrainArray [7] = new Material[] {
				matTerrainOpaque,
				matTerrainCutout,
				matTerrainWater
			};
			matTerrainArray [8] = new Material[] { matTerrainCutxss };
			matTerrainArray [9] = new Material[] {
				matTerrainOpaque,
				matTerrainCutxss
			};
			matTerrainArray [10] = new Material[] {
				matTerrainCutout,
				matTerrainCutxss
			};
			matTerrainArray [11] = new Material[] {
				matTerrainOpaque,
				matTerrainCutout,
				matTerrainCutxss
			};
			matTerrainArray [12] = new Material[] {
				matTerrainWater,
				matTerrainCutxss
			};
			matTerrainArray [13] = new Material[] {
				matTerrainOpaque,
				matTerrainWater,
				matTerrainCutxss
			};
			matTerrainArray [14] = new Material[] {
				matTerrainCutout,
				matTerrainWater,
				matTerrainCutxss
			};
			matTerrainArray [15] = new Material[] {
				matTerrainOpaque,
				matTerrainCutout,
				matTerrainWater,
				matTerrainCutxss
			};
			matTerrainArray [16] = new Material[] { matTerrainOpNoAO };
			matTerrainArray [17] = new Material[] {
				matTerrainOpaque,
				matTerrainOpNoAO
			};
			matTerrainArray [18] = new Material[] {
				matTerrainCutout,
				matTerrainOpNoAO
			};
			matTerrainArray [19] = new Material[] {
				matTerrainOpaque,
				matTerrainCutout,
				matTerrainOpNoAO
			};
			matTerrainArray [20] = new Material[] {
				matTerrainWater,
				matTerrainOpNoAO
			};
			matTerrainArray [21] = new Material[] {
				matTerrainOpaque,
				matTerrainWater,
				matTerrainOpNoAO
			};
			matTerrainArray [22] = new Material[] {
				matTerrainCutout,
				matTerrainWater,
				matTerrainOpNoAO
			};
			matTerrainArray [23] = new Material[] {
				matTerrainOpaque,
				matTerrainCutout,
				matTerrainWater,
				matTerrainOpNoAO
			};
			matTerrainArray [24] = new Material[] {
				matTerrainCutxss,
				matTerrainOpNoAO
			};
			matTerrainArray [25] = new Material[] {
				matTerrainOpaque,
				matTerrainCutxss,
				matTerrainOpNoAO
			};
			matTerrainArray [26] = new Material[] {
				matTerrainCutout,
				matTerrainCutxss,
				matTerrainOpNoAO
			};
			matTerrainArray [27] = new Material[] {
				matTerrainOpaque,
				matTerrainCutout,
				matTerrainCutxss,
				matTerrainOpNoAO
			};
			matTerrainArray [28] = new Material[] {
				matTerrainWater,
				matTerrainCutxss,
				matTerrainOpNoAO
			};
			matTerrainArray [29] = new Material[] {
				matTerrainOpaque,
				matTerrainWater,
				matTerrainCutxss,
				matTerrainOpNoAO
			};
			matTerrainArray [30] = new Material[] {
				matTerrainCutout,
				matTerrainWater,
				matTerrainCutxss,
				matTerrainOpNoAO
			};
			matTerrainArray [31] = new Material[] {
				matTerrainOpaque,
				matTerrainCutout,
				matTerrainWater,
				matTerrainCutxss, matTerrainOpNoAO
			};
			modelMeshColors = new List<Color32> (128);
			tempVertices = new List<Vector3> (36);
			tempNormals = new List<Vector3> (36);
			tempUVs = new List<Vector4> (36);
			tempIndices = new int[36];
			tempColors = new List<Color32> (36);

			InitMeshJobsPool ();

			if (effectiveUseGeometryShaders) {
				int initialCapacity = 2048;
				opaqueIndices = new List<int> (initialCapacity);
				cutoutIndices = new List<int> (initialCapacity);
				transpIndices = new List<int> (initialCapacity);
				cutxssIndices = new List<int> (initialCapacity);
				opNoAOIndices = new List<int> (initialCapacity);
			}

			Voxel.Empty.light = noLightValue;
			InitVirtualChunk ();

			greedyCollider = new VoxelPlayGreedyMesher ();
			greedyNavMesh = new VoxelPlayGreedyMesher ();
			greedyNoAO = new VoxelPlayGreedyMesher (true);
			greedyCutout = new VoxelPlayGreedyMesher (true);

			if (effectiveMultithreadGeneration) {
				generationThreadRunning = true;
				waitEvent = new AutoResetEvent (false);
				meshGenerationThread = new Thread (GenerateChunkMeshDataInBackgroundThread);
				meshGenerationThread.Start ();
			}


		}

	
		void InitVirtualChunk () {
			chunk9 = new Voxel[27][];
			emptyChunkUnderground = new Voxel[16 * 16 * 16];
			emptyChunkAboveTerrain = new Voxel[16 * 16 * 16];
			for (int k = 0; k < emptyChunkAboveTerrain.Length; k++) {
				emptyChunkAboveTerrain [k].light = (byte)15;

				emptyChunkUnderground [k].hasContent = 1;
				emptyChunkUnderground [k].opaque = FULL_OPAQUE;
			}

			virtualChunk = new VirtualVoxel[18 * 18 * 18];

			int index = 0;
			for (int y = 0; y < 18; y++) {
				for (int z = 0; z < 18; z++) {
					for (int x = 0; x < 18; x++, index++) {
						int vy = 1, vz = 1, vx = 1;
						if (y == 0) {
							vy = 0;
						} else if (y == 17) {
							vy = 2;
						} 
						if (z == 0) {
							vz = 0;
						} else if (z == 17) {
							vz = 2;
						}
						if (x == 0) {
							vx = 0;
						} else if (x == 17) {
							vx = 2;
						}
						virtualChunk [index].chunk9Index = vy * 9 + vz * 3 + vx;
						int py = (y + 15) % 16;
						int pz = (z + 15) % 16;
						int px = (x + 15) % 16;
						virtualChunk [index].voxelIndex = py * ONE_Y_ROW + pz * ONE_Z_ROW + px;
					}
				}
			}
		}

		void InitMeshJobsPool () {

			#if UNITY_WEBGL
			meshJobs = new MeshJobData[384];
			#else
			meshJobs = new MeshJobData[isMobilePlatform ? 256 : MESH_JOBS_POOL_SIZE];
			#endif
			meshJobMeshLastIndex = -1;
			meshJobMeshDataGenerationIndex = -1;
			meshJobMeshDataGenerationReadyIndex = -1;
			meshJobMeshUploadIndex = -1;
			int initialCapacity = effectiveUseGeometryShaders ? 2048 : 20000;
			for (int k = 0; k < meshJobs.Length; k++) {
				meshJobs [k].vertices = new List<Vector3> (initialCapacity);
				meshJobs [k].uv0 = new List<Vector4> (initialCapacity);
				meshJobs [k].uv2 = new List<Vector4> (initialCapacity);
				meshJobs [k].colors = new List<Color32> (initialCapacity);
				meshJobs [k].normals = new List<Vector3> (initialCapacity);
				if (!effectiveUseGeometryShaders) {
					meshJobs [k].opaqueIndices = new List<int> (20000);
					meshJobs [k].cutoutIndices = new List<int> (20000);
					meshJobs [k].transpIndices = new List<int> (5000);
					meshJobs [k].cutxssIndices = new List<int> (2000);
				}
				if (enableColliders) {
					meshJobs [k].colliderVertices = new List<Vector3> (2700);
					meshJobs [k].colliderIndices = new List<int> (4000);
				}
				if (enableNavMesh) {
					meshJobs [k].navMeshVertices = new List<Vector3> (2700);
					meshJobs [k].navMeshIndices = new List<int> (4000);
				}
				meshJobs [k].mivs = new List<ModelInVoxel> ();

			}
		}

		#endregion


		#region Rendering


		public void UpdateMaterialProperties () {

			if (matTerrainOpaque == null)
				return;

			if (draftModeActive || !enableAmbientOcclusion) {
				matTerrainOpaque.DisableKeyword (SKW_VOXELPLAY_USE_AO);
				matTerrainCutout.DisableKeyword (SKW_VOXELPLAY_USE_AO);
			} else {
				matTerrainOpaque.EnableKeyword (SKW_VOXELPLAY_USE_AO);
				matTerrainCutout.EnableKeyword (SKW_VOXELPLAY_USE_AO);
			}
			if (enableTinting) {
				matTerrainOpaque.EnableKeyword (SKW_VOXELPLAY_USE_TINTING);
				matTriangleOpaque.EnableKeyword (SKW_VOXELPLAY_USE_TINTING);
				matTriangleCutout.EnableKeyword (SKW_VOXELPLAY_USE_TINTING);
			} else {
				matTerrainOpaque.DisableKeyword (SKW_VOXELPLAY_USE_TINTING);
				matTriangleOpaque.DisableKeyword (SKW_VOXELPLAY_USE_TINTING);
				matTriangleCutout.DisableKeyword (SKW_VOXELPLAY_USE_TINTING);
			}
			if (enableFogSkyBlending) {
				Shader.EnableKeyword (SKW_VOXELPLAY_GLOBAL_USE_FOG);
			} else {
				Shader.DisableKeyword (SKW_VOXELPLAY_GLOBAL_USE_FOG);
			}
			if (hqFiltering) {
				matTerrainOpaque.EnableKeyword (SKW_VOXELPLAY_AA_TEXELS);
				matTerrainCutout.EnableKeyword (SKW_VOXELPLAY_AA_TEXELS);
				matTerrainCutxss.EnableKeyword (SKW_VOXELPLAY_AA_TEXELS);
				matTerrainOpNoAO.EnableKeyword (SKW_VOXELPLAY_AA_TEXELS);
				matTerrainWater.EnableKeyword (SKW_VOXELPLAY_AA_TEXELS);
				matTriangleOpaque.EnableKeyword (SKW_VOXELPLAY_AA_TEXELS);
				matTriangleCutout.EnableKeyword (SKW_VOXELPLAY_AA_TEXELS);
			} else {
				matTerrainOpaque.DisableKeyword (SKW_VOXELPLAY_AA_TEXELS);
				matTerrainCutout.DisableKeyword (SKW_VOXELPLAY_AA_TEXELS);
				matTerrainCutxss.DisableKeyword (SKW_VOXELPLAY_AA_TEXELS);
				matTerrainOpNoAO.DisableKeyword (SKW_VOXELPLAY_AA_TEXELS);
				matTerrainWater.DisableKeyword (SKW_VOXELPLAY_AA_TEXELS);
				matTriangleOpaque.DisableKeyword (SKW_VOXELPLAY_AA_TEXELS);
				matTriangleCutout.DisableKeyword (SKW_VOXELPLAY_AA_TEXELS);
			}

			Vector3 fogData = new Vector3 (fogDistance * fogDistance, fogFalloff * fogFalloff, 0);

			// Update terrain materials
			UpdateTerrainMaterial (matTerrainOpaque, fogData);
			UpdateTerrainMaterial (matTerrainCutout, fogData);
			UpdateTerrainMaterial (matTerrainWater, fogData);
			UpdateTerrainMaterial (matTerrainCutxss, fogData);

			// Update skybox material
			VOXELPLAY_SKYBOX worldSkybox = isMobilePlatform ? world.skyboxMobile : world.skyboxDesktop;

			if (worldSkybox != VOXELPLAY_SKYBOX.UserDefined) {
				if (skyboxMaterial != RenderSettings.skybox) {
					switch (worldSkybox) {
					case VOXELPLAY_SKYBOX.Earth:
						if (skyboxEarth == null) {
							skyboxEarth = Resources.Load<Material> ("VoxelPlay/Materials/VP Skybox Earth");
						}
						world.skyTint = new Color (0.52f, 0.5f, 1f); // default color for this style
						skyboxMaterial = skyboxEarth;
						break;
					case VOXELPLAY_SKYBOX.EarthSimplified:
						if (skyboxEarthSimplified == null) {
							skyboxEarthSimplified = Resources.Load<Material> ("VoxelPlay/Materials/VP Skybox Earth Simplified");
						}
						world.skyTint = new Color (0.52f, 0.5f, 1f); // default color for this style
						skyboxMaterial = skyboxEarthSimplified;
						break;
					case VOXELPLAY_SKYBOX.Space:
						if (skyboxSpace == null) {
							skyboxSpace = Resources.Load<Material> ("VoxelPlay/Materials/VP Skybox Space");
						}
						world.skyTint = Color.black;
						skyboxMaterial = skyboxSpace;
						break;
					case VOXELPLAY_SKYBOX.EarthNightCubemap:
						if (skyboxEarthNightCube == null) {
							skyboxEarthNightCube = Resources.Load<Material> ("VoxelPlay/Materials/VP Skybox Earth Night Cubemap");
						}
						world.skyTint = new Color (0.52f, 0.5f, 1f); // default color for this style
						if (world.skyboxNightCubemap != null)
							skyboxEarthNightCube.SetTexture ("_NightTex", world.skyboxNightCubemap);
						skyboxMaterial = skyboxEarthNightCube;
						break;
					case VOXELPLAY_SKYBOX.EarthDayNightCubemap:
						if (skyboxEarthDayNightCube == null) {
							skyboxEarthDayNightCube = Resources.Load<Material> ("VoxelPlay/Materials/VP Skybox Earth Day Night Cubemap");
						}
						world.skyTint = new Color (0.52f, 0.5f, 1f); // default color for this style
						if (world.skyboxDayCubemap != null)
							skyboxEarthDayNightCube.SetTexture ("_DayTex", world.skyboxDayCubemap);
						if (world.skyboxNightCubemap != null)
							skyboxEarthDayNightCube.SetTexture ("_NightTex", world.skyboxNightCubemap);
						skyboxMaterial = skyboxEarthDayNightCube;
						break;
					}
					RenderSettings.skybox = skyboxMaterial;
				}

				skyboxMaterial.SetColor ("_SkyTint", world.skyTint);
				skyboxMaterial.SetFloat ("_FogAmount", fogAmount);
				skyboxMaterial.SetFloat ("_Exposure", world.exposure);
			}
		}

		void UpdateTerrainMaterial (Material mat, Vector3 fogData) {
			mat.SetColor ("_SkyTint", world.skyTint);
			mat.SetFloat ("_FogAmount", fogAmount);
			mat.SetFloat ("_Exposure", world.exposure);
			mat.SetVector ("_FogData", fogData);
			mat.SetFloat ("_AmbientLight", ambientLight);
		}

		public void GetMeshJobsStatus (out int lastJobIndex, out int currentGenerationJobIndex, out int currentMeshUploadIndex) {
			lastJobIndex = meshJobMeshLastIndex;
			currentGenerationJobIndex = meshJobMeshDataGenerationReadyIndex;
			currentMeshUploadIndex = meshJobMeshUploadIndex;
		}

		bool CreateChunkMeshJob (VoxelChunk chunk) {
			int newJobIndex = meshJobMeshLastIndex + 1;
			if (newJobIndex >= meshJobs.Length) {
				newJobIndex = 0;
			}
			if (newJobIndex == meshJobMeshDataGenerationIndex || newJobIndex == meshJobMeshUploadIndex) {
				ShowMessage ("Exhausted mesh job queue!");
				return false;
			}
			lock (indicesUpdating) {
				meshJobs [newJobIndex].chunk = chunk;
				meshJobMeshLastIndex = newJobIndex;
				if (generationThreadRunning)
					waitEvent.Set ();
			}
			return true;
		}


		void GenerateChunkMeshDataInBackgroundThread () {
			try {
				while (generationThreadRunning) {
					bool idle;
					lock (indicesUpdating) {
						idle = meshJobMeshDataGenerationIndex == meshJobMeshLastIndex;
					}
					if (idle) {
						waitEvent.WaitOne ();
						//Thread.Sleep (0); 
						continue;
					}
					GenerateChunkMeshDataOneJob ();
					lock (indicesUpdating) {
						meshJobMeshDataGenerationReadyIndex = meshJobMeshDataGenerationIndex;
					}
				}
			} catch (Exception ex) {
				ShowExceptionMessage (ex);
			}
		}


		void GenerateChunkMeshDataInMainThread (long endTime) {

			long elapsed;
			do {
				if (meshJobMeshDataGenerationIndex == meshJobMeshLastIndex)
					return;
				GenerateChunkMeshDataOneJob ();
				meshJobMeshDataGenerationReadyIndex = meshJobMeshDataGenerationIndex;
				elapsed = stopWatch.ElapsedMilliseconds;
			} while (elapsed < endTime);
		}

		void GenerateChunkMeshDataOneJob () {
			meshJobMeshDataGenerationIndex++;
			if (meshJobMeshDataGenerationIndex >= meshJobs.Length) {
				meshJobMeshDataGenerationIndex = 0;
			}

			VoxelChunk chunk = meshJobs [meshJobMeshDataGenerationIndex].chunk;
			chunk9 [13] = chunk.voxels;

			Voxel[] emptyChunk = chunk.isAboveSurface ? emptyChunkAboveTerrain : emptyChunkUnderground;
			int chunkX = FastMath.FloorToInt (chunk.position.x / 16);
			int chunkY = FastMath.FloorToInt (chunk.position.y / 16);
			int chunkZ = FastMath.FloorToInt (chunk.position.z / 16);
			for (int y = -1; y <= 1; y++) {
				int yy = chunkY + y;
				for (int z = -1; z <= 1; z++) {
					int zz = chunkZ + z;
					for (int x = -1; x <= 1; x++) {
						if (y == 0 && z == 0 && x == 0)
							continue;
						int xx = chunkX + x;
						VoxelChunk neighbour;
						if (GetChunkFast (xx, yy, zz, out neighbour, false) && (neighbour.isPopulated || neighbour.isRendered)) {
							chunk9 [(y + 1) * 9 + (z + 1) * 3 + x + 1] = neighbour.voxels;
						} else {
							chunk9 [(y + 1) * 9 + (z + 1) * 3 + x + 1] = emptyChunk;
						}
					}
				}
			}

			if (effectiveUseGeometryShaders) {
				GenerateMeshData_Geo (meshJobMeshDataGenerationIndex);
			} else {
				GenerateMeshData_Triangle (meshJobMeshDataGenerationIndex);
			}
		}

		void UploadMeshData (int jobIndex) {

			VoxelChunk chunk = meshJobs [jobIndex].chunk;

			if (meshJobs [jobIndex].totalVisibleVoxels == 0) {
				if (chunk.mf.sharedMesh != null) {
					chunk.mf.sharedMesh.Clear (false);
				}
				chunk.mc.enabled = false;
				chunk.renderState = CHUNK_RENDER_STATE.RenderingComplete;
				return;
			}

			// Create mesh
			#if !UNITY_EDITOR
			Mesh mesh;
			if (isMobilePlatform) {
			mesh = new Mesh (); // on mobile will be released mesh data upon uploading to the GPU so the mesh is no longer readable; need to recreate it everytime the chunk is rendered
			} else {
			mesh = chunk.mf.sharedMesh;
			if (mesh == null) {
			mesh = new Mesh ();
			} else {
			mesh.Clear ();
			}
			}
			#else
			Mesh mesh = chunk.mf.sharedMesh;
			if (mesh == null) {
				mesh = new Mesh ();
				chunksDrawn++;
			} else {
				voxelsCreatedCount -= mesh.vertexCount;
				mesh.Clear ();
			}
			voxelsCreatedCount += meshJobs [jobIndex].totalVisibleVoxels;
			#endif

			// Vertices
			mesh.SetVertices (meshJobs [jobIndex].vertices);

			// UVs, normals, colors
			mesh.SetUVs (0, meshJobs [jobIndex].uv0);
			if (effectiveUseGeometryShaders) {
				mesh.SetUVs (1, meshJobs [jobIndex].uv2);
			} else {
				mesh.SetNormals (meshJobs [jobIndex].normals);
			}
			if (enableTinting) {
				mesh.SetColors (meshJobs [jobIndex].colors);
			}

			// Assign materials and submeshes
			int subMeshIndex = -1;
			mesh.subMeshCount = meshJobs [jobIndex].subMeshCount;
			if (mesh.subMeshCount > 0) {
				if (effectiveUseGeometryShaders) {
					if (meshJobs [jobIndex].opaqueVoxelsCount > 0) {
						mesh.SetIndices (meshJobs [jobIndex].opaqueIndicesArray, MeshTopology.Points, ++subMeshIndex, false);
					}
					if (meshJobs [jobIndex].cutoutVoxelsCount > 0) {
						mesh.SetIndices (meshJobs [jobIndex].cutoutIndicesArray, MeshTopology.Points, ++subMeshIndex, false);
					}
					if (meshJobs [jobIndex].transpVoxelsCount > 0) {
						mesh.SetIndices (meshJobs [jobIndex].transpIndicesArray, MeshTopology.Points, ++subMeshIndex, false);
					}
					if (meshJobs [jobIndex].cutxssVoxelsCount > 0) {
						mesh.SetIndices (meshJobs [jobIndex].cutxssIndicesArray, MeshTopology.Points, ++subMeshIndex, false);
					}
					if (meshJobs [jobIndex].opNoAOVoxelsCount > 0) {
						mesh.SetIndices (meshJobs [jobIndex].opNoAOIndicesArray, MeshTopology.Points, ++subMeshIndex, false);
					}
				} else {
					if (meshJobs [jobIndex].opaqueVoxelsCount > 0) {
						mesh.SetTriangles (meshJobs [jobIndex].opaqueIndices, ++subMeshIndex, false);
					}
					if (meshJobs [jobIndex].cutoutVoxelsCount > 0) {
						mesh.SetTriangles (meshJobs [jobIndex].cutoutIndices, ++subMeshIndex, false);
					}
					if (meshJobs [jobIndex].transpVoxelsCount > 0) {
						mesh.SetTriangles (meshJobs [jobIndex].transpIndices, ++subMeshIndex, false);
					}
					if (meshJobs [jobIndex].cutxssVoxelsCount > 0) {
						mesh.SetTriangles (meshJobs [jobIndex].cutxssIndices, ++subMeshIndex, false);
					}
				}
				chunk.mr.sharedMaterials = matTerrainArray [meshJobs [jobIndex].matVoxelIndex];

				mesh.bounds = Misc.bounds16;

				chunk.mf.sharedMesh = mesh;

				#if !UNITY_EDITOR
			if (isMobilePlatform) {
				mesh.UploadMeshData (true);
			}
				#endif

				if (!chunk.mr.enabled) {
					chunk.mr.enabled = true;
				}
			}

			// Update collider?
			if (enableColliders && chunk.needsColliderRebuild) {
				int colliderVerticesCount = meshJobs [jobIndex].colliderVertices.Count;
				Mesh colliderMesh = chunk.mc.sharedMesh;
				if (colliderVerticesCount == 0 || !applicationIsPlaying) {
					chunk.mc.enabled = false;
				} else {
					if (colliderMesh == null) {
						colliderMesh = new Mesh ();
					} else {
						colliderMesh.Clear ();
					}
					colliderMesh.SetVertices (meshJobs [jobIndex].colliderVertices);
					colliderMesh.SetTriangles (meshJobs [jobIndex].colliderIndices, 0);
					chunk.mc.sharedMesh = colliderMesh;
					chunk.mc.enabled = true;
				}

				// Update navmesh
				if (enableNavMesh) {
					int navMeshVerticesCount = meshJobs [jobIndex].navMeshVertices.Count;
					Mesh navMesh = chunk.navMesh;
					if (navMesh == null) {
						navMesh = new Mesh ();
					} else {
						navMesh.Clear ();
					}
					navMesh.SetVertices (meshJobs [jobIndex].navMeshVertices);
					navMesh.SetTriangles (meshJobs [jobIndex].navMeshIndices, 0);
					chunk.navMesh = navMesh;
					AddChunkNavMesh (chunk);
				}
			}

			if (chunk.renderState != CHUNK_RENDER_STATE.RenderingComplete) {
				chunk.renderState = CHUNK_RENDER_STATE.RenderingComplete;
				if (OnChunkAfterFirstRender != null) {
					OnChunkAfterFirstRender (chunk);
				}
			}

			RenderModelsInVoxels (chunk, meshJobs [jobIndex].mivs);

			shouldUpdateParticlesLighting = true;
		}


		void RenderModelsInVoxels (VoxelChunk chunk, List<ModelInVoxel> mivs) {

			int count = mivs.Count;
			Vector3 pos;
			for (int k = 0; k < count; k++) {
				ModelInVoxel miv = mivs [k];
				VoxelDefinition voxelDefinition = miv.vd;

				VoxelPlaceholder placeholder = GetVoxelPlaceholder (chunk, miv.voxelIndex, true);
				bool createModel = true;
				if (placeholder.modelInstance != null) {
					if (placeholder.modelTemplate != voxelDefinition.model) {
						DestroyImmediate (placeholder.modelInstance);
					} else {
						createModel = false;
					}
				}
				MeshFilter mf;
				Mesh mesh;

				if (createModel || placeholder.modelMeshFilter == null || placeholder.modelMeshFilter.sharedMesh == null) {
					placeholder.modelTemplate = voxelDefinition.model;
					placeholder.modelInstance = Instantiate (voxelDefinition.model);
					mf = placeholder.modelMeshFilter = placeholder.modelInstance.GetComponent<MeshFilter> ();
					mesh = mf.sharedMesh = Instantiate<Mesh> (mf.sharedMesh);
					mesh.hideFlags = HideFlags.DontSave;

					// Custom position
					pos = placeholder.transform.position;
					pos.x += voxelDefinition.offset.x;
					pos.y += voxelDefinition.offset.y;
					pos.z += voxelDefinition.offset.z;
					placeholder.transform.position = pos;

					// Custom scale
					Bounds bounds = mesh.bounds;
					bounds.center = voxelDefinition.offset;
					bounds.size = new Vector3 (bounds.size.x * voxelDefinition.scale.x, bounds.size.y * voxelDefinition.scale.y, bounds.size.z * voxelDefinition.scale.z);
					placeholder.bounds = bounds;

					// Custom rotation
					placeholder.transform.localRotation = Quaternion.Euler (voxelDefinition.rotation.x, voxelDefinition.rotation.y, voxelDefinition.rotation.z);
				} else {
					mf = placeholder.modelMeshFilter;
					mesh = mf.sharedMesh;
				}

				Transform tModel = placeholder.modelInstance.transform;
				tModel.SetParent (placeholder.transform, false);

				if (effectiveGlobalIllumination || chunk.voxels [miv.voxelIndex].isColored) {
					// Update mesh colors
					float voxelLight = chunk.voxels [miv.voxelIndex].lightMesh / 15f;
					Color32 color = chunk.voxels [miv.voxelIndex].color;
					color.r = (byte)(color.r * voxelLight);
					color.g = (byte)(color.g * voxelLight);
					color.b = (byte)(color.b * voxelLight);
					modelMeshColors.Clear ();
					for (int c = 0; c < mesh.vertexCount; c++) {
						modelMeshColors.Add (color);
					}
					mesh.SetColors (modelMeshColors);
					mesh.UploadMeshData (false);
				}

				if (!tModel.gameObject.activeSelf)
					tModel.gameObject.SetActive (true);

			}

		}

		#endregion
	
	}



}
