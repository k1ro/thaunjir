﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.Rendering;

namespace VoxelPlay {

	public partial class VoxelPlayEnvironment : MonoBehaviour {

		/// <summary>
		/// Uses geometry-shader-based materials and mesh to render the chunk
		/// </summary>
		void GenerateMeshData_Geo (int jobIndex) {

			VoxelChunk chunk = meshJobs [jobIndex].chunk;
			tempChunkVertices = meshJobs [jobIndex].vertices;
			tempChunkUV0 = meshJobs [jobIndex].uv0;
			tempChunkUV2 = meshJobs [jobIndex].uv2;
			tempChunkColors32 = meshJobs [jobIndex].colors;
			meshColliderVertices = meshJobs [jobIndex].colliderVertices;
			meshColliderIndices = meshJobs [jobIndex].colliderIndices;
			navMeshVertices = meshJobs [jobIndex].navMeshVertices;
			navMeshIndices = meshJobs [jobIndex].navMeshIndices;
			mivs = meshJobs [jobIndex].mivs;

			tempChunkVertices.Clear ();
			tempChunkUV0.Clear ();
			tempChunkColors32.Clear ();
			if (enableColliders) {
				meshColliderIndices.Clear ();
				meshColliderVertices.Clear ();
				if (enableNavMesh) {
					navMeshIndices.Clear ();
					navMeshVertices.Clear ();
				}
			}
			opaqueIndices.Clear ();
			cutoutIndices.Clear ();
			transpIndices.Clear ();
			cutxssIndices.Clear ();
			opNoAOIndices.Clear ();	
			mivs.Clear ();

			int opaqueVoxelsCount = 0;
			int cutoutVoxelsCount = 0;
			int transpVoxelsCount = 0;
			int cutxssVoxelsCount = 0;
			int opNoAOVoxelsCount = 0;
			Voxel[] voxels = chunk.voxels;
			Vector4 uvAux, uv2Aux;
			tempChunkUV2.Clear ();

			int chunkUVIndex = -1;
			int chunkVoxelCount = 0;
			Color32 tintColor = Misc.color32White;
			Vector3 pos = Misc.vector3zero;
			Vector3 offsetPos = Misc.vector3zero;
			ModelInVoxel miv = new ModelInVoxel ();

			const int V_ONE_Y_ROW = 18 * 18;
			const int V_ONE_Z_ROW = 18;
		
			int voxelIndex = 0;
			int voxelSignature = 0;
			for (int y = 0; y < 16; y++) {
				int vy = (y + 1) * 18 * 18;
				for (int z = 0; z < 16; z++) {
					int vyz = vy + (z + 1) * 18;
					for (int x = 0; x < 16; x++, voxelIndex++) {
						voxels [voxelIndex].lightMesh = voxels [voxelIndex].light;
						if (voxels [voxelIndex].hasContent != 1)
							continue;
		
						int vxyz = vyz + x + 1;

						int vindex = vxyz - 1;
						Voxel[] chunk_middle_middle_left = chunk9 [virtualChunk [vindex].chunk9Index];
						int middle_middle_left = virtualChunk [vindex].voxelIndex;

						vindex = vxyz + 1;
						Voxel[] chunk_middle_middle_right = chunk9 [virtualChunk [vindex].chunk9Index];
						int middle_middle_right = virtualChunk [vindex].voxelIndex;

						vindex = vxyz + V_ONE_Y_ROW;
						Voxel[] chunk_top_middle_middle = chunk9 [virtualChunk [vindex].chunk9Index];
						int top_middle_middle = virtualChunk [vindex].voxelIndex;

						vindex = vxyz - V_ONE_Y_ROW;
						Voxel[] chunk_bottom_middle_middle = chunk9 [virtualChunk [vindex].chunk9Index];
						int bottom_middle_middle = virtualChunk [vindex].voxelIndex;

						vindex = vxyz + V_ONE_Z_ROW;
						Voxel[] chunk_middle_forward_middle = chunk9 [virtualChunk [vindex].chunk9Index];
						int middle_forward_middle = virtualChunk [vindex].voxelIndex;

						vindex = vxyz - V_ONE_Z_ROW;
						Voxel[] chunk_middle_back_middle = chunk9 [virtualChunk [vindex].chunk9Index];
						int middle_back_middle = virtualChunk [vindex].voxelIndex;

						// If voxel is surrounded by material, don't render
						int v1u = chunk_top_middle_middle [top_middle_middle].opaque;
						int v1f = chunk_middle_forward_middle [middle_forward_middle].opaque;
						int v1b = chunk_middle_back_middle [middle_back_middle].opaque;
						int v1l = chunk_middle_middle_left [middle_middle_left].opaque;
						int v1r = chunk_middle_middle_right [middle_middle_right].opaque;
						int v1d = chunk_bottom_middle_middle [bottom_middle_middle].opaque; 
						if (v1u + v1f + v1b + v1l + v1r + v1d == 90) // 90 = 15 * 6
							continue;
//						if (chunk_middle_middle_left [middle_middle_left].opaque == FULL_OPAQUE &&
//						    chunk_middle_middle_right [middle_middle_right].opaque == FULL_OPAQUE &&
//						    chunk_top_middle_middle [top_middle_middle].opaque == FULL_OPAQUE &&
//						    chunk_bottom_middle_middle [bottom_middle_middle].opaque == FULL_OPAQUE &&
//						    chunk_middle_forward_middle [middle_forward_middle].opaque == FULL_OPAQUE &&
//						    chunk_middle_back_middle [middle_back_middle].opaque == FULL_OPAQUE)
//							continue;

						// top
						vindex = vxyz + V_ONE_Y_ROW + V_ONE_Z_ROW - 1;
						Voxel[] chunk_top_forward_left = chunk9 [virtualChunk [vindex].chunk9Index];
						int top_forward_left = virtualChunk [vindex].voxelIndex;

						vindex++;
						Voxel[] chunk_top_forward_middle = chunk9 [virtualChunk [vindex].chunk9Index];
						int top_forward_middle = virtualChunk [vindex].voxelIndex;

						vindex++;
						Voxel[] chunk_top_forward_right = chunk9 [virtualChunk [vindex].chunk9Index];
						int top_forward_right = virtualChunk [vindex].voxelIndex;

						vindex = vxyz + V_ONE_Y_ROW - 1;
						Voxel[] chunk_top_middle_left = chunk9 [virtualChunk [vindex].chunk9Index];
						int top_middle_left = virtualChunk [vindex].voxelIndex;

						vindex += 2;
						Voxel[] chunk_top_middle_right = chunk9 [virtualChunk [vindex].chunk9Index];
						int top_middle_right = virtualChunk [vindex].voxelIndex;

						vindex = vxyz + V_ONE_Y_ROW - V_ONE_Z_ROW - 1;
						Voxel[] chunk_top_back_left = chunk9 [virtualChunk [vindex].chunk9Index];
						int top_back_left = virtualChunk [vindex].voxelIndex;

						vindex++;
						Voxel[] chunk_top_back_middle = chunk9 [virtualChunk [vindex].chunk9Index];
						int top_back_middle = virtualChunk [vindex].voxelIndex;

						vindex++;
						Voxel[] chunk_top_back_right = chunk9 [virtualChunk [vindex].chunk9Index];
						int top_back_right = virtualChunk [vindex].voxelIndex;

						// middle
						vindex = vxyz + V_ONE_Z_ROW - 1;
						Voxel[] chunk_middle_forward_left = chunk9 [virtualChunk [vindex].chunk9Index];
						int middle_forward_left = virtualChunk [vindex].voxelIndex;

						vindex += 2;
						Voxel[] chunk_middle_forward_right = chunk9 [virtualChunk [vindex].chunk9Index];
						int middle_forward_right = virtualChunk [vindex].voxelIndex;

						vindex = vxyz - V_ONE_Z_ROW - 1;
						Voxel[] chunk_middle_back_left = chunk9 [virtualChunk [vindex].chunk9Index];
						int middle_back_left = virtualChunk [vindex].voxelIndex;

						vindex += 2;
						Voxel[] chunk_middle_back_right = chunk9 [virtualChunk [vindex].chunk9Index];
						int middle_back_right = virtualChunk [vindex].voxelIndex;

						// bottom
						vindex = vxyz - V_ONE_Y_ROW + V_ONE_Z_ROW - 1;
						Voxel[] chunk_bottom_forward_left = chunk9 [virtualChunk [vindex].chunk9Index];
						int bottom_forward_left = virtualChunk [vindex].voxelIndex;

						vindex++;
						Voxel[] chunk_bottom_forward_middle = chunk9 [virtualChunk [vindex].chunk9Index];
						int bottom_forward_middle = virtualChunk [vindex].voxelIndex;

						vindex++;
						Voxel[] chunk_bottom_forward_right = chunk9 [virtualChunk [vindex].chunk9Index];
						int bottom_forward_right = virtualChunk [vindex].voxelIndex;

						vindex = vxyz - V_ONE_Y_ROW - 1;
						Voxel[] chunk_bottom_middle_left = chunk9 [virtualChunk [vindex].chunk9Index];
						int bottom_middle_left = virtualChunk [vindex].voxelIndex;

						vindex += 2;
						Voxel[] chunk_bottom_middle_right = chunk9 [virtualChunk [vindex].chunk9Index];
						int bottom_middle_right = virtualChunk [vindex].voxelIndex;

						vindex = vxyz - V_ONE_Y_ROW - V_ONE_Z_ROW - 1;
						Voxel[] chunk_bottom_back_left = chunk9 [virtualChunk [vindex].chunk9Index];
						int bottom_back_left = virtualChunk [vindex].voxelIndex;

						vindex++;
						Voxel[] chunk_bottom_back_middle = chunk9 [virtualChunk [vindex].chunk9Index];
						int bottom_back_middle = virtualChunk [vindex].voxelIndex;

						vindex++;
						Voxel[] chunk_bottom_back_right = chunk9 [virtualChunk [vindex].chunk9Index];
						int bottom_back_right = virtualChunk [vindex].voxelIndex;

		
						pos.y = y - 7.5f;
						pos.z = z - 7.5f;
						pos.x = x - 7.5f;

						voxelSignature += voxelIndex;
						chunkVoxelCount++;

						VoxelDefinition type = voxelDefinitions [voxels [voxelIndex].typeIndex];
		
						uvAux.x = type.textureIndexSide;
						uvAux.y = type.textureIndexTop;
						uvAux.z = type.textureIndexBottom;
						uvAux.w = 0;
		
						int occlusionX = 0;
						int occlusionY = 0;
						int occlusionZ = 0;
						int occlusionW = 0;
						int occ = 0;
		
						RenderType rt = type.renderType;
						switch (rt) {
						case RenderType.Transparent:
							++chunkUVIndex;
							transpVoxelsCount++;

							transpIndices.Add (chunkUVIndex);
		
							uvAux.w = voxels [voxelIndex].light / 15f;
		
							int hf = chunk_middle_forward_middle [middle_forward_middle].waterLevel;
							int hb = chunk_middle_back_middle [middle_back_middle].waterLevel;
							int hr = chunk_middle_middle_right [middle_middle_right].waterLevel;
							int hl = chunk_middle_middle_left [middle_middle_left].waterLevel;

								// compute water neighbours and account for foam
								// back
							occ = chunk_middle_back_middle [middle_back_middle].hasContent;
							if (occ != 0) {
								// occlusionX bit = 0 means that face is visible; this occurs if next voxel is water BUT voxel below that neighour voxel has contents, otherwise it needs to be
								// visible because water voxels are slightly displaced down
								if (hb > 0 && chunk_bottom_back_middle [bottom_back_middle].hasContent != 0) {
									occlusionX |= 1;
								}
								if (hb == 0) {
									occlusionY += 1;
								}
							}
											
								// front
							occ = chunk_middle_forward_middle [middle_forward_middle].hasContent;
							if (occ != 0) {
								if (hf > 0 && chunk_bottom_forward_middle [bottom_forward_middle].hasContent != 0) {
									occlusionX |= (1 << 1);
								}
								if (hf == 0) {
									occlusionY += 2;
								}
							}
								// up
							occlusionX += ((chunk_top_middle_middle [top_middle_middle].hasContent & 1) << 2);
								// down
							occlusionX += ((chunk_bottom_middle_middle [bottom_middle_middle].hasContent & 1) << 3);

								// left
							occ = chunk_middle_middle_left [middle_middle_left].hasContent;
							if (occ != 0) {
								if (hl > 0 && chunk_bottom_middle_left [bottom_middle_left].hasContent != 0) {
									occlusionX |= (1 << 4);
								}
								if (hl == 0) {
									occlusionY += 4;
								}
							}
								// right
							occ = chunk_middle_middle_right [middle_middle_right].hasContent;
							if (occ != 0) {
								if (hr > 0 && chunk_bottom_middle_right [bottom_middle_right].hasContent != 0) {
									occlusionX += (1 << 5);
								}
								if (hr == 0) {
									occlusionY += 8;
								}
							}

								// if there's something on top, no foam
							if (chunk_top_middle_middle [top_middle_middle].hasContent == 1) {
								if (chunk_top_back_middle [top_back_middle].hasContent != 1 || chunk_top_forward_middle [top_forward_middle].hasContent != 1 ||
								    chunk_top_middle_left [top_middle_left].hasContent != 1 || chunk_top_middle_right [top_middle_right].hasContent != 1) {
									occlusionY = 0;
								}
							}

							// Get corners heights
							// If there's water on top, full size
							if (chunk_top_middle_middle [top_middle_middle].waterLevel > 0) {
								occlusionW = 15 + (15 << 4) + (15 << 8) + (15 << 12); // full height
								occlusionY += (1 << 4) + (1 << 6);	// neutral/no flow
							} else {
								int h = voxels [voxelIndex].waterLevel;
								int hfr = chunk_middle_forward_right [middle_forward_right].waterLevel;
								int hbr = chunk_middle_back_right [middle_back_right].waterLevel;
								int hbl = chunk_middle_back_left [middle_back_left].waterLevel;
								int hfl = chunk_middle_forward_left [middle_forward_left].waterLevel;

								int tf = chunk_top_forward_middle [top_forward_middle].waterLevel;
								int tfr = chunk_top_forward_right [top_forward_right].waterLevel;
								int tr = chunk_top_middle_right [top_middle_right].waterLevel;
								int tbr = chunk_top_back_right [top_back_right].waterLevel;
								int tb = chunk_top_back_middle [top_back_middle].waterLevel;
								int tbl = chunk_top_back_left [top_back_left].waterLevel;
								int tl = chunk_top_middle_left [top_middle_left].waterLevel;
								int tfl = chunk_top_forward_left [top_forward_left].waterLevel;

								// forward right corner
								if ( tf * hf + tfr * hfr + tr * hr > 0) {
									hfr = 15;
								} else {
									hfr = h > hfr ? h: hfr;
									if (hf > hfr)
										hfr = hf;
									if (hr > hfr)
										hfr = hr;
								}
								// bottom right corner
								if ( tr * hr + tbr * hbr + tb * hb > 0) {
									hbr = 15;
								} else {
									hbr = h > hbr ? h : hbr;
									if (hr > hbr)
										hbr = hr;
									if (hb > hbr)
										hbr = hb;
								}
								// bottom left corner
								if ( tb * hb + tbl * hbl + tl * hl > 0) {
									hbl = 15;
								} else {
									hbl = h > hbl ? h : hbl;
									if (hb > hbl)
										hbl = hb;
									if (hl > hbl)
										hbl = hl;
								}
								// forward left corner
								if (tl * hl + tfl * hfl + tf * hf > 0) {
									hfl = 15;
								} else {
									hfl = h > hfl ? h : hfl;
									if (hl > hfl)
										hfl = hl;
									if (hf > hfl)
										hfl = hf;
								}
								occlusionW = hfr + (hbr<<4) + (hbl<<8) + (hfl<<12);

								// flow
								int fx = hfr + hbr - hfl - hbl;
								if (fx > 0)
									fx = 2;
								else if (fx < 0)
									fx = 0;
								else
									fx = 1;
								int fz = hfl + hfr - hbl - hbr;
								if (fz > 0)
									fz = 2;
								else if (fz < 0)
									fz = 0;
								else
									fz = 1;

								occlusionY += (fx << 4) + (fz << 6);
							}
							pos.y -= 0.5f;
							tempChunkVertices.Add (pos);
							break;
						case RenderType.CutoutCross:
							{
								++chunkUVIndex;
								cutxssVoxelsCount++;
								cutxssIndices.Add (chunkUVIndex);
								uvAux.w = voxels [voxelIndex].light / 15f;
								float random = WorldRand.GetValue (pos);
								uvAux.w *= 0.8f + random * 0.3f;  // adds color variation
								offsetPos.y = pos.y - random * 0.1f;
								offsetPos.x = pos.x + random * 0.5f - 0.25f;
								Vector3 aux = pos;
								aux.x += 1f;
								random = WorldRand.GetValue (aux);
								offsetPos.z = pos.z + random * 0.5f - 0.25f;
								tempChunkVertices.Add (offsetPos);
							}
							break;
						case RenderType.OpaqueNoAO:
							++chunkUVIndex;
							opNoAOVoxelsCount++;
							tempChunkVertices.Add (pos);
							uvAux.w = 1f / 15f;
							opNoAOIndices.Add (chunkUVIndex);
							break;
						case RenderType.Custom:
							uvAux.w = voxels [voxelIndex].light / 15f;
							miv.voxelIndex = voxelIndex;
							miv.vd = type;
							mivs.Add (miv);
							continue;
						default: // Opaque & Cutout
							++chunkUVIndex;
							tempChunkVertices.Add (pos);

							byte leavesLight = 0;
							if (rt == RenderType.Cutout) {
								leavesLight = voxels [voxelIndex].light;
								cutoutVoxelsCount++;
								cutoutIndices.Add (chunkUVIndex);
							} else {
								opaqueVoxelsCount++;
								opaqueIndices.Add (chunkUVIndex);
							}

							if (denseTrees && rt == RenderType.Cutout) {
								uvAux.w = 0;
							} else {
								v1b = (v1b + 1) >> 4;	// substitutes a comparisson with FULL_OPAQUE (15)
								v1f = (v1f + 1) >> 4;
								v1u = (v1u + 1) >> 4;
								v1d = (v1d + 1) >> 4;
								v1l = (v1l + 1) >> 4;
								v1r = (v1r + 1) >> 4;
								uvAux.w = v1b + (v1f << 1) + (v1u << 2) + (v1d << 3) + (v1l << 4) + (v1r << 5);
		
								// Add collider data
								if (enableColliders && rt == RenderType.Opaque) {
									if (v1b == 0) {
										greedyCollider.AddQuad (FACE_DIRECTION.Back, x, y, z);
									}
									if (v1f == 0) {
										greedyCollider.AddQuad (FACE_DIRECTION.Forward, x, y, z);
									}
									if (v1u == 0) {
										greedyCollider.AddQuad (FACE_DIRECTION.Top, x, z, y);
									}
									if (v1d == 0) {
										greedyCollider.AddQuad (FACE_DIRECTION.Bottom, x, z, y);
									}
									if (v1l == 0) {
										greedyCollider.AddQuad (FACE_DIRECTION.Left, z, y, x);
									}
									if (v1r == 0) {
										greedyCollider.AddQuad (FACE_DIRECTION.Right, z, y, x);
									}
									if (enableNavMesh && type.navigatable) {
										if (v1b == 0) {
											greedyNavMesh.AddQuad (FACE_DIRECTION.Back, x, y, z);
										}
										if (v1f == 0) {
											greedyNavMesh.AddQuad (FACE_DIRECTION.Forward, x, y, z);
										}
										if (v1u == 0) {
											greedyNavMesh.AddQuad (FACE_DIRECTION.Top, x, z, y);
										}
										if (v1l == 0) {
											greedyNavMesh.AddQuad (FACE_DIRECTION.Left, z, y, x);
										}
										if (v1r == 0) {
											greedyNavMesh.AddQuad (FACE_DIRECTION.Right, z, y, x);
										}
									}
								}
							}
							if (rt == RenderType.Cutout) {
								// Add color variation
								float random = WorldRand.GetValue (pos);
								int r = (int)((0.8f + random * 0.4f) * 255);  // adds color variation
								uvAux.w += (r << 6);
								if (type.windAnimation) {
									uvAux.w += 65536; //1 << 16;
								}
							} 

											#if UNITY_EDITOR
							if (enableAmbientOcclusion && !draftModeActive) {
								#else
											if (enableAmbientOcclusion) {
								#endif
								int v2r = chunk_top_middle_right [top_middle_right].occludes;
								int v2br = chunk_top_back_right [top_back_right].occludes;
								int v2b = chunk_top_back_middle [top_back_middle].occludes;
								int v2bl = chunk_top_back_left [top_back_left].occludes;
								int v2l = chunk_top_middle_left [top_middle_left].occludes;
								int v2fl = chunk_top_forward_left [top_forward_left].occludes;
								int v2f = chunk_top_forward_middle [top_forward_middle].occludes;
								int v2fr = chunk_top_forward_right [top_forward_right].occludes;

								int v1fr = chunk_middle_forward_right [middle_forward_right].occludes;
								int v1br = chunk_middle_back_right [middle_back_right].occludes;
								int v1bl = chunk_middle_back_left [middle_back_left].occludes;
								int v1fl = chunk_middle_forward_left [middle_forward_left].occludes;

								int v0r = chunk_bottom_middle_right [bottom_middle_right].occludes;
								int v0br = chunk_bottom_back_right [bottom_back_right].occludes;
								int v0b = chunk_bottom_back_middle [bottom_back_middle].occludes;
								int v0bl = chunk_bottom_back_left [bottom_back_left].occludes;
								int v0l = chunk_bottom_middle_left [bottom_middle_left].occludes;
								int v0fl = chunk_bottom_forward_left [bottom_forward_left].occludes;
								int v0f = chunk_bottom_forward_middle [bottom_forward_middle].occludes;
								int v0fr = chunk_bottom_forward_right [bottom_forward_right].occludes;

								// Backwards face
								int backFaceGI = chunk_middle_back_middle [middle_back_middle].light | leavesLight;
								if (backFaceGI > 0) {
									// Vertex 0
									occ = v0b + v0bl + v1bl;
									occlusionX += (int)(backFaceGI / (1f + occ * occ * ambientOcclusionIntensity));
									// Vertex 1
									occ = v0b + v0br + v1br;
									occlusionX += (int)(backFaceGI / (1f + occ * occ * ambientOcclusionIntensity)) << 4;
									// Vertex 2
									occ = v1bl + v2bl + v2b;
									occlusionX += (int)(backFaceGI / (1f + occ * occ * ambientOcclusionIntensity)) << 8;
									// Vertex 3
									occ = v2b + v2br + v1br;
									occlusionX += (int)(backFaceGI / (1f + occ * occ * ambientOcclusionIntensity)) << 12;
								}
		
								// Forward face
								int frontFaceGI = chunk_middle_forward_middle [middle_forward_middle].light | leavesLight;
								if (frontFaceGI > 0) {
		
									// Vertex 5
									occ = v0f + v0fr + v1fr;
									occlusionX += (int)(frontFaceGI / (1f + occ * occ * ambientOcclusionIntensity)) << 16;
									// Vertex 4
									occ = v0f + v0fl + v1fl;
									occlusionX += (int)(frontFaceGI / (1f + occ * occ * ambientOcclusionIntensity)) << 20;
									// Vertex 6
									occ = v1fr + v2fr + v2f;
									occlusionY += (int)(frontFaceGI / (1f + occ * occ * ambientOcclusionIntensity));
									// Vertex 7
									occ = v2f + v2fl + v1fl;
									occlusionY += (int)(frontFaceGI / (1f + occ * occ * ambientOcclusionIntensity)) << 4;
								}
		
								// Top face
								int topFaceGI = chunk_top_middle_middle [top_middle_middle].light | leavesLight;
								if (topFaceGI > 0) {
									// Vertex 2
									occ = v2b + v2bl + v2l;
									occlusionY += (int)(topFaceGI / (1f + occ * occ * ambientOcclusionIntensity)) << 8;
									// Vertex 3
									occ = v2b + v2br + v2r;
									occlusionY += (int)(topFaceGI / (1f + occ * occ * ambientOcclusionIntensity)) << 12;
									// Vertex 7
									occ = v2l + v2fl + v2f;
									occlusionY += (int)(topFaceGI / (1f + occ * occ * ambientOcclusionIntensity)) << 16;
									// Vertex 6
									occ = v2r + v2fr + v2f;
									occlusionY += (int)(topFaceGI / (1f + occ * occ * ambientOcclusionIntensity)) << 20;
								}
		
								// Left face
								int leftFaceGI = chunk_middle_middle_left [middle_middle_left].light | leavesLight;
								if (leftFaceGI > 0) {
									// Vertex 0
									occ = v1bl + v0l + v0bl;
									occlusionZ += (int)(leftFaceGI / (1f + occ * occ * ambientOcclusionIntensity));
									// Vertex 4
									occ = v1fl + v0l + v0fl;
									occlusionZ += (int)(leftFaceGI / (1f + occ * occ * ambientOcclusionIntensity)) << 4;
									// Vertex 2
									occ = v2l + v2bl + v1bl;
									occlusionZ += (int)(leftFaceGI / (1f + occ * occ * ambientOcclusionIntensity)) << 8;
									// Vertex 7
									occ = v2l + v2fl + v1fl;
									occlusionZ += (int)(leftFaceGI / (1f + occ * occ * ambientOcclusionIntensity)) << 12;
								}
		
								// Right face
								int rightFaceGI = chunk_middle_middle_right [middle_middle_right].light | leavesLight;
								if (rightFaceGI > 0) {
									// Vertex 1
									occ = v1br + v0r + v0br;
									occlusionZ += (int)(rightFaceGI / (1f + occ * occ * ambientOcclusionIntensity)) << 16;
									// Vertex 5
									occ = v1fr + v0r + v0fr;
									occlusionZ += (int)(rightFaceGI / (1f + occ * occ * ambientOcclusionIntensity)) << 20;
									// Vertex 3
									occ = v2r + v2br + v1br;
									occlusionW += (int)(rightFaceGI / (1f + occ * occ * ambientOcclusionIntensity));
									// Vertex 6
									occ = v2r + v2fr + v1fr;
									occlusionW += (int)(rightFaceGI / (1f + occ * occ * ambientOcclusionIntensity)) << 4;
								}
		
								// Bottom face
								int bottomFaceGI = chunk_bottom_middle_middle [bottom_middle_middle].light | leavesLight;
								if (bottomFaceGI > 0) {
		
									// Vertex 0
									occ = v0b + v0l + v0bl;
									occlusionW += (int)(bottomFaceGI / (1f + occ * occ * ambientOcclusionIntensity)) << 8;
									// Vertex 1
									occ = v0b + v0r + v0br;
									occlusionW += (int)(bottomFaceGI / (1f + occ * occ * ambientOcclusionIntensity)) << 12;
									// Vertex 4
									occ = v0f + v0l + v0fl;
									occlusionW += (int)(bottomFaceGI / (1f + occ * occ * ambientOcclusionIntensity)) << 16;
									// Vertex 5
									occ = v0f + v0r + v0fr;
									occlusionW += (int)(bottomFaceGI / (1f + occ * occ * ambientOcclusionIntensity)) << 20;
								}
							} else {
								occlusionX = chunk_middle_back_middle [middle_back_middle].light;  // back
								occlusionX += chunk_middle_forward_middle [middle_forward_middle].light << 4;  // forward
								occlusionX += chunk_top_middle_middle [top_middle_middle].light << 8;  // top
								occlusionX += chunk_middle_middle_left [middle_middle_left].light << 12;  // left
								occlusionX += chunk_middle_middle_right [middle_middle_right].light << 16;  // right
								occlusionX += chunk_bottom_middle_middle [bottom_middle_middle].light << 20;  // bottom
								if (leavesLight > 0) {
									occlusionX |= leavesLight + (leavesLight << 4) + (leavesLight << 8) + (leavesLight << 12) + (leavesLight << 16) + (leavesLight << 20);
								} 
							}
							break;
						}
						tempChunkUV0.Add (uvAux);
						uv2Aux.x = occlusionX;
						uv2Aux.y = occlusionY;
						uv2Aux.z = occlusionZ;
						uv2Aux.w = occlusionW;
						tempChunkUV2.Add (uv2Aux);
		
						if (enableTinting) {
							tintColor.r = voxels [voxelIndex].red;
							tintColor.g = voxels [voxelIndex].green;
							tintColor.b = voxels [voxelIndex].blue;
							tempChunkColors32.Add (tintColor);
						}
					}
				}
			}

			meshJobs [jobIndex].chunk = chunk;
			meshJobs [jobIndex].totalVisibleVoxels = chunkVoxelCount;
			if (chunkVoxelCount == 0) {
				return;
			}

			voxelSignature++;
			chunk.needsColliderRebuild = voxelSignature != chunk.voxelSignature;
			chunk.voxelSignature = voxelSignature;

			if (enableColliders) {
				if (chunk.needsColliderRebuild) {
					greedyCollider.FlushTriangles (meshColliderVertices, meshColliderIndices);
					if (enableNavMesh) {
						greedyNavMesh.FlushTriangles (navMeshVertices, navMeshIndices);
					}
				} else {
					greedyCollider.Clear ();
					greedyNavMesh.Clear ();
				}
			}

			meshJobs [jobIndex].vertices = tempChunkVertices;
			meshJobs [jobIndex].uv0 = tempChunkUV0;
			meshJobs [jobIndex].uv2 = tempChunkUV2;
			meshJobs [jobIndex].colors = tempChunkColors32;

			int matVoxelIndex = 0;
			int subMeshCount = 0;
			meshJobs [jobIndex].opaqueVoxelsCount = opaqueVoxelsCount;
			if (opaqueVoxelsCount > 0) {
				meshJobs [jobIndex].opaqueIndicesArray = opaqueIndices.ToArray ();
				subMeshCount++;
				matVoxelIndex++;
			}
			meshJobs [jobIndex].cutoutVoxelsCount = cutoutVoxelsCount;
			if (cutoutVoxelsCount > 0) {
				meshJobs [jobIndex].cutoutIndicesArray = cutoutIndices.ToArray ();
				subMeshCount++;
				matVoxelIndex += 2;
			}
			meshJobs [jobIndex].transpVoxelsCount = transpVoxelsCount;
			if (transpVoxelsCount > 0) {
				meshJobs [jobIndex].transpIndicesArray = transpIndices.ToArray ();
				subMeshCount++;
				matVoxelIndex += 4;
			}
			meshJobs [jobIndex].cutxssVoxelsCount = cutxssVoxelsCount;
			if (cutxssVoxelsCount > 0) {
				meshJobs [jobIndex].cutxssIndicesArray = cutxssIndices.ToArray ();
				subMeshCount++;
				matVoxelIndex += 8;
			}
			meshJobs [jobIndex].opNoAOVoxelsCount = opNoAOVoxelsCount;
			if (opNoAOVoxelsCount > 0) {
				meshJobs [jobIndex].opNoAOIndicesArray = opNoAOIndices.ToArray ();
				subMeshCount++;
				matVoxelIndex += 16;
			}
			meshJobs [jobIndex].matVoxelIndex = matVoxelIndex;
			meshJobs [jobIndex].subMeshCount = subMeshCount;

			meshJobs [jobIndex].colliderVertices = meshColliderVertices;
			meshJobs [jobIndex].colliderIndices = meshColliderIndices;

			meshJobs [jobIndex].navMeshVertices = navMeshVertices;
			meshJobs [jobIndex].navMeshIndices = navMeshIndices;

			meshJobs [jobIndex].mivs = mivs;

		}


	}



}
