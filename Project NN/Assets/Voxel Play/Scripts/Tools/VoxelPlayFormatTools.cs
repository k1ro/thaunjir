﻿using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelPlay {

				/// <summary>
				/// A neutral / engine independent model definition. Basically an array of colors which specify voxels inside a model with specified size and optional center offset
				/// </summary>
				public struct ColorBasedModelDefinition {
								public string name;
								public int sizeX, sizeY, sizeZ;
								public int offsetX, offsetY, offsetZ;

								/// <summary>
								/// Colors are arranged in Y/Z/X structure
								/// </summary>
								public Color32[] colors;

								public static ColorBasedModelDefinition Null = new ColorBasedModelDefinition ();
				}


				public static class VoxelPlayFormatTools {

								public static ModelDefinition GetModelDefinition (VoxelDefinition voxelTemplate, ColorBasedModelDefinition model, bool ignoreOffset) {
												ModelDefinition md = ScriptableObject.CreateInstance<ModelDefinition> ();
												md.sizeX = model.sizeX;
												md.sizeY = model.sizeY;
												md.sizeZ = model.sizeZ;
												if (!ignoreOffset) {
																md.offsetX = model.offsetX;
																md.offsetY = model.offsetY;
																md.offsetZ = model.offsetZ;
												}
												List<ModelBit> bits = new List<ModelBit> ();
												for (int y = 0; y < model.sizeY; y++) {
																int posy = y * model.sizeX * model.sizeZ;
																for (int z = 0; z < model.sizeZ; z++) {
																				int posz = z * model.sizeX;
																				for (int x = 0; x < model.sizeX; x++) {
																								int index = posy + posz + x;
																								if (model.colors [index].a > 0) {
																												ModelBit bit = new ModelBit ();
																												bit.voxelIndex = index;
																												bit.voxelDefinition = voxelTemplate;
																												bit.color = model.colors [index];
																												bits.Add (bit);
																								}
																				}
																}
												}
												md.bits = bits.ToArray ();
												return md;
								}
				}

}
