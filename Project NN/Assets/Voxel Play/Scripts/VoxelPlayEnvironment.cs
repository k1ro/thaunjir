﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelPlay {

	public struct VoxelIndex {
		public Vector3 position;
		public VoxelChunk chunk;
		public int voxelIndex;
	}

	public partial class VoxelPlayEnvironment : MonoBehaviour {

		public WorldDefinition world;
		public bool enableBuildMode = true;
		public bool constructorMode;
		public bool buildMode;
		public Camera cameraMain;
		public bool editorDraftMode;
		public bool renderInEditor;
		public bool renderInEditorLowPriority = true;
		public CharacterController characterController;
		public bool enableConsole = true;
		public bool showConsole;
		public bool enableInventory = true;
		public bool enableDebugWindow = true;
		public bool showFPS;
		public string welcomeMessage = "<color=green>Welcome to <color=white>Voxel Play</color>! Press<color=yellow> F1 </color>for console commands.</color>";
		public float welcomeMessageDuration = 5f;
		public GameObject UICanvasPrefab;
		public GameObject crosshairPrefab;
		public Texture2D crosshairTexture;
		public Color consoleBackgroundColor = new Color (0, 0, 0, 82f / 255f);
		public bool enableStatusBar = true;
		public Color statusBarBackgroundColor = new Color (0, 0, 0, 192 / 255f);
		public int layerParticles = 2;
		public int layerVoxels = 1;
		public bool enableLoadingPanel = true;

		public bool loadSavedGame;
		public string saveFilename = "save0001";

		public bool globalIllumination = true;
		[Range (0f, 1f)]
		public float ambientLight = 0.2f;

		public bool enableAmbientOcclusion = true;
		[Range (0, 0.5f)]
		public float ambientOcclusionIntensity = 0.2f;

		public bool enableFogSkyBlending = true;

		public int textureSize = 64;

		public int maxChunks = 16000;

		public bool hqFiltering = true;
		[Range (0, 2f)]
		public float mipMapBias = 1;

		/// <summary>
		/// Minimum recommended chunk pool size based on visible distance
		/// </summary>
		public int maxChunksRecommended {
			get {
				int dxz = _visibleChunksDistance * 2 + 1;
				int dy = Mathf.Min (_visibleChunksDistance, 8) * 2 + 1;
				return Mathf.Max (3000, dxz * dxz * dy * 2);
			}
		}

		public int prewarmChunksInEditor = 5000;

		public bool enableTinting = false;

		[Range (1, 25)]
		[SerializeField]
		int _visibleChunksDistance = 10;

		public int visibleChunksDistance {
			get { return _visibleChunksDistance; }
			set {
				if (_visibleChunksDistance != value) {
					_visibleChunksDistance = value;
					lastCamPos.y += 0.0001f; // forces check chunks in frustum
					InitOctrees ();
				}
			}
		}

		public bool useGeometryShaders = true;
		public bool enableShadows = true;
		public bool shadowsOnWater = false;
		[Range (1, 8)] public int forceChunkDistance = 3;
		public long maxCPUTimePerFrame = 30;
		public int maxChunksPerFrame = 50;
		public int maxTreesPerFrame = 10;
		public int maxBushesPerFrame = 10;
		public bool multiThreadGeneration = true;
		public bool onlyGenerateInFrustum = true;
		public bool onlyRenderInFrustum = true;

		public bool enableColliders = true;
		public bool enableNavMesh = false;

		public bool enableTrees = true;
		public bool denseTrees = true;
		public bool enableVegetation = true;

		public Light sun;
		[Range (0, 1)]
		public float fogAmount = 0.5f;
		public float fogDistance = 300;
		public float fogFalloff = 0.004f;
		public bool enableClouds = true;

		/// <summary>
		/// Default build sound.
		/// </summary>
		public AudioClip defaultBuildSound;

		/// <summary>
		/// Default pick up sound.
		/// </summary>
		public AudioClip defaultPickupSound;

		/// <summary>
		/// Default pick up sound.
		/// </summary>
		public AudioClip defaultDestructionSound;

		/// <summary>
		/// Default impact/hit sound.
		/// </summary>
		public AudioClip defaultImpactSound;

		[Tooltip ("Assumed voxel when the voxel definition is missing or placing colors directly on the positions")]
		public VoxelDefinition defaultVoxel;


		#region Public useful state fields

		[NonSerialized, HideInInspector]
		public VoxelHitInfo lastHitInfo;

		public bool cameraHasMoved {
			get { return _cameraHasMoved; }
		}

		[NonSerialized]
		public int chunksCreated, chunksUsed, chunksInRenderQueueCount, chunksDrawn;
		[NonSerialized]
		public int voxelsCreatedCount;
		[NonSerialized]
		public int treesInCreationQueueCount, treesCreated;
		[NonSerialized]
		public int vegetationInCreationQueueCount, vegetationCreated;

		#endregion


		#region Public Events

		/// <summary>
		/// Triggered after a voxel receives damage
		/// </summary>
		public event VoxelHitEvent OnVoxelDamaged;

		/// <summary>
		/// Triggered just before a voxel is destroyed
		/// </summary>
		public event VoxelEvent OnVoxelBeforeDestroyed;

		/// <summary>
		/// Triggered after a voxel is destroyed
		/// </summary>
		public event VoxelEvent OnVoxelDestroyed;

		/// <summary>
		/// Triggered just before a recoverable voxel is created.
		/// </summary>
		public event VoxelDropItemEvent OnVoxelBeforeDropItem;

		/// <summary>
		/// Triggered when clicking on a voxel.
		/// </summary>
		public event VoxelClickEvent OnVoxelClick;

		/// <summary>
		/// Triggered after the contents of a chunk changes (ie. placing a new voxel)
		/// </summary>
		public event VoxelChunkEvent OnChunkChanged;

		/// <summary>
		/// Triggered after a torch is placed
		/// </summary>
		public event VoxelTorchEvent OnTorchAttached;

		/// <summary>
		/// Triggered after a torch is removed
		/// </summary>
		public event VoxelTorchEvent OnTorchDetached;

		/// <summary>
		/// Triggered after a saved game is loaded
		/// </summary>
		public event VoxelPlayEvent OnGameLoaded;

		/// <summary>
		/// Triggered after Voxel Play has finished loading and initializing stuff
		/// </summary>
		public event VoxelPlayEvent OnInitialized;


		/// <summary>
		/// Triggered just before the chunk is filled with default contents (terrain, etc.)
		/// Set overrideDefaultContents to fill in the voxels array with your own content (voxel array is a linear array of 18x18x18 voxels)
		/// </summary>
		public event VoxelChunkBeforeCreationEvent OnChunkBeforeCreate;

		/// <summary>
		/// Triggered just after the chunk has been filled with default contents (terrain, etc.)
		/// </summary>
		public event VoxelChunkEvent OnChunkAfterCreate;

		/// <summary>
		/// Triggered just after the chunk has been rendered for the first time
		/// </summary>
		public event VoxelChunkEvent OnChunkAfterFirstRender;


		#endregion



		#region Public API

		static VoxelPlayEnvironment _instance;

		/// <summary>
		/// An empty voxel
		/// </summary>
		public Voxel Empty;


		/// <summary>
		/// Returns the singleton instance of Voxel Play API.
		/// </summary>
		/// <value>The instance.</value>
		public static VoxelPlayEnvironment instance {
			get {
				if (_instance == null) {
					_instance = FindObjectOfType<VoxelPlayEnvironment> ();
					if (_instance == null) {
						VoxelPlayEnvironment[] vv = Resources.FindObjectsOfTypeAll<VoxelPlayEnvironment> ();
						for (int k = 0; k < vv.Length; k++) {
							if (vv [k].hideFlags != HideFlags.HideInHierarchy) {
								_instance = vv [k];
								break;
							}
						}
					}
				}
				return _instance;
			}
		}

		/// <summary>
		/// The default value for the light amount on a clear voxel. If global illumination is enabled, this value is 0 (dark). If it's disabled, then this value is 15 (so it does not darken the voxel).
		/// </summary>
		/// <value>The no light value.</value>
		public byte noLightValue {
			get {
				return effectiveGlobalIllumination ? (byte)0 : (byte)15;
			}
		}

		/// <summary>
		/// Gets the GameObject of the player
		/// </summary>
		/// <value>The player game object.</value>
		public GameObject playerGameObject {
			get {
				if (characterController != null) {
					return characterController.gameObject;
				} else {
					return cameraMain.gameObject;
				}
			}
		}

		/// <summary>
		/// Destroyes everything and reloads current assigned world
		/// </summary>
		public void ReloadWorld () {
			if (!applicationIsPlaying && !renderInEditor)
				return;

			LoadWorldInt ();
			initialized = true;
			DoWork ();
			// Refresh behaviours lighting
			VoxelPlayBehaviour[] bh = FindObjectsOfType<VoxelPlayBehaviour> ();
			for (int k = 0; k < bh.Length; k++) {
				bh [k].UpdateLighting ();
			}
		}


		/// <summary>
		/// Clears all chunks in the world and initializes all structures
		/// </summary>
		public void DestroyAllVoxels () {
			LoadWorldInt ();
			WarmChunks (null);
			initialized = true;
		}


		/// <summary>
		/// Issues a redraw command on all chunks
		/// </summary>
		public void Redraw (bool reloadWorldTextures = false) {
			if (reloadWorldTextures) {
				LoadWorldTextures ();
			}
			UpdateMaterialProperties ();
			if (cachedChunks != null) {
				foreach (KeyValuePair<int, CachedChunk> kv in cachedChunks) {
					CachedChunk cc = kv.Value;
					if (cc != null && cc.chunk != null) {
						ChunkRequestRefresh (cc.chunk, true, true);
					}
				}
			}
		}

		/// <summary>
		/// Toggles on/off chunks visibility
		/// </summary>
		public bool ChunksToggle () {
			if (chunksRoot != null) {
				chunksRoot.gameObject.SetActive (!chunksRoot.gameObject.activeSelf);
				return chunksRoot.gameObject.activeSelf;
			}
			return false;
		}

		/// <summary>
		/// Toggles chunks visibility
		/// </summary>
		public void ChunksToggle (bool visible) {
			if (chunksRoot != null) {
				chunksRoot.gameObject.SetActive (visible);
			}
		}

		/// <summary>
		/// Casts a ray and applies given damage to any voxel impacted in the direction.
		/// </summary>
		/// <returns><c>true</c>, if voxel was hit, <c>false</c> otherwise.</returns>
		/// <param name="ray">Ray.</param>
		/// <param name="damage">Damage.</param>
		/// <param name="maxDistance">Max distance of ray.</param>
		public bool RayHit (Ray ray, int damage, float maxDistance = 0, int damageRadius = 1) {
			return RayHit (ray.origin, ray.direction, damage, maxDistance, damageRadius);
		}

		/// <summary>
		/// Casts a ray and applies given damage to any voxel impacted in the direction.
		/// </summary>
		/// <returns><c>true</c>, if voxel was hit, <c>false</c> otherwise.</returns>
		/// <param name="ray">Ray.</param>
		/// <param name="damage">Damage.</param>
		/// <param name="hitInfo">VoxelHitInfo structure with additional details.</param>
		/// <param name="maxDistance">Max distance of ray.</param>
		public bool RayHit (Ray ray, int damage, out VoxelHitInfo hitInfo, float maxDistance = 0, int damageRadius = 1) {
			return RayHit (ray.origin, ray.direction, damage, out hitInfo, maxDistance, damageRadius);
		}

		/// <summary>
		/// Casts a ray and applies given damage to any voxel impacted in the direction.
		/// </summary>
		/// <returns><c>true</c>, if voxel was hit, <c>false</c> otherwise.</returns>
		/// <param name="ray">Ray.</param>
		/// <param name="damage">Damage.</param>
		/// <param name="maxDistance">Max distance of ray.</param>
		public bool RayHit (Vector3 origin, Vector3 direction, int damage, float maxDistance = 0, int damageRadius = 1) {
			VoxelHitInfo hitInfo;
			bool impact = HitVoxelFast (origin, direction, damage, out hitInfo, maxDistance, damageRadius);
			return impact;

		}

		/// <summary>
		/// Casts a ray and applies given damage to any voxel impacted in the direction.
		/// </summary>
		/// <returns><c>true</c>, if voxel was hit, <c>false</c> otherwise.</returns>
		/// <param name="ray">Ray.</param>
		/// <param name="damage">Damage.</param>
		/// <param name="hitInfo">VoxelHitInfo structure with additional details.</param>
		/// <param name="maxDistance">Max distance of ray.</param>
		public bool RayHit (Vector3 origin, Vector3 direction, int damage, out VoxelHitInfo hitInfo, float maxDistance = 0, int damageRadius = 1) {
			return HitVoxelFast (origin, direction, damage, out hitInfo, maxDistance, damageRadius);
		}


		/// <summary>
		/// Raycasts in the direction of the ray from ray's origin.
		/// </summary>
		/// <returns><c>true</c>, if a voxel was hit, <c>false</c> otherwise.</returns>
		/// <param name="ray">Ray.</param>
		/// <param name="hitInfo">Hit info.</param>
		/// <param name="maxDistance">Max distance.</param>
		/// <param name="minOpaque">Optionally limit the rayhit to voxels with certain opaque factor (15 = solid/full opaque, 2 = grass).</param>
		public bool RayCast (Ray ray, out VoxelHitInfo hitInfo, float maxDistance = 0, byte minOpaque = 0) {
			return RayCastFast (ray.origin, ray.direction, out hitInfo, maxDistance, false, minOpaque);
		}

		/// <summary>
		/// Raycasts from a given origin and direction.
		/// </summary>
		/// <returns><c>true</c>, if a voxel was hit, <c>false</c> otherwise.</returns>
		/// <param name="ray">Ray.</param>
		/// <param name="hitInfo">Hit info.</param>
		/// <param name="maxDistance">Max distance.</param>
		/// <param name="minOpaque">Optionally limit the rayhit to voxels with certain opaque factor (15 = solid/full opaque, 2 = grass).</param>
		public bool RayCast (Vector3 origin, Vector3 direction, out VoxelHitInfo hitInfo, float maxDistance = 0, byte minOpaque = 0) {
			return RayCastFast (origin, direction, out hitInfo, maxDistance, false, minOpaque);
		}

		/// <summary>
		/// Gets the highest voxel position under a given location
		/// </summary>
		public float GetHeight (Vector3 position) {
			VoxelHitInfo hitInfo;
			RayCastFast (new Vector3 (position.x, 1000, position.z), Misc.vector3down, out hitInfo, 0, true);
			return hitInfo.point.y;
		}

		/// <summary>
		/// Returns the voxel chunk where the player is located
		/// </summary>
		public VoxelChunk GetCurrentChunk () {
			if (cameraMain == null)
				return null;
			return GetChunkOrCreate (cameraMain.transform.position);
		}

		/// <summary>
		/// Returns true if water is found at a given position (only X/Z values are considered)
		/// </summary>
		/// <returns><c>true</c> if this instance is water at position; otherwise, <c>false</c>.</returns>
		public bool IsWaterAtPosition (Vector3 position) {
			if (heightMapCache == null)
				return false;
			float groundLevel = GetHeightMapInfoFast (position.x, position.z).groundLevel;
			if (waterLevel > groundLevel) {
				return true;
			} else {
				return false;
			}
		}

		/// <summary>
		/// Returns the chunk at a given position
		/// </summary>
		/// <returns><c>true</c>, if chunk was gotten, <c>false</c> otherwise.</returns>
		/// <param name="position">Position.</param>
		/// <param name="chunk">Chunk.</param>
		/// <param name="forceCreation">If set to <c>true</c> force creation.</param>
		public bool GetChunk (Vector3 position, out VoxelChunk chunk, bool forceCreation = false) {
			int chunkX = FastMath.FloorToInt (position.x / 16);
			int chunkY = FastMath.FloorToInt (position.y / 16);
			int chunkZ = FastMath.FloorToInt (position.z / 16);
			return GetChunkFast (chunkX, chunkY, chunkZ, out chunk, forceCreation);
		}


		/// <summary>
		/// Returns a list of created chunks. 
		/// </summary>
		/// <param name="chunks">User provided list for returning the chunks.</param>
		public void GetChunks (List<VoxelChunk> chunks) {
			chunks.Clear ();
			for (int k = 0; k < chunksPoolLoadIndex; k++) {
				if (chunksPool [k].isPopulated) {
					chunks.Add (chunksPool [k]);
				}
			}
		}


		/// <summary>
		/// Returns the chunk at a given position without invoking the terrain generator (the chunk should be empty but only if it's got before it has rendered)
		/// You can use chunk.isPopulated to query if terrain has rendered into this chunk or not
		/// </summary>
		/// <returns><c>true</c>, if chunk was gotten, <c>false</c> otherwise.</returns>
		/// <param name="position">Position.</param>
		/// <param name="chunk">Chunk.</param>
		public VoxelChunk GetChunkUnpopulated (Vector3 position) {
			int chunkX = FastMath.FloorToInt (position.x / 16);
			int chunkY = FastMath.FloorToInt (position.y / 16);
			int chunkZ = FastMath.FloorToInt (position.z / 16);
			VoxelChunk chunk;
			STAGE = 201;
			GetChunkFast (chunkX, chunkY, chunkZ, out chunk, false);
			if ((object)chunk == null) {
				STAGE = 202;
				int hash = GetChunkHash (chunkX, chunkY, chunkZ);
				chunk = CreateChunk (hash, chunkX, chunkY, chunkZ, true, false);
			}
			return chunk;
		}




		/// <summary>
		/// Returns the chunk position that encloses a given position
		/// </summary>
		/// <param name="position">Position.</param>
		public Vector3 GetChunkPosition (Vector3 position) {
			int x = FastMath.FloorToInt (position.x / 16) * 16 + 8;
			int y = FastMath.FloorToInt (position.y / 16) * 16 + 8;
			int z = FastMath.FloorToInt (position.z / 16) * 16 + 8;
			return new Vector3 (x, y, z);
		}


		/// <summary>
		/// Gets the voxel at a given position. Returns Voxel.Empty if no voxel found.
		/// </summary>
		/// <returns>The voxel.</returns>
		/// <param name="position">Position.</param>
		/// <param name="createChunkIfNotExists">If set to <c>true</c> create chunk if not exists.</param>
		/// <param name="onlyRenderedVoxels">If set to <c>true</c> the voxel will only be returned if it's rendered. If you're calling GetVoxel as part of a spawning logic, pass true as it will ensure the voxel returned also has the collider in place so your spawned stuff won't fall down.</param>
		public Voxel GetVoxel (Vector3 position, bool createChunkIfNotExists = true, bool onlyRenderedVoxels = false) {
			int chunkX = FastMath.FloorToInt (position.x / 16);
			int chunkY = FastMath.FloorToInt (position.y / 16);
			int chunkZ = FastMath.FloorToInt (position.z / 16);
			VoxelChunk chunk;
			GetChunkFast (chunkX, chunkY, chunkZ, out chunk, createChunkIfNotExists);
			if (chunk != null && (!onlyRenderedVoxels || onlyRenderedVoxels && chunk.renderState == CHUNK_RENDER_STATE.RenderingComplete)) {
				Voxel[] voxels = chunk.voxels;
				int px = FastMath.FloorToInt (position.x) - chunkX * 16;
				int py = FastMath.FloorToInt (position.y) - chunkY * 16;
				int pz = FastMath.FloorToInt (position.z) - chunkZ * 16;
				int voxelIndex = py * ONE_Y_ROW + pz * ONE_Z_ROW + px;
				return voxels [voxelIndex];
			}
			return Voxel.Empty;
		}


		/// <summary>
		/// Returns in indices list all visible voxels inside a volume defined by boxMin and boxMax
		/// </summary>
		/// <returns>Count of all visible voxel indices.</returns>
		/// <param name="boxMin">Bottom/left/back or minimum corner of the enclosing box.</param>
		/// <param name="boxMax">Top/right/forward or maximum corner of the enclosing box.</param>
		/// <param name="indices">A list of indices provided by the user to write to.</param>
		public int GetVoxelIndices (Vector3 boxMin, Vector3 boxMax, List<VoxelIndex> indices) {
			int minx = FastMath.FloorToInt (boxMin.x);
			int miny = FastMath.FloorToInt (boxMin.y);
			int minz = FastMath.FloorToInt (boxMin.z);
			int maxx = FastMath.FloorToInt (boxMax.x) + 1;
			int maxy = FastMath.FloorToInt (boxMax.y) + 1;
			int maxz = FastMath.FloorToInt (boxMax.z) + 1;
			Vector3 position, voxelPosition;
			VoxelIndex index = new VoxelIndex ();
			indices.Clear ();
			tempChunks.Clear ();
			for (int y = miny; y < maxy; y += 16) {
				position.y = y;
				for (int z = minz; z < maxz; z += 16) {
					position.z = z;
					for (int x = minx; x < maxx; x += 16) {
						position.x = x;
						VoxelChunk chunk;
						if (GetChunk (position, out chunk, false) && !tempChunks.Contains (chunk)) {
							tempChunks.Add (chunk);
							for (int v = 0; v < chunk.voxels.Length; v++) {
								if (chunk.voxels [v].hasContent == 1) {
									int py = v / ONE_Y_ROW;
									voxelPosition.y = chunk.position.y - 7.5f + py;
									if (voxelPosition.y >= miny && voxelPosition.y < maxy) {
										int remy = v - py * ONE_Y_ROW;
										int pz = remy / ONE_Z_ROW;
										voxelPosition.z = chunk.position.z - 7.5f + pz;
										if (voxelPosition.z >= minz && voxelPosition.z < maxz) {
											int px = remy - pz * ONE_Z_ROW;
											voxelPosition.x = chunk.position.x - 7.5f + px;
											if (voxelPosition.x >= minx && voxelPosition.x < maxx) {
												index.chunk = chunk;
												index.voxelIndex = v;
												index.position = voxelPosition;
												indices.Add (index);
											}
										}
									}
								}
							}
						}
					}
				}
			}
			return indices.Count;
		}



		/// <summary>
		/// Adds a new voxel definition to the internal dictionary
		/// </summary>
		/// <param name="vd">Vd.</param>
		public void AddVoxelDefinition (VoxelDefinition vd) {
			// Check if voxelType is not added
			if (vd.index <= 0) {
				sessionUserVoxels.Add (vd);
				requireTextureArrayUpdate = true;
			}
		}

		/// <summary>
		/// Gets the voxel definition by name
		/// </summary>
		/// <returns>The voxel definition.</returns>
		public VoxelDefinition GetVoxelDefinition (string name) {
			VoxelDefinition vd;
			voxelDefinitionsDict.TryGetValue (name, out vd);
			return vd;
		}


		/// <summary>
		/// Gets the voxel definition by index
		/// </summary>
		/// <returns>The voxel definition.</returns>
		/// <param name="index">Index.</param>
		public VoxelDefinition GetVoxelDefinition (int index) {
			if (index >= 0 && index < voxelDefinitionsCount) {
				return voxelDefinitions [index];
			}
			return null;
		}


		/// <summary>
		/// Gets the chunk position and voxelIndex corresponding to a given world position (note that the chunk might not exists yet)
		/// </summary>
		/// <param name="position">World position.</param>
		/// <param name="chunk">Chunk position.</param>
		/// <param name="index">Voxel index in the chunk.voxels array</param>
		public void GetVoxelIndex (Vector3 position, out Vector3 chunkPosition, out int voxelIndex) {
			int chunkX, chunkY, chunkZ;
			chunkX = FastMath.FloorToInt (position.x / 16f) * 16;
			chunkY = FastMath.FloorToInt (position.y / 16f) * 16;
			chunkZ = FastMath.FloorToInt (position.z / 16f) * 16;
			chunkPosition.x = chunkX + 8;
			chunkPosition.y = chunkY + 8;
			chunkPosition.z = chunkZ + 8;
			int px = FastMath.FloorToInt (position.x) - chunkX; 
			int py = FastMath.FloorToInt (position.y) - chunkY;
			int pz = FastMath.FloorToInt (position.z) - chunkZ;
			voxelIndex = py * ONE_Y_ROW + pz * ONE_Z_ROW + px;
		}


		/// <summary>
		/// Gets the chunk and array index of the voxel at a given position
		/// </summary>
		/// <param name="position">Position.</param>
		/// <param name="chunk">Chunk.</param>
		/// <param name="index">Voxel index in the chunk.voxels array</param>
		public bool GetVoxelIndex (Vector3 position, out VoxelChunk chunk, out int voxelIndex, bool createChunkIfNotExists = true) {
			int chunkX, chunkY, chunkZ;
			chunkX = FastMath.FloorToInt (position.x / 16f);
			chunkY = FastMath.FloorToInt (position.y / 16f);
			chunkZ = FastMath.FloorToInt (position.z / 16f);
			if (GetChunkFast (chunkX, chunkY, chunkZ, out chunk, createChunkIfNotExists)) {
				int py = FastMath.FloorToInt (position.y) - chunkY * 16;
				int pz = FastMath.FloorToInt (position.z) - chunkZ * 16;
				int px = FastMath.FloorToInt (position.x) - chunkX * 16;
				voxelIndex = py * ONE_Y_ROW + pz * ONE_Z_ROW + px;
				return true;
			} else {
				voxelIndex = 0;
				return false;
			}
		}

		/// <summary>
		/// Gets the index of a voxel by its local x,y,z positions inside a voxel
		/// </summary>
		/// <returns>The voxel index.</returns>
		public int GetVoxelIndex (int px, int py, int pz) {
			return py * ONE_Y_ROW + pz * ONE_Z_ROW + px;
		}


		/// <summary>
		/// Gets the chunk and voxel index corresponding to certain offset to another chunk/voxel index. Useful to get a safe reference to voxels on top of others, etc.
		/// </summary>
		/// <returns><c>true</c>, if voxel index was gotten, <c>false</c> otherwise.</returns>
		/// <param name="chunk">Chunk.</param>
		/// <param name="voxelIndex">Voxel index.</param>
		/// <param name="offsetX">Offset x.</param>
		/// <param name="offsetY">Offset y.</param>
		/// <param name="offsetZ">Offset z.</param>
		/// <param name="otherChunk">Other chunk.</param>
		/// <param name="otherVoxelIndex">Other voxel index.</param>
		/// <param name="createChunkIfNotExists">If set to <c>true</c> create chunk if not exists.</param>
		public bool GetVoxelIndex (VoxelChunk chunk, int voxelIndex, int offsetX, int offsetY, int offsetZ, out VoxelChunk otherChunk, out int otherVoxelIndex, bool createChunkIfNotExists = false) {
			int chunkX, chunkY, chunkZ;

			int px, py, pz;
			GetVoxelChunkCoordinates (voxelIndex, out px, out py, out pz);
			Vector3 position;
			position.x = chunk.position.x - 7.5f + px + offsetX;
			position.y = chunk.position.y - 7.5f + py + offsetY;
			position.z = chunk.position.z - 7.5f + pz + offsetZ;
			chunkX = FastMath.FloorToInt (position.x / 16f);
			chunkY = FastMath.FloorToInt (position.y / 16f);
			chunkZ = FastMath.FloorToInt (position.z / 16f);
			if (GetChunkFast (chunkX, chunkY, chunkZ, out otherChunk, createChunkIfNotExists)) {
				py = FastMath.FloorToInt (position.y) - chunkY * 16;
				pz = FastMath.FloorToInt (position.z) - chunkZ * 16;
				px = FastMath.FloorToInt (position.x) - chunkX * 16;
				otherVoxelIndex = py * ONE_Y_ROW + pz * ONE_Z_ROW + px;
				return true;
			} else {
				otherVoxelIndex = 0;
				return false;
			}
		}

		/// <summary>
		/// Gets the voxel position in world space coordinates.
		/// </summary>
		/// <returns>The voxel position.</returns>
		/// <param name="chunk">Chunk.</param>
		/// <param name="voxelIndex">Voxel index.</param>
		public Vector3 GetVoxelPosition (VoxelChunk chunk, int voxelIndex) {
			int px, py, pz;
			GetVoxelChunkCoordinates (voxelIndex, out px, out py, out pz);
			Vector3 position;
			position.x = chunk.position.x - 7.5f + px;
			position.y = chunk.position.y - 7.5f + py;
			position.z = chunk.position.z - 7.5f + pz;
			return position;
		}


		/// <summary>
		/// Gets the voxel position in world space coordinates.
		/// </summary>
		/// <returns>The voxel position.</returns>
		/// <param name="chunkPosition">Chunk position.</param>
		/// <param name="voxelIndex">Voxel index.</param>
		public Vector3 GetVoxelPosition (Vector3 chunkPosition, int voxelIndex) {
			int px, py, pz;
			GetVoxelChunkCoordinates (voxelIndex, out px, out py, out pz);
			Vector3 position;
			position.x = chunkPosition.x - 7.5f + px;
			position.y = chunkPosition.y - 7.5f + py;
			position.z = chunkPosition.z - 7.5f + pz;
			return position;
		}

		/// <summary>
		/// Given a voxel index, returns its x, y, z position inside the chunk
		/// </summary>
		/// <param name="voxelIndex">Voxel index.</param>
		/// <param name="px">Px.</param>
		/// <param name="py">Py.</param>
		/// <param name="pz">Pz.</param>
		public void GetVoxelChunkCoordinates (int voxelIndex, out int px, out int py, out int pz) {
			py = voxelIndex / ONE_Y_ROW;
			int remy = voxelIndex - py * ONE_Y_ROW;
			pz = remy / ONE_Z_ROW;
			px = remy - pz * ONE_Z_ROW;
		}

		/// <summary>
		/// Returns true if the specified voxel has content and is visible from any of the 6 surrounding faces
		/// </summary>
		/// <returns><c>true</c>, if voxel is visible, <c>false</c> otherwise.</returns>
		/// <param name="chunk">Chunk.</param>
		/// <param name="voxelIndex">Voxel index.</param>
		public bool GetVoxelVisibility (VoxelChunk chunk, int voxelIndex) {
			if (chunk == null || chunk.voxels [voxelIndex].hasContent != 1)
				return false;
			Vector3 position = GetVoxelPosition (chunk, voxelIndex);
			position.y++;
			if (GetVoxel (position, false).hasContent != 1)
				return true;
			position.y -= 2;
			if (GetVoxel (position, false).hasContent != 1)
				return true;
			position.y++;
			position.x--;
			if (GetVoxel (position, false).hasContent != 1)
				return true;
			position.x += 2;
			if (GetVoxel (position, false).hasContent != 1)
				return true;
			position.x--;
			position.z--;
			if (GetVoxel (position, false).hasContent != 1)
				return true;
			position.z += 2;
			if (GetVoxel (position, false).hasContent != 1)
				return true;
			return false;
		}


		/// <summary>
		/// Requests a refresh of a given chunk. The chunk mesh will be recreated and the lightmap will be computed again.
		/// </summary>
		/// <param name="chunk">Chunk.</param>
		public void ChunkRedraw (VoxelChunk chunk) {
			ChunkRequestRefresh (chunk, true, true);
		}

		/// <summary>
		/// Gets the voxel under a given position. Returns Voxel.Empty if no voxel found.
		/// </summary>
		public Voxel GetVoxelUnder (Vector3 position) {
			VoxelHitInfo hitinfo;
			if (RayCastFast (position, Misc.vector3down, out hitinfo)) {
				return hitinfo.chunk.voxels [hitinfo.voxelIndex];
			}
			return Voxel.Empty;
		}

		/// <summary>
		/// Changes/set the tint color of one voxel
		/// </summary>
		/// <param name="chunk">Chunk.</param>
		/// <param name="voxelIndex">Voxel index.</param>
		/// <param name="color">Color.</param>
		public void VoxelSetColor (VoxelChunk chunk, int voxelIndex, Color32 color) {
			if (chunk != null) {
				#if UNITY_EDITOR
				CheckEditorTintColor ();
				#endif
				chunk.voxels [voxelIndex].color = color;
				ChunkRequestRefresh (chunk, false, true);
			}
		}

		/// <summary>
		/// Places a new voxel on a givel position in world space coordinates. Optionally plays a sound.
		/// </summary>
		/// <param name="position">Position.</param>
		/// <param name="voxelType">Voxel.</param>
		/// <param name="playSound">If set to <c>true</c> play sound.</param>
		public void VoxelPlace (Vector3 position, VoxelDefinition voxelType, bool playSound = false) {
			VoxelPlace (position, voxelType, playSound, Misc.colorWhite);
		}

		/// <summary>
		/// Places a new voxel on a givel position in world space coordinates. Optionally plays a sound.
		/// </summary>
		/// <param name="position">Position.</param>
		/// <param name="voxelType">Voxel.</param>
		/// <param name="tintColor">Tint color.</param>
		/// <param name="playSound">If set to <c>true</c> play sound.</param>
		public void VoxelPlace (Vector3 position, VoxelDefinition voxelType, Color tintColor, bool playSound = false) {
			VoxelPlace (position, voxelType, playSound, tintColor);
		}

		/// <summary>
		/// Places a default voxel on a givel position in world space coordinates with specified color. Optionally plays a sound.
		/// </summary>
		/// <param name="position">Position.</param>
		/// <param name="tintColor">The tint color for the voxel.</param>
		/// <param name="playSound">If set to <c>true</c> play sound when placing the voxel.</param>
		public void VoxelPlace (Vector3 position, Color tintColor, bool playSound = false) {
			VoxelPlace (position, defaultVoxel, playSound, tintColor);
		}


		/// <summary>
		/// Places a default voxel on an existing chunk with specified color. Optionally plays a sound.
		/// </summary>
		/// <param name="chunk">The chunk object.</param>
		/// <param name="voxelIndex">The index of the voxel.</param>
		/// <param name="tintColor">The tint color for the voxel.</param>
		/// <param name="playSound">If set to <c>true</c> play sound when placing the voxel.</param>
		public void VoxelPlace (VoxelChunk chunk, int voxelIndex, Color tintColor, bool playSound = false) {
			Vector3 position = GetVoxelPosition (chunk, voxelIndex);
			VoxelPlace (position, defaultVoxel, playSound, tintColor);
		}

		/// <summary>
		/// Places a list of voxels within a given chunk. List of voxels is given by a list of ModelBit structs.
		/// </summary>
		/// <param name="chunk">The chunk object.</param>
		/// <param name="voxels">The list of voxels to insert into the chunk.</param>
		/// <param name="indices">Optional user-provided list. Will contain the list of visible voxels if provided.</param>
		public void VoxelPlace (VoxelChunk chunk, List<ModelBit> voxels) {
			ModelPlace (chunk, voxels);
		}


		/// <summary>
		/// Places a voxel at a given position.
		/// </summary>
		/// <param name="chunk">The chunk object.</param>
		/// <param name="voxels">The list of voxels to insert into the chunk.</param>
		/// /// <param name="playSound">If set to <c>true</c> play sound when placing the voxel.</param>
		public void VoxelPlace (Vector3 position, Voxel voxel, bool playSound = false) {
			VoxelPlace (position, voxelDefinitions[voxel.typeIndex], voxel.color, playSound);
		}


		/// <summary>
		/// Places a new voxel on a givel position in world space coordinates. Optionally plays a sound.
		/// </summary>
		/// <param name="position">Position.</param>
		/// <param name="voxelType">Voxel.</param>
		/// <param name="playSound">If set to <c>true</c> play sound when placing the voxel.</param>
		/// <param name="tintColor">The tint color for the voxel.</param>
		public void VoxelPlace (Vector3 position, VoxelDefinition voxelType, bool playSound, Color tintColor) {
			if (voxelType == null)
				return;

			#if UNITY_EDITOR
			if (!enableTinting && tintColor != Color.white) {
				Debug.Log ("Option enableTinting is disabled. To use colored voxels, please enable the option in the VoxelPlayEnvironment component inspector.");
			}
			#endif

			// Check if voxelType is known
			if (voxelType.index <= 0) {
				sessionUserVoxels.Add (voxelType);
				requireTextureArrayUpdate = true;
			}

			if (playSound) {
				PlayBuildSound (voxelType.buildSound, position);
			}

			VoxelChunk chunk;
			int voxelIndex;
			VoxelPlaceFast (position, voxelType, out chunk, out voxelIndex, tintColor);

			// Moves up character controller if voxel is put just on its position
			const float minDist = 0.5f;
			if (characterController != null && Vector3.SqrMagnitude (characterController.transform.position - GetVoxelPosition (chunk, voxelIndex)) < minDist * minDist) {
				characterController.transform.position += Misc.vector3up * 0.5f;
			}
		}



		/// <summary>
		/// Damages a voxel.
		/// </summary>
		/// <returns><c>true</c>, if voxel was hit, <c>false</c> otherwise.</returns>
		/// <param name="position">Position.</param>
		public bool VoxelDamage (Vector3 position, byte damage, int damageRadius) {
			return RayHit (position, Misc.vector3down, damage, 0, damageRadius);
		}



		/// <summary>
		/// Clears a voxel.
		/// </summary>
		/// <returns><c>true</c>, if voxel was destroyed, <c>false</c> otherwise.</returns>
		/// <param name="position">Position.</param>
		public bool VoxelDestroy (VoxelChunk chunk, int voxelIndex) {
			if (chunk == null)
				return false;
			if (chunk.voxels [voxelIndex].hasContent == 1) {
				VoxelDestroyFast (chunk, voxelIndex);
				return true;
			}
			return false;
		}

		/// <summary>
		/// Clears a voxel.
		/// </summary>
		/// <returns><c>true</c>, if voxel was destroyed, <c>false</c> otherwise.</returns>
		/// <param name="position">Position.</param>
		/// <param name="createChunkIfNotExists">Pass true if you need to make sure the terrain has been generated before destroying this voxel.</param>
		public bool VoxelDestroy (Vector3 position, bool createChunkIfNotExists = false) {
			VoxelChunk chunk;
			int voxelIndex;
			if (!GetVoxelIndex (position, out chunk, out voxelIndex, createChunkIfNotExists)) {
				return false;
			}
			if (chunk.voxels [voxelIndex].hasContent == 1) {
				VoxelDestroyFast (chunk, voxelIndex);
				return true;
			}
			return false;
		}

		/// <summary>
		/// Returns true if a given voxel is dynamic
		/// </summary>
		/// <param name="chunk">Chunk.</param>
		/// <param name="voxelIndex">Voxel index.</param>
		public bool VoxelIsDynamic (VoxelChunk chunk, int voxelIndex) {
			VoxelPlaceholder placeHolder = GetVoxelPlaceholder (chunk, voxelIndex, true);
			if (placeHolder == null)
				return false;

			if (placeHolder.modelMeshFilter == null)
				return false;

			return true;
		}

		/// <summary>
		/// Converts a voxel into a dynamic gameobject. If voxel has already been converted, it just returns the reference to the gameobject.
		/// </summary>
		/// <returns>The Gameobject.</returns>
		public GameObject VoxelGetDynamic (VoxelChunk chunk, int voxelIndex, bool addRigidbody = false) {

			if (chunk == null)
				return null;

			VoxelDefinition vd = voxelDefinitions[chunk.voxels [voxelIndex].typeIndex];
			if (vd.renderType != RenderType.Custom && vd.renderType != RenderType.Opaque && vd.renderType != RenderType.OpaqueNoAO && vd.renderType != RenderType.Cutout) {
				#if UNITY_EDITOR
				Debug.LogError ("Only opaque, opaque-no-AO and cutout voxel types can be converted to dynamic voxels.");
				#endif
				return null;
			}

			VoxelPlaceholder placeHolder = GetVoxelPlaceholder (chunk, voxelIndex, true);
			if (placeHolder == null)
				return null;

			if (placeHolder.modelMeshFilter != null) {
				return placeHolder.gameObject;
			}

			VoxelDefinition vdDyn = vd.dynamicDefinition;

			if (vdDyn == null) {
				// Setup and save voxel definition
				vd.dynamicDefinition = vdDyn = ScriptableObject.CreateInstance<VoxelDefinition> ();
				vdDyn.renderType = RenderType.Custom;
				vdDyn.textureIndexBottom = vd.textureIndexBottom;
				vdDyn.textureIndexSide = vd.textureIndexSide;
				vdDyn.textureIndexTop = vd.textureIndexTop;
				vdDyn.scale = vd.scale;
				vdDyn.offset = vd.offset;
				vdDyn.rotation = vd.rotation;
				vdDyn.sampleColor = vd.sampleColor;
				vdDyn.promotesTo = vd.promotesTo;
				vdDyn.playerDamageDelay = vd.playerDamageDelay;
				vdDyn.playerDamage = vd.playerDamage;
				vdDyn.pickupSound = vd.pickupSound;
				vdDyn.landingSound = vd.landingSound;
				vdDyn.jumpSound = vd.jumpSound;
				vdDyn.impactSound = vd.impactSound;
				vdDyn.footfalls = vd.footfalls;
				vdDyn.destructionSound = vd.destructionSound;
				vdDyn.canBeCollected = vd.canBeCollected;
				vdDyn.buildSound = vd.buildSound;
				vdDyn.navigatable = true;
				vdDyn.windAnimation = false;
				vdDyn.model = MakeDynamicCubeFromVoxel (chunk, voxelIndex);
				vdDyn.name = vd.name + " (Dynamic)";
				AddVoxelTextures (vdDyn);
			}

			// Add rigid body
			if (addRigidbody && placeHolder.GetComponent<Rigidbody> () == null) {
				placeHolder.gameObject.AddComponent<Rigidbody> ();
			}

			// Clear any vegetation on top
			VoxelChunk topChunk;
			int topIndex;
			if (GetVoxelIndex (chunk, voxelIndex, 0, 1, 0, out topChunk, out topIndex)) {
				if (topChunk.voxels [topIndex].hasContent == 1 && voxelDefinitions[topChunk.voxels [topIndex].typeIndex].renderType == RenderType.CutoutCross) {
					VoxelDestroyFast (topChunk, topIndex);
				}
			}

			// Update chunk
			Color32 color = chunk.voxels [voxelIndex].color;
			chunk.voxels [voxelIndex].Set (vdDyn, color);
			chunk.modified = true;
			ChunkRequestRefresh (chunk, true, true);

			// Triggers event
			if (OnChunkChanged != null) {
				OnChunkChanged (chunk);
			}

			return placeHolder.gameObject;
		}


		/// <summary>
		/// Creates a recoverable voxel and throws it at given position, direction and strength
		/// </summary>
		/// <param name="position">Position.</param>
		/// <param name="direction">Direction.</param>
		/// <param name="voxelType">Voxel definition.</param>
		public void VoxelThrow (Vector3 position, Vector3 direction, float velocity, VoxelDefinition voxelType, Color32 color) {
			GameObject voxelGO = CreateRecoverableVoxel (position, voxelType, color, GetVoxelLight (position));
			if (voxelGO == null)
				return;
			Rigidbody rb = voxelGO.GetComponent<Rigidbody> ();
			if (rb == null)
				return;
			rb.velocity = direction * velocity;
		}

		/// <summary>
		/// Clears a chunk given a world space position.
		/// </summary>
		/// <param name="position">Position.</param>
		public bool ChunkDestroy (Vector3 position) {
			VoxelChunk chunk;
			GetChunk (position, out chunk, false);
			return ChunkDestroy (chunk);
		}


		/// <summary>
		/// Clears a chunk given a world space position.
		/// </summary>
		/// <param name="position">Position.</param>
		public bool ChunkDestroy (VoxelChunk chunk) {
			if (chunk == null)
				return false;
			ChunkClearFast (chunk);

			// Refresh rendering
			UpdateChunkRR (chunk);

			// Triggers event
			if (OnChunkChanged != null) {
				OnChunkChanged (chunk);
			}
			return true;
		}


		/// <summary>
		/// Returns true if the underline NavMEs
		/// </summary>
		/// <returns><c>true</c>, if has nav mesh ready was chunked, <c>false</c> otherwise.</returns>
		/// <param name="chunk">Chunk.</param>
		public bool ChunkHasNavMeshReady (VoxelChunk chunk) {
			return chunk.navMeshSourceIndex >= 0 && !navMeshHasNewData && !navMeshIsUpdating;
		}


		/// <summary>
		/// Gets the terrain height under a given position, optionally including water
		/// </summary>
		public float GetTerrainHeight (Vector3 position, bool includeWater = false) {

			if ((object)heightMapCache == null)
				return 0;
			float groundLevel = GetHeightMapInfoFast (position.x, position.z).groundLevel;
			if (includeWater && waterLevel > groundLevel) {
				return waterLevel + 0.9f;
			} else {
				return groundLevel + 1f;
			}
		}

		/// <summary>
		/// Gets info about the terrain on a given position
		/// </summary>
		public HeightMapInfo GetTerrainInfo (Vector3 position) {
			return GetHeightMapInfoFast (position.x, position.z);
		}


		/// <summary>
		/// Gets the computed light amount at a given position in 0..1 range
		/// </summary>
		/// <returns>The light intensity.</returns>
		public float GetVoxelLight (Vector3 position) {
			VoxelChunk chunk;
			int voxelIndex;
			return GetVoxelLight (position, out chunk, out voxelIndex);
		}


		/// <summary>
		/// Gets the computed light amount at a given position in 0..1 range
		/// </summary>
		/// <returns>The light intensity at the voxel position..</returns>
		public float GetVoxelLight (Vector3 position, out VoxelChunk chunk, out int voxelIndex) {
			chunk = null;
			voxelIndex = 0;
			if (!globalIllumination) {
				return 1f;
			}

			if (GetVoxelIndex (position, out chunk, out voxelIndex, false)) {
				if (chunk.voxels [voxelIndex].opaque < FULL_OPAQUE) {
					return chunk.voxels [voxelIndex].lightMesh / 15f;
				}
				// voxel has contents try to retrieve light information from nearby voxels
				int nearby = voxelIndex + ONE_Y_ROW;
				if (nearby < chunk.voxels.Length && chunk.voxels [nearby].opaque < FULL_OPAQUE) {
					return chunk.voxels [nearby].lightMesh / 15f;
				}
				nearby = voxelIndex - ONE_Y_ROW;
				if (nearby >= 0 && chunk.voxels [nearby].opaque < FULL_OPAQUE) {
					return chunk.voxels [nearby].lightMesh / 15f;
				}
				return chunk.voxels [voxelIndex].lightMesh / 15f;
			}

			// Estimate light by height
			float height = GetTerrainHeight (position, false);
			if (height >= position.y) { // is below surface, we assume a lightIntensity of 0
				return 0;
			}
			return 1f;
		}


		/// <summary>
		/// Creates a placeholder for a given voxel.
		/// </summary>
		/// <returns>The voxel placeholder.</returns>
		/// <param name="chunk">Chunk.</param>
		/// <param name="voxelIndex">Voxel index.</param>
		/// <param name="createIfNotExists">If set to <c>true</c> create if not exists.</param>
		public VoxelPlaceholder GetVoxelPlaceholder (VoxelChunk chunk, int voxelIndex, bool createIfNotExists = true) {
			if (chunk == null)
				return null;

			bool phArrayCreated = (object)chunk.placeholders != null;

			if (phArrayCreated) {
				int count = chunk.placeholders.Count;
				for (int k = 0; k < count; k++) {
					VoxelPlaceholder ph = chunk.placeholders [k];
					if (ph.voxelIndex == voxelIndex) {
						return  ph;
					}
				}
			}

			// Create placeholder
			if (createIfNotExists) {
				if (!phArrayCreated) {
					chunk.placeholders = new List<VoxelPlaceholder> ();
				}
				GameObject placeHolder = new GameObject ("Voxel Placeholder");
				placeHolder.transform.SetParent (chunk.transform, false);
				placeHolder.transform.position = GetVoxelPosition (chunk, voxelIndex);
				VoxelPlaceholder voxelPlaceholder = placeHolder.AddComponent<VoxelPlaceholder> ();
				voxelPlaceholder.chunk = chunk;
				voxelPlaceholder.voxelIndex = voxelIndex;
				voxelPlaceholder.bounds.center = Misc.vector3zero;
				voxelPlaceholder.bounds.size = Misc.vector3one;
				if (chunk.voxels [voxelIndex].hasContent == 1) {
					voxelPlaceholder.resistancePointsLeft = voxelDefinitions[chunk.voxels [voxelIndex].typeIndex].resistancePoints;
				}
				chunk.placeholders.Add (voxelPlaceholder);
				return voxelPlaceholder;
			}

			return null;
		}

		/// <summary>
		/// Destroys a voxel placeholder
		/// </summary>
		/// <param name="chunk">Chunk.</param>
		/// <param name="voxelIndex">Voxel index.</param>
		public void VoxelPlaceholderDestroy (VoxelChunk chunk, int voxelIndex) {
			
			if (chunk == null)
				return;

			bool phArrayCreated = (object)chunk.placeholders != null;

			if (phArrayCreated) {
				int count = chunk.placeholders.Count;
				for (int k = 0; k < count; k++) {
					VoxelPlaceholder ph = chunk.placeholders [k];
					if (ph.voxelIndex == voxelIndex) {
						DestroyImmediate (ph.gameObject);
						chunk.placeholders.RemoveAt (k);
						return;
					}
				}
			}
		}


		/// <summary>
		/// Array with all available item definitions in current world
		/// </summary>
		[NonSerialized]
		public List<InventoryItem> allItems;


		/// <summary>
		/// Places a model defined by a matrix of colors in the world at the given position. Colors with zero alpha will be skipped.
		/// </summary>
		/// <param name="position">Position.</param>
		/// <param name="colors">3-dimensional array of colors (y/z/x).</param>
		public void ModelPlace (Vector3 position, Color[,,] colors) {
			#if UNITY_EDITOR
			CheckEditorTintColor ();
			#endif

			Vector3 pos;
			VoxelChunk lastChunk = null;
			int maxY = colors.GetUpperBound (0) + 1;
			int maxZ = colors.GetUpperBound (1) + 1;
			int maxX = colors.GetUpperBound (2) + 1;
			int halfZ = maxZ / 2;
			int halfX = maxX / 2;
			VoxelDefinition vd = defaultVoxel;
			for (int y = 0; y < maxY; y++) {
				pos.y = position.y + y;
				for (int z = 0; z < maxZ; z++) {
					pos.z = position.z + z - halfZ;
					for (int x = 0; x < maxX; x++) {
						Color32 color = colors [y, z, x];
						if (color.a == 0)
							continue;
						pos.x = position.x + x - halfX;
						VoxelChunk chunk;
						int voxelIndex;
						if (GetVoxelIndex (pos, out chunk, out voxelIndex)) {
							chunk.voxels [voxelIndex].Set (vd, color);
							if (chunk != lastChunk) {
								lastChunk = chunk;
								chunk.modified = true;
								if (!lastChunk.inqueue) {
									ChunkRequestRefresh (lastChunk, true, true);
								}
							}
						}
					}
				}
			}
		}


		/// <summary>
		/// Places a model defined by a matrix of colors in the world at the given position. Colors with zero alpha will be skipped.
		/// </summary>
		/// <param name="position">Position.</param>
		/// <param name="voxels">3-dimensional array of voxel definitions (y/z/x).</param>
		/// <param name="colors">3-dimensional array of colors (y/z/x).</param>
		public void ModelPlace (Vector3 position, VoxelDefinition[,,] voxels, Color[,,] colors = null) {
			Vector3 pos;
			VoxelChunk lastChunk = null;
			int maxY = voxels.GetUpperBound (0) + 1;
			int maxZ = voxels.GetUpperBound (1) + 1;
			int maxX = voxels.GetUpperBound (2) + 1;
			int halfZ = maxZ / 2;
			int halfX = maxX / 2;
			bool hasColors = colors != null;
			if (hasColors) {
				#if UNITY_EDITOR
				CheckEditorTintColor ();
				#endif
				if (colors.GetUpperBound (0) != voxels.GetUpperBound (0) || colors.GetUpperBound (1) != voxels.GetUpperBound (1) || colors.GetUpperBound (2) != voxels.GetUpperBound (2)) {
					Debug.LogError ("Colors array dimensions must match those of voxels array.");
					return;
				}
			}
			for (int y = 0; y < maxY; y++) {
				pos.y = position.y + y;
				for (int z = 0; z < maxZ; z++) {
					pos.z = position.z + z - halfZ;
					for (int x = 0; x < maxX; x++) {
						VoxelDefinition vd = voxels [y, z, x];
						if (vd != null) {
							pos.x = position.x + x - halfX;
							VoxelChunk chunk;
							int voxelIndex;
							if (GetVoxelIndex (pos, out chunk, out voxelIndex)) {
								if (hasColors) {
									chunk.voxels [voxelIndex].Set (vd, colors [y, z, x]);
								} else {
									chunk.voxels [voxelIndex].Set (vd, Misc.color32White);
								}
								if (chunk != lastChunk) {
									lastChunk = chunk;
									chunk.modified = true;
									if (!lastChunk.inqueue) {
										ChunkRequestRefresh (lastChunk, true, true);
									}
								}
							}
						}
					}
				}
			}
		}


		/// <summary>
		/// Places a list of voxels given by a list of ModelBits within a given chunk
		/// </summary>
		/// <param name="position">Position.</param>
		/// <param name="bits">List of voxels described by a list of modelbit structs</param>
		/// <param name="indices">Optional user-provided list. Will contain the list of visible voxels if provided.</param>
		public void ModelPlace (VoxelChunk chunk, List<ModelBit>bits) {
			int count = bits.Count;
			for (int b = 0; b < count; b++) {
				int bitIndex = bits [b].voxelIndex;
				VoxelDefinition vd = bits [b].voxelDefinition ?? defaultVoxel;
				chunk.voxels [bitIndex].Set (vd, bits [b].colorOrDefault);
			}
			chunk.modified = true;
			ChunkRequestRefresh (chunk, true, true);
		}


		/// <summary>
		/// Places a model in the world at the given position
		/// </summary>
		/// <param name="position">Position.</param>
		/// <param name="model">Model Definition.</param>
		/// <param name="rotationDegrees">0, 90, 180 or 270 degree rotation. A value of 360 means random rotation.</param>
		/// <param name="colorBrightness">Optionally pass a color brightness value. This value is multiplied by the voxel color.</param>
		/// <param name="fitTerrain">If set to true, vegetation and trees are prevented and some space is flattened around the model.</param>
		/// <param name="indices">Optional user-provided list. If provided, it will contain the indices and positions of all visible voxels in the model</param>
		public void ModelPlace (Vector3 position, ModelDefinition model, int rotationDegrees = 0, float colorBrightness = 1f, bool fitTerrain = false, List<VoxelIndex> indices = null) {

			if (model == null)
				return;

			Vector3 pos;
			int modelOneYRow = model.sizeZ * model.sizeX;
			int modelOneZRow = model.sizeX;
			int halfSizeX = model.sizeX / 2;
			int halfSizeZ = model.sizeZ / 2;

			if (rotationDegrees == 360) {
				switch (UnityEngine.Random.Range (0, 4)) {
				case 0:
					rotationDegrees = 90;
					break;
				case 1:
					rotationDegrees = 180;
					break;
				case 2:
					rotationDegrees = 270;
					break;
				}
			}

			bool indicesProvided = indices != null;
			if (indicesProvided)
				indices.Clear ();
			VoxelIndex index = new VoxelIndex ();
			VoxelChunk lastChunk = null;
			int tmp;
			for (int b = 0; b < model.bits.Length; b++) {
				int bitIndex = model.bits [b].voxelIndex;
				int py = bitIndex / modelOneYRow;
				int remy = bitIndex - py * modelOneYRow;
				int pz = remy / modelOneZRow;
				int px = remy - pz * modelOneZRow;
				switch (rotationDegrees) {
				case 90:
					tmp = px;
					px = halfSizeZ - pz;
					pz = tmp - halfSizeX;
					break;
				case 180:
					tmp = px;
					px = pz - halfSizeZ;
					pz = tmp - halfSizeX;
					break;
				case 270:
					tmp = px;
					px = pz - halfSizeZ;
					pz = halfSizeX - tmp;
					break;
				default:
					px -= halfSizeX;
					pz -= halfSizeZ;
					break;
				}

				pos.x = position.x + model.offsetX + px;
				pos.y = position.y + model.offsetY + py;
				pos.z = position.z + model.offsetZ + pz;

				VoxelChunk chunk;
				int voxelIndex;
				if (GetVoxelIndex (pos, out chunk, out voxelIndex)) {
					Color32 color = model.bits [b].colorOrDefault;
					VoxelDefinition vd = model.bits [b].voxelDefinition ?? defaultVoxel;
					bool emptyVoxel = model.bits [b].isEmpty;
					if (emptyVoxel) {
						chunk.voxels [voxelIndex] = Voxel.Empty;
					} else {
						if (colorBrightness != 1f) {
							color.r = (byte)(color.r * colorBrightness);
							color.g = (byte)(color.g * colorBrightness);
							color.b = (byte)(color.b * colorBrightness);
						}
						chunk.voxels [voxelIndex].Set (vd, color);
						if (indicesProvided) {
							index.chunk = chunk;
							index.voxelIndex = voxelIndex;
							index.position = pos;
							indices.Add (index);
						}
					}

					// Prevent tree population
					chunk.allowTrees = false;
					chunk.modified = true;

					if (fitTerrain && !emptyVoxel) {
						// Fill beneath row 1
						if (py == 0) {
							Vector3 under = pos;
							under.y -= 1;
							for (int k = 0; k < 100; k++, under.y--) {
								VoxelChunk lowChunk;
								int vindex;
								GetVoxelIndex (under, out lowChunk, out vindex, false);
								if (lowChunk != null && lowChunk.voxels [vindex].opaque < FULL_OPAQUE) {
									lowChunk.voxels [vindex].Set (vd, color);
									if (lowChunk != lastChunk) {
										lastChunk = lowChunk;
										if (!lastChunk.inqueue) {
											ChunkRequestRefresh (lastChunk, true, true);
										}
									}
								} else {
									break;
								}
							}
						}
					}

					if (chunk != lastChunk) {
						lastChunk = chunk;
						if (!lastChunk.inqueue) {
							ChunkRequestRefresh (lastChunk, true, true);
						}
					}
				}
			}
		}


		/// <summary>
		/// Fills empty voxels inside the model with Empty blocks so they clear any other existing voxel when placing the model
		/// </summary>
		/// <param name="model">Model.</param>
		public void ModelFillInside (ModelDefinition model) {

			if (model == null)
				return;
			int sx = model.sizeX;
			int sy = model.sizeY;
			int sz = model.sizeZ;
			Voxel[] voxels = new Voxel[sy * sz * sx];

			// Load model inside the temporary voxel array
			for (int k = 0; k < model.bits.Length; k++) {
				int voxelIndex = model.bits [k].voxelIndex;
				voxels [voxelIndex].hasContent = 1;
			}

			// Fill inside
			List<ModelBit> newBits = new List<ModelBit> (model.bits);
			ModelBit empty = new ModelBit ();
			empty.isEmpty = true;
			for (int z = 0; z < sz; z++) {
				for (int x = 0; x < sx; x++) {
					int miny = -1;
					int maxy = sy;
					for (int y = 0; y < sy; y++) {
						int voxelIndex = y * sz * sx + z * sx + x;
						if (voxels [voxelIndex].hasContent == 1) {
							if (miny < 0)
								miny = y;
							else
								maxy = y;
						}
					}
					if (miny >= 0) {
						miny++;
						maxy--;
						for (int y = miny; y < maxy; y++) {
							int voxelIndex = y * sz * sx + z * sx + x;
							if (voxels [voxelIndex].hasContent != 1) {
								empty.voxelIndex = voxelIndex;
								newBits.Add (empty);
							}
						}
					}
				}
			}
			model.bits = newBits.ToArray ();
		}


		/// <summary>
		/// Reloads all world textures
		/// </summary>
		public void ReloadTextures () {
			LoadWorldTextures ();
		}


		/// <summary>
		/// Sets or cancel build mode
		/// </summary>
		/// <param name="buildMode">If set to <c>true</c> build mode.</param>
		public void SetBuildMode (bool buildMode) {
			if (!enableBuildMode)
				return;
			this.buildMode = buildMode;

			// select first item
			VoxelPlayPlayer.instance.selectedItemIndex = 0;

			// refresh inventory
			VoxelPlayUI.instance.RefreshInventoryContents ();
		}

		/// <summary>
		/// Shows a custom message into the status text.
		/// </summary>
		/// <param name="txt">Text.</param>
		public void ShowMessage (string txt, float displayDuration = 4, bool flashEffect = false) {
			if (lastMessage == txt)
				return;
			lastMessage = txt;

			ExecuteInMainThread (delegate () {
				VoxelPlayUI.instance.AddMessage (txt, displayDuration, flashEffect);
			});
		}


		/// <summary>
		/// Get an item from the allItems array of a given category and voxel type
		/// </summary>
		/// <returns>The item of requested category and type.</returns>
		/// <param name="category">Category.</param>
		/// <param name="voxelType">Voxel type.</param>
		public ItemDefinition GetItemByType (ITEM_CATEGORY category, VoxelDefinition voxelType = null) {
			if (allItems == null)
				return null;
			int allItemsCount = allItems.Count;
			for (int k = 0; k < allItemsCount; k++) {
				if (allItems [k].item.category == category && (allItems [k].item.voxelType == voxelType || voxelType == null)) {
					return allItems [k].item;
				}
			}
			return null;
		}


		/// <summary>
		/// Adds a torch.
		/// </summary>
		/// <param name="hitInfo">Information about the hit position where the torch should be attached.</param>
		public GameObject TorchAttach (VoxelHitInfo hitInfo) {
			return TorchAttachInt (hitInfo);
		}

		/// <summary>
		/// Removes an existing torch.
		/// </summary>
		/// <param name="chunk">Chunk where the torch is currently attached.</param>
		/// <param name="gameObject">Game object of the torch itself.</param>
		public void TorchDetach (VoxelChunk chunk, GameObject gameObject) {
			TorchDetachInt (chunk, gameObject);
		}



		/// <summary>
		/// Voxel Play Input manager reference.
		/// </summary>
		[NonSerialized]
		public VoxelPlayInputController input;

		#endregion

	}

}