using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace VoxelPlay {

	public delegate void OnPlayerGetDamaged (ref byte damage);

	[RequireComponent (typeof(CharacterController))]
	[RequireComponent (typeof(AudioSource))]
	[ExecuteInEditMode]
	public partial class VoxelPlayFirstPersonController : MonoBehaviour
    {
		[Header ("Start Position")]
		[Tooltip ("Places player on a random position in world which is flat. If this option is not enabled, the current gameobject transform position will be used.")]
		public bool startOnFlat = true;

		[Tooltip ("Number of terrain checks to determine a flat position. The more iterations the lower the resulting starting position.")]
		[Range (1, 100)]
		public int startOnFlatIterations = 50;

		[Header ("State Flags (Informative)")]
		[Tooltip ("Player is flying - can go up/down with E and Q keys.")]
		public bool isFlying;

		[Tooltip ("Player uses walking speed")]
		public bool isWalking;

		[Tooltip ("Player is either on water surface or under water")]
		public bool isInWater;

		[Tooltip ("Player is on water surface.")]
		public bool isSwimming;

		[Tooltip ("Player is below water surface.")]
		public bool isUnderwater;

		[Tooltip ("Player is on ground.")]
		public bool isGrounded;

		[Tooltip ("Player is crouched.")]
		public bool isCrouched;

		[Header ("Movement")]
		public float walkSpeed = 5f;
		public float runSpeed = 10f;
		public float flySpeed = 20f;
		public float swimSpeed = 3.7f;
		[Range (0f, 1f)] public  float runstepLenghten = 0.7f;
		public  float jumpSpeed = 10f;
		public  float stickToGroundForce = 10f;
		public  float gravityMultiplier = 2f;
		public  MouseLook mouseLook;
		public  bool useFovKick = true;
		[SerializeField] private FOVKick m_FovKick = new FOVKick ();
		public bool useHeadBob = true;
		[SerializeField] private CurveControlledBob m_HeadBob = new CurveControlledBob ();
		[SerializeField] private LerpControlledBob m_JumpBob = new LerpControlledBob ();
		public float footStepInterval = 5;
		public float swimStrokeInterval = 8;
		public bool limitBoundsEnabled;
		public Bounds limitBounds;

		[Header ("Orbit")]
		public bool orbitMode;
		public Vector3 lookAt;
		public float minDistance = 1f;
		public float maxDistance = 100f;


		[Header ("Audio Effects")]
		// an array of footstep sounds that will be randomly selected from.
		public AudioClip[] footstepSounds;

		// an array of swim stroke sounds that will be randomly selected from.
		public AudioClip[] swimStrokeSounds;

		// the sound played when character leaves the ground.
		public AudioClip jumpSound;

		// the sound played when character touches back on ground.
		public AudioClip landSound;

		// the sound played when character enters water.
		public AudioClip waterSplash;

		// a reference to the inventory component
		VoxelPlayPlayer _player;

		public VoxelPlayPlayer player {
			get {
				if (_player == null) {
					_player = GetComponent<VoxelPlayPlayer> ();
					if (_player == null) {
						_player = gameObject.AddComponent<VoxelPlayPlayer> ();
					}
				}
				return _player;
			}
		}

		Camera m_Camera;
		bool m_Jump;
		Vector3 m_Input;
        private Vector3 m_MoveDir = Vector3.zero;
		CharacterController m_CharacterController;
		CollisionFlags m_CollisionFlags;
		bool m_PreviouslyGrounded;
		Vector3 m_OriginalCameraPosition;
		float m_StepCycle;
		float m_NextStep;
		bool m_Jumping;
		AudioSource m_AudioSource;
		VoxelPlayEnvironment env;
		float lastHitButtonPressed;
		GameObject underwaterPanel;
		float lastDiveTime;
		Material underWaterMat;
		Transform crouch;

		int lastNearClipPosX, lastNearClipPosY, lastNearClipPosZ;
		Vector3 curPos;
		int lastPosX, lastPosY, lastPosZ;
		float waterLevelTop;
		VoxelDefinition lastVoxelType;

		static VoxelPlayFirstPersonController _FPSController;

		const float switchDuration = 2f;
		bool switching;
		float switchingStartTime;
		float switchingLapsed;
		VoxelPlayInputController input;
		float nextPlayerDamageTime;

		public static VoxelPlayFirstPersonController instance {
			get {
				if (_FPSController == null) {
					_FPSController = FindObjectOfType<VoxelPlayFirstPersonController> ();
				}
				return _FPSController;
			}
		}

		void OnEnable () {
			m_CharacterController = GetComponent<CharacterController> ();
			env = VoxelPlayEnvironment.instance;
			if (env == null) {
				Debug.LogError ("Voxel Play Environment must be added first.");
			} else {
				env.characterController = m_CharacterController;
			}
			crouch = transform.Find ("Crouch").transform;
		}

		void Start () {
			m_AudioSource = GetComponent<AudioSource> ();
			m_Camera = GetComponentInChildren<Camera> ();
			if (env != null)
				env.cameraMain = m_Camera;
			m_Camera.nearClipPlane = 0.1f; // good value for interacting with water
			m_OriginalCameraPosition = m_Camera.transform.localPosition;
			m_FovKick.Setup (m_Camera);
			m_HeadBob.Setup (m_Camera, footStepInterval);
			m_StepCycle = 0f;
			m_NextStep = m_StepCycle / 2f;
			m_Jumping = false;

			if (env == null || !env.applicationIsPlaying)
				return;

			underwaterPanel = Instantiate<GameObject> (Resources.Load<GameObject> ("VoxelPlay/Prefabs/UnderwaterPanel"), m_Camera.transform);
			underwaterPanel.name = "UnderwaterPanel";
			underWaterMat = underwaterPanel.GetComponent<Renderer> ().sharedMaterial;
			underwaterPanel.transform.localPosition = new Vector3 (0, 0, m_Camera.nearClipPlane + 0.001f);
			underwaterPanel.SetActive (false);

			ToggleCharacterController (false);

			// Position character on ground
			if (!env.saveFileIsLoaded) {
				if (startOnFlat) {
					float minAltitude = env.world.terrainGenerator.maxHeight;
					Vector3 flatPos = transform.position;
					Vector3 randomPos;
					for (int k = 0; k < startOnFlatIterations; k++) {
						randomPos = Random.insideUnitSphere * 1000;
						float alt = env.GetTerrainHeight (randomPos);
						if (alt < minAltitude && alt >= env.waterLevel + 1) {
							minAltitude = alt;
							randomPos.y = alt + m_CharacterController.height + 1;
							flatPos = randomPos;
						}
					}
					transform.position = flatPos;
				}
			}

			input = env.input;

			InitCrosshair ();

			SetOrbitMode (orbitMode);
			mouseLook.Init (transform, m_Camera.transform, input);

			if (!env.initialized) {
				env.OnInitialized += () => WaitForCurrentChunk ();
			} else {
				WaitForCurrentChunk ();
			}
		}


		/// <summary>
		/// Disables character controller until chunk is ready
		/// </summary>
		public void WaitForCurrentChunk () {
			ToggleCharacterController (false);
			StartCoroutine (WaitForCurrentChunkCoroutine ());
		}

		/// <summary>
		/// Enables/disables character controller
		/// </summary>
		/// <param name="state">If set to <c>true</c> state.</param>
		public void ToggleCharacterController (bool state) {
			m_CharacterController.enabled = state;
			enabled = state;
		}

		/// <summary>
		/// Ensures player chunk is finished before allow player movement / interaction with colliders
		/// </summary>
		IEnumerator WaitForCurrentChunkCoroutine () {
			// Wait until current player chunk is rendered
			WaitForSeconds w = new WaitForSeconds (0.2f);
			VoxelChunk chunk = env.GetCurrentChunk ();
			for (int k = 0; k < 20; k++) {
				if (chunk.isRendered)
					break;
				yield return w;
			}
			Unstuck ();
			ToggleCharacterController (true);
		}

		void Update () {
			if (env == null || !env.applicationIsPlaying || !env.initialized || input == null)
				return;

			RotateView ();

			if (orbitMode)
				isFlying = true;

			// the jump state needs to read here to make sure it is not missed
			if (!m_Jump && !isFlying) {
				m_Jump = input.GetButtonDown (INPUT_BUTTON_NAMES.Jump);
			}

			curPos = transform.position;

			CheckFootfalls ();

			if (!m_PreviouslyGrounded && m_CharacterController.isGrounded) {
				StartCoroutine (m_JumpBob.DoBobCycle ());
				PlayLandingSound ();
				m_MoveDir.y = 0f;
				m_Jumping = false;
			}
			if (!m_CharacterController.isGrounded && !m_Jumping && m_PreviouslyGrounded) {
				m_MoveDir.y = 0f;
			}

			m_PreviouslyGrounded = m_CharacterController.isGrounded;

			// Process click events
			if (input.focused) {
				bool leftAltPressed = input.GetButton (INPUT_BUTTON_NAMES.LeftAlt);
				bool leftShiftPressed = input.GetButton (INPUT_BUTTON_NAMES.LeftShift);
				bool leftControlPressed = input.GetButton (INPUT_BUTTON_NAMES.LeftControl);
				bool fire1Pressed = input.GetButton (INPUT_BUTTON_NAMES.Button1);
				bool fire2Clicked = input.GetButtonDown (INPUT_BUTTON_NAMES.Button2);
				if (!leftShiftPressed && !leftAltPressed && !leftControlPressed) {
					if (Time.time - lastHitButtonPressed > player.hitDelay) {
						if (fire1Pressed) {
							DoHit (player.hitDamage);
						}
					}
					if (fire2Clicked) {
						DoHit (0);
					}
				}

				if (input.GetButtonDown (INPUT_BUTTON_NAMES.Build)) {
					env.SetBuildMode (!env.buildMode);
					if (env.buildMode) {
						#if UNITY_EDITOR
						env.ShowMessage ("<color=green>Entered <color=yellow>Build Mode</color>. Press <color=white>B</color> to cancel, <color=white>V</color> to enter the Constructor.</color>");
						#else
						env.ShowMessage ("<color=green>Entered <color=yellow>Build Mode</color>. Press <color=white>B</color> to cancel.</color>");
						#endif
					} else {
						env.ShowMessage ("<color=green>Back to <color=yellow>Normal Mode</color>.</color>");
					}
				}

				if (fire2Clicked && !leftAltPressed && !leftShiftPressed) {
					DoBuild ();
				}

				// Toggles Flight mode
				if (input.GetButtonDown (INPUT_BUTTON_NAMES.Fly)) {
					isFlying = !isFlying;
					if (isFlying) {
						env.ShowMessage ("<color=green>Flying <color=yellow>ON</color></color>");
					} else {
						env.ShowMessage ("<color=green>Flying <color=yellow>OFF</color></color>");
					}
				}

				if (isGrounded && input.GetButtonDown (INPUT_BUTTON_NAMES.Crouch)) {
					isCrouched = !isCrouched;
					if (isCrouched) {
						env.ShowMessage ("<color=green>Crouching <color=yellow>ON</color></color>");
					} else {
						env.ShowMessage ("<color=green>Crouching <color=yellow>OFF</color></color>");
					}
				} else if (input.GetButtonDown (INPUT_BUTTON_NAMES.Light)) {
					ToggleCharacterLight ();
				} else if (input.GetButtonDown (INPUT_BUTTON_NAMES.ThrowItem)) {
					ThrowCurrentItem ();
				}
			}

			// Check water
			CheckWaterStatus ();

			// Check crouch status
			if (!isInWater) {
				if (isCrouched && crouch.localPosition.y == 0) {
					crouch.transform.localPosition = new Vector3 (0, -1f, 0);
					m_CharacterController.stepOffset = 0.3f;
				} else if (!isCrouched && crouch.localPosition.y != 0) {
					crouch.transform.localPosition = Misc.vector3zero;
					m_CharacterController.stepOffset = 1f;
				}
			}

			#if UNITY_EDITOR
			UpdateConstructor ();
			#endif
		}

		public void SetOrbitMode (bool enableOrbitMode) {
			if (orbitMode != enableOrbitMode) {
				orbitMode = enableOrbitMode;
				switching = true;
				switchingStartTime = Time.time;
			}
			freeMode = orbitMode;
		}

		void CheckWaterStatus () {

			Vector3 nearClipPos = m_Camera.transform.position + m_Camera.transform.forward * (m_Camera.nearClipPlane + 0.001f);
			if (nearClipPos.x == lastNearClipPosX && nearClipPos.y == lastNearClipPosY && nearClipPos.z == lastNearClipPosZ)
				return;

			lastNearClipPosX = (int)nearClipPos.x;
			lastNearClipPosY = (int)nearClipPos.y;
			lastNearClipPosZ = (int)nearClipPos.z;

			bool wasInWater = isInWater;

			isInWater = false;
			isSwimming = false;
			isUnderwater = false;

			if (env.waterLevel == 0)
				return;

			// Check water on character controller position
			Voxel voxelCh = env.GetVoxel (curPos);
			CheckDamage (voxelCh.type);

			// Check if water surrounds camera
			Voxel voxelCamera = env.GetVoxel (nearClipPos);
			CheckDamage (voxelCamera.type);

			if (voxelCamera.waterLevel > 7) {
				// More water on top?
				Vector3 pos1Up = nearClipPos;
				pos1Up.y += 1f;
				Voxel voxel1Up = env.GetVoxel (pos1Up);
				if (voxel1Up.waterLevel > 0) {
					isUnderwater = true;
					waterLevelTop = nearClipPos.y + 1f;
				} else {
					waterLevelTop = FastMath.FloorToInt (nearClipPos.y) + 0.9f;
					isUnderwater = nearClipPos.y < waterLevelTop;
					isSwimming = !isUnderwater;
				}
				underWaterMat.color = voxelCamera.type.diveColor;
			} else if (voxelCh.waterLevel > 7) { // type == env.world.waterVoxel) {
				isSwimming = true;
				waterLevelTop = FastMath.FloorToInt (curPos.y) + 0.9f;
				underWaterMat.color = voxelCh.type.diveColor;

			}
			underWaterMat.SetFloat ("_WaterLevel", waterLevelTop);

			isInWater = isSwimming || isUnderwater;
			if (!wasInWater && isInWater) {
				PlayWaterSplashSound ();
				crouch.localPosition = Misc.vector3down * 0.6f; // crouch
			} else if (wasInWater && !isInWater) {
				crouch.localPosition = Misc.vector3zero;
			}

			// Show/hide underwater panel
			if (isInWater && !underwaterPanel.activeSelf) {
				underwaterPanel.SetActive (true);
			} else if (!isInWater && underwaterPanel.activeSelf) {
				underwaterPanel.SetActive (false);
			}
		}

		void CheckFootfalls () {
			if (isGrounded) {
				int x = (int)curPos.x;
				int y = (int)curPos.y;
				int z = (int)curPos.z;
				if (x != lastPosX || y != lastPosY || z != lastPosZ) {
					lastPosX = x;
					lastPosY = y;
					lastPosZ = z;
					Voxel voxel = env.GetVoxelUnder (curPos);
					if (voxel.type != lastVoxelType) {
						lastVoxelType = voxel.type;
						if (lastVoxelType != null) {
							SetFootstepSounds (lastVoxelType.footfalls, lastVoxelType.landingSound, lastVoxelType.jumpSound);
						}
						CheckDamage (voxel.type);
					}
				}
			}
		}

		void CheckDamage (VoxelDefinition voxelType) {
			if (voxelType == null)
				return;
			int playerDamage = voxelType.playerDamage;
			if (playerDamage > 0 && Time.time > nextPlayerDamageTime) {
				nextPlayerDamageTime = Time.time + voxelType.playerDamageDelay;
				player.life -= playerDamage;
			}
		}

		void DoHit (int damage) {
			lastHitButtonPressed = Time.time;
			Ray ray;
			if (freeMode) {
				ray = m_Camera.ScreenPointToRay (input.screenPos);
			} else {
				ray = m_Camera.ViewportPointToRay (Misc.vector2half);
			}
			env.RayHit (ray, damage, player.hitRange, player.hitDamageRadius);
		}

		void DoBuild () {
			if (player.selectedItemIndex < 0 || player.selectedItemIndex >= player.items.Count)
				return;

			ItemDefinition currentItem = player.GetSelectedItem ().item;
			switch (currentItem.category) {
			case ITEM_CATEGORY.Voxel:
				bool canPlace = crosshairOnBlock;
				Voxel existingVoxel = crosshairHitInfo.voxel;
				Vector3 placePos;
				if (currentItem.voxelType.renderType == RenderType.Transparent && !canPlace) {
					canPlace = true; // water can be poured anywhere
					placePos = m_Camera.transform.position + m_Camera.transform.forward * 3f;
				} else {
					placePos = crosshairHitInfo.voxelCenter + crosshairHitInfo.normal;
					if (canPlace && crosshairHitInfo.normal.y == 1) {
						// Make sure there's a valid voxel under position (ie. do not build a voxel on top of grass)
						VoxelDefinition vd = existingVoxel.type;
						canPlace = (vd != null && vd.renderType != RenderType.CutoutCross && (vd.renderType != RenderType.Transparent || currentItem.voxelType.renderType == RenderType.Transparent));
					}
				}
				if (canPlace) {
					if (existingVoxel.type == currentItem.voxelType && existingVoxel.type.renderType == RenderType.Custom && existingVoxel.type.promotesTo != null) {
						// Promote existing voxel
						env.VoxelDestroy (crosshairHitInfo.voxelCenter);
						env.VoxelPlace (crosshairHitInfo.voxelCenter, existingVoxel.type.promotesTo, true);
					} else {
						env.VoxelPlace (placePos, currentItem.voxelType, true);
					}
					if (!env.buildMode) {
						player.ConsumeItem ();
					}
				}
#if UNITY_EDITOR
                else if (env.constructorMode) {
					env.VoxelPlace (voxelHighlight.transform.position, currentItem.voxelType, true);
				}
#endif
				break;
			case ITEM_CATEGORY.Torch:
				if (crosshairOnBlock) {
					GameObject torchAttached = env.TorchAttach (crosshairHitInfo);
					if (!env.buildMode && torchAttached != null) {
						player.ConsumeItem ();
					}
				}
				break;
			}

		}

		private void PlayLandingSound () {
			if (isSwimming)
				return;
			m_AudioSource.clip = landSound;
			m_AudioSource.Play ();
			m_NextStep = m_StepCycle + .5f;
		}

		private void PlayWaterSplashSound () {
			if (Time.time - lastDiveTime < 1f)
				return;
			lastDiveTime = Time.time;
			if (waterSplash != null) {
				AudioSource.PlayClipAtPoint (waterSplash, curPos);
			}
		}

		private void FixedUpdate () {
			float speed;
			GetInput (out speed);

			Vector3 pos = transform.position;
			if (!m_Jumping && (isFlying || isInWater)) {
				m_MoveDir = m_Camera.transform.forward * m_Input.y + m_Camera.transform.right * m_Input.x + m_Camera.transform.up * m_Input.z;
				m_MoveDir *= speed;
				if (!isFlying) {
					if (m_MoveDir.y < 0) {
						m_MoveDir.y += 0.1f * Time.fixedDeltaTime;
					}
					if (m_Jump) {
						// Check if player is next to terrain
						if (env.CheckCollision (new Vector3 (pos.x + m_Camera.transform.forward.x, pos.y, pos.z + m_Camera.transform.forward.z))) {
							m_MoveDir.y = jumpSpeed * 0.5f;
							m_Jumping = true;
						}
						m_Jump = false;
					} else {
						m_MoveDir += Physics.gravity * gravityMultiplier * Time.fixedDeltaTime * 0.5f;
					}
					if (pos.y > waterLevelTop && m_MoveDir.y > 0) {
						m_MoveDir.y = 0; // do not exit water
					}
					ProgressSwimCycle (swimSpeed);
				}
			}
            else {
				// always move along the camera forward as it is the direction that it being aimed at
				Vector3 desiredMove = transform.forward * m_Input.y + transform.right * m_Input.x;

				// get a normal for the surface that is being touched to move along it
				RaycastHit hitInfo;
				Physics.SphereCast (pos, m_CharacterController.radius, Vector3.down, out hitInfo,
					m_CharacterController.height / 2f, Physics.AllLayers, QueryTriggerInteraction.Ignore);
				desiredMove = Vector3.ProjectOnPlane (desiredMove, hitInfo.normal).normalized;

				m_MoveDir.x = desiredMove.x * speed;
				m_MoveDir.z = desiredMove.z * speed;
				if (m_CharacterController.isGrounded) {
					m_MoveDir.y = -stickToGroundForce;

					if (m_Jump) {
						m_MoveDir.y = jumpSpeed;
						PlayJumpSound ();
						m_Jump = false;
						m_Jumping = true;
					}
				} else {
					m_MoveDir += Physics.gravity * gravityMultiplier * Time.fixedDeltaTime;
				}

				UpdateCameraPosition (speed);
				ProgressStepCycle (speed);
			}


			Vector3 finalMove = m_MoveDir * Time.fixedDeltaTime;
			if (!limitBoundsEnabled || limitBounds.Contains (pos + finalMove)) {
				m_CollisionFlags = m_CharacterController.Move (finalMove);
			}
			isGrounded = m_CharacterController.isGrounded;

			// Check limits
			if (orbitMode) {
				if (FastVector.ClampDistance (ref lookAt, ref pos, minDistance, maxDistance)) {
					m_CharacterController.transform.position = pos;
				}
			}

			mouseLook.UpdateCursorLock ();

			if (!isGrounded && !isFlying) {
				// Check current chunk
				VoxelChunk chunk = env.GetCurrentChunk ();
				if (chunk != null && !chunk.isRendered) {
					WaitForCurrentChunk ();
					return;
				}
			}
		}


		private void PlayJumpSound () {
			if (isSwimming || isFlying)
				return;
			m_AudioSource.clip = jumpSound;
			m_AudioSource.Play ();
		}


		private void ProgressStepCycle (float speed) {
			if (m_CharacterController.velocity.sqrMagnitude > 0 && (m_Input.x != 0 || m_Input.y != 0)) {
				m_StepCycle += (m_CharacterController.velocity.magnitude + (speed * (isWalking ? 1f : runstepLenghten))) *
				Time.fixedDeltaTime;
			}

			if (!(m_StepCycle > m_NextStep)) {
				return;
			}

			m_NextStep = m_StepCycle + footStepInterval;

			PlayFootStepAudio ();
		}


		private void PlayFootStepAudio () {
			if (!m_CharacterController.isGrounded) {
				return;
			}
			if (footstepSounds == null || footstepSounds.Length == 0)
				return;
			// pick & play a random footstep sound from the array,
			// excluding sound at index 0
			int n;
			if (footstepSounds.Length == 1) {
				n = 0;
			} else {
				n = Random.Range (1, footstepSounds.Length);
			}
			m_AudioSource.clip = footstepSounds [n];
			m_AudioSource.PlayOneShot (m_AudioSource.clip);
			// move picked sound to index 0 so it's not picked next time
			footstepSounds [n] = footstepSounds [0];
			footstepSounds [0] = m_AudioSource.clip;
		}

		private void ProgressSwimCycle (float speed) {
			if (m_CharacterController.velocity.sqrMagnitude > 0 && (m_Input.x != 0 || m_Input.y != 0)) {
				m_StepCycle += (m_CharacterController.velocity.magnitude + speed) * Time.fixedDeltaTime;
			}

			if (!(m_StepCycle > m_NextStep)) {
				return;
			}

			m_NextStep = m_StepCycle + swimStrokeInterval;

			if (!isUnderwater) {
				PlaySwimStrokeAudio ();
			}
		}


		private void PlaySwimStrokeAudio () {
			if (swimStrokeSounds == null || swimStrokeSounds.Length == 0)
				return;
			// pick & play a random swim stroke sound from the array,
			// excluding sound at index 0
			int n;
			if (swimStrokeSounds.Length == 1) {
				n = 0;
			} else {
				n = Random.Range (1, swimStrokeSounds.Length);
			}
			m_AudioSource.clip = swimStrokeSounds [n];
			m_AudioSource.PlayOneShot (m_AudioSource.clip);
			// move picked sound to index 0 so it's not picked next time
			swimStrokeSounds [n] = swimStrokeSounds [0];
			swimStrokeSounds [0] = m_AudioSource.clip;
		}


		private void UpdateCameraPosition (float speed) {
			Vector3 newCameraPosition;
			if (!useHeadBob) {
				return;
			}
			if (m_CharacterController.velocity.magnitude > 0 && m_CharacterController.isGrounded) {
				m_Camera.transform.localPosition =
                    m_HeadBob.DoHeadBob (m_CharacterController.velocity.magnitude +
				(speed * (isWalking ? 1f : runstepLenghten)));
				newCameraPosition = m_Camera.transform.localPosition;
				newCameraPosition.y = m_Camera.transform.localPosition.y - m_JumpBob.Offset ();
			} else {
				newCameraPosition = m_Camera.transform.localPosition;
				newCameraPosition.y = m_OriginalCameraPosition.y - m_JumpBob.Offset ();
			}

			m_Camera.transform.localPosition = newCameraPosition;
		}


		private void GetInput (out float speed) {
			float up = 0;
			bool waswalking = isWalking;
			if (input == null) {
				speed = 0;
				return;
			}
			if (input.GetButton (INPUT_BUTTON_NAMES.LeftControl)) {
				m_Input = Misc.vector3zero;
				speed = 0;
			} else {
				if (input.GetButton (INPUT_BUTTON_NAMES.Up)) {
					up = 1f;
				} else if (input.GetButton (INPUT_BUTTON_NAMES.Down)) {
					up = -1f;
				}

				bool leftShiftPressed = input.GetButton (INPUT_BUTTON_NAMES.LeftShift);
				isWalking = isGrounded && !isInWater && !isFlying && !leftShiftPressed;

				// set the desired speed to be walking or running
				if (isFlying) {
					speed = leftShiftPressed ? flySpeed * 2 : flySpeed;
				} else if (isInWater) {
					speed = swimSpeed;
				} else if (isCrouched) {
					speed = walkSpeed * 0.25f;
				} else if (isWalking) {
					speed = walkSpeed;
				} else {
					speed = runSpeed;
				}
				m_Input = new Vector3 (input.horizontalAxis, input.verticalAxis, up);
			}

			// normalize input if it exceeds 1 in combined length:
			if (m_Input.sqrMagnitude > 1) {
				m_Input.Normalize ();
			}

			// handle speed change to give an fov kick
			// only if the player is going to a run, is running and the fovkick is to be used
			if (isWalking != waswalking && useFovKick && m_CharacterController.velocity.sqrMagnitude > 0) {
				StopAllCoroutines ();
				StartCoroutine (!isWalking ? m_FovKick.FOVKickUp () : m_FovKick.FOVKickDown ());
			}

		}


		private void RotateView () {
			if (switching) {
				switchingLapsed = (Time.time - switchingStartTime) / switchDuration;
				if (switchingLapsed > 1f) {
					switchingLapsed = 1f;
					switching = false;
				}
			} else {
				switchingLapsed = 1;
			}
			mouseLook.LookRotation (transform, m_Camera.transform, orbitMode, lookAt, switchingLapsed);
		}


		private void OnControllerColliderHit (ControllerColliderHit hit) {
			Rigidbody body = hit.collider.attachedRigidbody;
			//dont move the rigidbody if the character is on top of it
			if (m_CollisionFlags == CollisionFlags.Below) {
				return;
			}

			if (body == null || body.isKinematic) {
				return;
			}
			body.AddForceAtPosition (m_CharacterController.velocity * 0.1f, hit.point, ForceMode.Impulse);
		}

		public void SetFootstepSounds (AudioClip[] footStepsSounds, AudioClip jumpSound, AudioClip landSound) {
			this.footstepSounds = footStepsSounds;
			this.jumpSound = jumpSound;
			this.landSound = landSound;
		}

		/// <summary>
		/// Ensures player is above terrain
		/// </summary>
		public void Unstuck () {
			if (env.CheckCollision (env.cameraMain.transform.position) || env.CheckCollision (transform.position)) {
				float minAltitude = env.GetTerrainHeight (transform.position);
				transform.position = new Vector3 (transform.position.x, minAltitude + m_CharacterController.height, transform.position.z);
			}
		}


		/// <summary>
		/// Toggles on/off character light
		/// </summary>
		public void ToggleCharacterLight () {
			Light light = GetComponentInChildren<Light> ();
			if (light != null) {
				light.enabled = !light.enabled;
			}

			if (light.enabled) {
				env.ShowMessage ("<color=green>Player torch <color=yellow>ON</color></color>");
			} else {
				env.ShowMessage ("<color=green>Player torch <color=yellow>OFF</color></color>");
			}
		}

		/// <summary>
		/// Toggles on/off character light
		/// </summary>
		public void ToggleCharacterLight (bool state) {
			Light light = GetComponentInChildren<Light> ();
			if (light != null) {
				light.enabled = state;
			}
		}


		/// <summary>
		/// Removes an unit fcrom current item in player inventory and throws it into the scene
		/// </summary>
		public void ThrowCurrentItem () {
			InventoryItem inventoryItem = player.ConsumeItem ();
			if (inventoryItem == InventoryItem.Null)
				return;

			if (inventoryItem.item.category != ITEM_CATEGORY.Voxel)
				return;

			env.VoxelThrow (m_Camera.transform.position, m_Camera.transform.forward, 15f, inventoryItem.item.voxelType, Misc.color32White);
		}

	}
}
