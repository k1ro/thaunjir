﻿#ifndef VOXELPLAY_COMMON
#define VOXELPLAY_COMMON

#include "UnityCG.cginc"
#include "AutoLight.cginc"
#include "Lighting.cginc"


/* cube coords
   
  7+------+6
  /.   3 /|
2+------+ |
 |4.....|.+5
 |/     |/
0+------+1

*/

const static float3 cubeVerts[8] = { 
	-0.5,	-0.5,	-0.5,
	 0.5,	-0.5,	-0.5,
	-0.5,	 0.5,	-0.5,
	 0.5,	 0.5,	-0.5,
	-0.5,	-0.5,	 0.5,
	 0.5,	-0.5,	 0.5,
	 0.5,	 0.5,	 0.5,
	-0.5,	 0.5,	 0.5
};


fixed _AmbientLight;

#ifndef NON_ARRAY_TEXTURE
UNITY_DECLARE_TEX2DARRAY(_MainTex); 
#endif
float4 _MainTex_TexelSize;

#if VOXELPLAY_USE_AA

#if defined(SHADER_API_D3D11) || defined(SHADER_API_XBOXONE) || defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE) || defined(SHADER_API_METAL)
	#define UNITY_SAMPLE_TEX2DARRAY_GRAD(tex,coord,dx,dy) tex.SampleGrad (sampler##tex,coord,dx,dy)
#elif defined(UNITY_COMPILER_HLSL2GLSL) || defined(SHADER_TARGET_SURFACE_ANALYSIS)
	#define UNITY_SAMPLE_TEX2DARRAY_GRAD(tex,coord,dx,dy) tex2DArray(tex,coord,dx,dy)
#else
	#define UNITY_SAMPLE_TEX2DARRAY_GRAD(tex,coord,dx,dy) UNITY_SAMPLE_TEX2DARRAY(tex,coord)
#endif

inline fixed4 ReadSmoothTexel(float3 uv) {
	float2 ruv = uv.xy * _MainTex_TexelSize.zw - 0.5;
	float2 f = fwidth(ruv);
	uv.xy = (floor(ruv) + 0.5 + saturate( (frac(ruv) - 0.5 + f ) / f)) / _MainTex_TexelSize.zw;	
	return UNITY_SAMPLE_TEX2DARRAY(_MainTex, uv);
}

inline fixed4 ReadSmoothTexelWithDerivatives(float3 uv) {
	float2 ruv = frac(uv.xy) * _MainTex_TexelSize.zw - 0.5;
	float2 f = fwidth(ruv);
	float2 nuv = (floor(ruv) + 0.5 + saturate( (frac(ruv) - 0.5 + f ) / f)) / _MainTex_TexelSize.zw;	
	return UNITY_SAMPLE_TEX2DARRAY_GRAD(_MainTex, float3(nuv, uv.z), ddx(uv.xy), ddy(uv.xy));
}

#define VOXELPLAY_GET_TEXEL_GEO(uv) ReadSmoothTexel(uv);
#define VOXELPLAY_GET_TEXEL(uv) ReadSmoothTexel(uv);
#define VOXELPLAY_GET_TEXEL_DD(uv) ReadSmoothTexelWithDerivatives(uv);

#else

#define VOXELPLAY_GET_TEXEL_GEO(uv) UNITY_SAMPLE_TEX2DARRAY(_MainTex, uv.xyz);
#define VOXELPLAY_GET_TEXEL(uv) UNITY_SAMPLE_TEX2DARRAY(_MainTex, uv.xyz);
#define VOXELPLAY_GET_TEXEL_DD(uv) UNITY_SAMPLE_TEX2DARRAY(_MainTex, float3(frac(uv.xy), uv.z));

#endif

inline float3 GetLight(float3 normal) {
	return saturate(_WorldSpaceLightPos0.xyz * normal) * saturate(1.0 + _WorldSpaceLightPos0.y * 2.0);
}


float3 Shade4PointLightsWithoutNormal (
    float4 lightPosX, float4 lightPosY, float4 lightPosZ,
    float3 lightColor0, float3 lightColor1, float3 lightColor2, float3 lightColor3,
    float4 lightAttenSq,
    float3 pos)
{
    // to light vectors
    float4 toLightX = lightPosX - pos.x;
    float4 toLightY = lightPosY - pos.y;
    float4 toLightZ = lightPosZ - pos.z;
    // squared lengths
    float4 lengthSq = 0;
    lengthSq += toLightX * toLightX;
    lengthSq += toLightY * toLightY;
    lengthSq += toLightZ * toLightZ;
    // don't produce NaNs if some vertex position overlaps with the light
    lengthSq = max(lengthSq, 0.000001);

    // attenuation
    float4 atten = 1.0 / (1.0 + lengthSq * lightAttenSq);
    float4 diff = atten;
    // final color
    float3 col = 0;
    col += lightColor0 * diff.x;
    col += lightColor1 * diff.y;
    col += lightColor2 * diff.z;
    col += lightColor3 * diff.w;
    return col;
}



#if VOXELPLAY_GLOBAL_USE_FOG
half3 _SkyTint;
float _FogAmount;
half _Exposure;
float3 _FogData;

fixed3 getSkyColor(float3 ray) {
	float3 delta  = _WorldSpaceLightPos0.xyz - ray;
	float dist    = dot(delta, delta);
	float y = abs(ray.y);

	// sky base color
	half3 skyColor = _SkyTint;

	// fog
	half fog = saturate(_FogAmount - y) / (1.0001 - _FogAmount);
	skyColor = lerp(skyColor, 1.0.xxx, fog);

	// sky tint
	float hy = abs(_WorldSpaceLightPos0.y) + y;
	half t = saturate( (0.4 - hy) * 2.2) / (1.0 + dist * 0.8);
	skyColor.r = lerp(skyColor.r, 1.0, t);
	skyColor.b = lerp(skyColor.b, 0.0, t);

	// daylight + obscure opposite side of sky
	fixed dayLightDir = 1.0 + _WorldSpaceLightPos0.y * 2.0;
	half daylight = saturate(dayLightDir - dist * 0.03);
	skyColor *= daylight;

	// exposure
	skyColor *= _Exposure * _LightColor0.rgb;

	// gamma
	#if defined(UNITY_COLORSPACE_GAMMA)
	skyColor = sqrt(skyColor);
	#endif

	return skyColor;
}


#define VOXELPLAY_FOG_DATA(idx1) fixed4 skyColor: TEXCOORD##idx1;
#define VOXELPLAY_APPLY_FOG(color, i) color.rgb = lerp(color.rgb, i.skyColor.rgb, i.skyColor.a);

#define VOXELPLAY_INITIALIZE_LIGHT_AND_FOG_GEO(viewDir, normal) fixed3 light = GetLight(normal); i.skyColor = fixed4(getSkyColor(normalize(-viewDir)), saturate((dot(viewDir, viewDir)-_FogData.x)* _FogData.y));
#define VOXELPLAY_INITIALIZE_LIGHT_AND_FOG_NORMAL(worldPos, normal) float3 viewDir = worldPos - _WorldSpaceCameraPos; o.skyColor = fixed4(getSkyColor(normalize(viewDir)), saturate((dot(viewDir, viewDir)-_FogData.x)* _FogData.y)); o.light = saturate(dot(_WorldSpaceLightPos0.xyz, normal)) * saturate(1.0 + _WorldSpaceLightPos0.y * 2.0);
#define VOXELPLAY_INITIALIZE_LIGHT_AND_FOG(worldPos) float3 viewDir = worldPos - _WorldSpaceCameraPos; float3 normal = normalize(-viewDir); o.skyColor = fixed4(getSkyColor(normalize(viewDir)), saturate((dot(viewDir, viewDir)-_FogData.x)* _FogData.y)); o.light = saturate(dot(_WorldSpaceLightPos0.xyz, normal)) * saturate(1.0 + _WorldSpaceLightPos0.y * 2.0);

#else // fallbacks when fog is disabled

#define VOXELPLAY_FOG_DATA(idx1)
#define VOXELPLAY_APPLY_FOG(color, i)
#define VOXELPLAY_INITIALIZE_LIGHT_AND_FOG_GEO(viewDir, normal) fixed3 light = GetLight(normal); //saturate(_WorldSpaceLightPos0.xyz * normal) * saturate(1.0 + _WorldSpaceLightPos0.y * 2.0);
#define VOXELPLAY_INITIALIZE_LIGHT_AND_FOG_NORMAL(worldPos, normal) o.light = saturate(dot(_WorldSpaceLightPos0.xyz, normal)) * saturate(1.0 + _WorldSpaceLightPos0.y * 2.0);
#define VOXELPLAY_INITIALIZE_LIGHT_AND_FOG(worldPos) float3 viewDir = _WorldSpaceCameraPos - worldPos; float3 normal = normalize(viewDir); o.light = saturate(dot(_WorldSpaceLightPos0.xyz, normal)) * saturate(1.0 + _WorldSpaceLightPos0.y * 2.0);

#endif // VOXELPLAY_GLOBAL_USE_FOG


	#if defined(VERTEXLIGHT_ON)
		#define VOXELPLAY_LIGHT_DATA(idx1,idx2) fixed light: TEXCOORD##idx1; fixed3 vertexLightColor: TEXCOORD##idx2;
		#define VOXELPLAY_SET_VERTEX_LIGHT(i, worldPos, normal) i.vertexLightColor = Shade4PointLights(unity_4LightPosX0, unity_4LightPosY0, unity_4LightPosZ0,unity_LightColor[0].rgb, unity_LightColor[1].rgb,unity_LightColor[2].rgb, unity_LightColor[3].rgb,unity_4LightAtten0, worldPos, normal);
		#define VOXELPLAY_SET_VERTEX_LIGHT_WITHOUT_NORMAL(i, worldPos) i.vertexLightColor = Shade4PointLightsWithoutNormal(unity_4LightPosX0, unity_4LightPosY0, unity_4LightPosZ0,unity_LightColor[0].rgb, unity_LightColor[1].rgb,unity_LightColor[2].rgb, unity_LightColor[3].rgb,unity_4LightAtten0, worldPos);
		#define VOXELPLAY_APPLY_LIGHTING(color,i) fixed atten = SHADOW_ATTENUATION(i); atten *= i.light; atten = max(saturate(atten + _WorldSpaceLightPos0.y), _AmbientLight); color.rgb *= atten * _LightColor0.rgb + i.vertexLightColor;
		#define VOXELPLAY_APPLY_LIGHTING_AO_AND_GI(color,i) fixed atten = SHADOW_ATTENUATION(i); atten *= i.light; atten = max(saturate(atten + _WorldSpaceLightPos0.y), _AmbientLight); float ao = i.uv.w; ao = 1.0-(1.0-ao)*(1.0-ao); color.rgb *= atten * ao * _LightColor0 + i.vertexLightColor;
		#define VOXELPLAY_APPLY_LIGHTING_AND_GI(color,i) fixed atten = SHADOW_ATTENUATION(i); atten *= i.light; atten = max(saturate(atten + _WorldSpaceLightPos0.y), _AmbientLight); color.rgb *= atten * i.uv.w * _LightColor0.rgb + i.vertexLightColor;
	#else
		#define VOXELPLAY_LIGHT_DATA(idx1,idx2) fixed light: TEXCOORD##idx1;	// light incidence
		#define VOXELPLAY_SET_VERTEX_LIGHT(i,worldPos, normal) 
		#define VOXELPLAY_SET_VERTEX_LIGHT_WITHOUT_NORMAL(i,worldPos)
		#define VOXELPLAY_APPLY_LIGHTING(color,i) fixed atten = SHADOW_ATTENUATION(i); atten *= i.light; atten = max(saturate(atten + _WorldSpaceLightPos0.y), _AmbientLight); color.rgb *= atten * _LightColor0;
		#define VOXELPLAY_APPLY_LIGHTING_AO_AND_GI(color,i) fixed atten = SHADOW_ATTENUATION(i); atten *= i.light; atten = max(saturate(atten + _WorldSpaceLightPos0.y), _AmbientLight); float ao = i.uv.w; ao = 1.0-(1.0-ao)*(1.0-ao); color.rgb *= atten * ao * _LightColor0.rgb;
		#define VOXELPLAY_APPLY_LIGHTING_AND_GI(color,i) fixed atten = SHADOW_ATTENUATION(i); atten *= i.light; atten = max(saturate(atten + _WorldSpaceLightPos0.y), _AmbientLight); color.rgb *= atten * i.uv.w * _LightColor0.rgb;
	#endif




#endif // VOXELPLAY_COMMON

