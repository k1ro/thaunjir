﻿#include "VPCommon.cginc"

struct appdata {
	float4 vertex   : POSITION;
	float3 uv       : TEXCOORD0;
	float4 uv2		: TEXCOORD1;
	#if VOXELPLAY_USE_TINTING
	fixed3 color    : COLOR;
	#endif
};


struct g2f {
	float4 pos    : SV_POSITION;
	float3 uv     : TEXCOORD0;
	VOXELPLAY_LIGHT_DATA(1,2)
	VOXELPLAY_FOG_DATA(3)
	SHADOW_COORDS(4)
	#if VOXELPLAY_USE_TINTING
	fixed3 color  : COLOR;
	#endif
};

struct vertexInfo {
	float4 vertex;
};

void vert (inout appdata v) {
}


inline void PushCorner(inout g2f i, inout TriangleStream<g2f>o, float3 center, float3 corner, float3 uv) {
	vertexInfo v;
	v.vertex = float4(center + corner, 1.0);
	i.pos    = UnityObjectToClipPos(v.vertex);
	i.uv     = uv;
	TRANSFER_SHADOW(i);
	o.Append(i);
}



#if VOXELPLAY_USE_TINTING
void PushVoxelNoAO(float3 center, float3 uv, fixed3 color, inout TriangleStream<g2f> o) {
#else
void PushVoxelNoAO(float3 center, float3 uv, inout TriangleStream<g2f> o) {
#endif
	// cube vertices
	float3 worldCenter = mul(unity_ObjectToWorld, float4(center, 1.0)).xyz;
	float3 viewDir     = _WorldSpaceCameraPos - worldCenter;
	float3 normal      = sign(viewDir);
	float3 viewSide    = saturate(normal);

	float3 v0          = cubeVerts[0];
	float3 v1          = cubeVerts[1];
	float3 v2          = cubeVerts[2];
	float3 v3          = cubeVerts[3];

	#if defined(VP_CUTOUT)
		float disp = sin(worldCenter.x + worldCenter.y + _Time.w) * 0.005;
		v0.xy += disp;
		v1.xy += disp;
		v2.xy += disp;
		v3.xy += disp;
	#endif

	float3 v4          = -v3;
	float3 v5          = -v2;
	float3 v7          = -v1;

	g2f i;
	VOXELPLAY_INITIALIZE_LIGHT_AND_FOG_GEO(viewDir, normal);
#if VOXELPLAY_USE_TINTING
	i.color  = color;
#endif

	// Front/back face
	VOXELPLAY_SET_VERTEX_LIGHT(i, worldCenter, float3(0,0,normal.z))
	i.light = light.z;
	float3 side = lerp(1.0.xxx, float3(-1,1,-1), viewSide.z);
	PushCorner(i, o, center, v1 * side, float3(1, 0, uv.x));
	PushCorner(i, o, center, v0 * side, float3(0, 0, uv.x));
	PushCorner(i, o, center, v3 * side, float3(1, 1, uv.x));
	PushCorner(i, o, center, v2 * side, float3(0, 1, uv.x));
	o.RestartStrip();

	// Left/right face
	VOXELPLAY_SET_VERTEX_LIGHT(i, worldCenter, float3(normal.x,0,0))
	i.light  = light.x;
	side = lerp(1.0.xxx, float3(-1,1,-1), viewSide.x);
	PushCorner(i, o, center, v0 * side, float3(1, 0, uv.x));
	PushCorner(i, o, center, v4 * side, float3(0, 0, uv.x));
	PushCorner(i, o, center, v2 * side, float3(1, 1, uv.x));
	PushCorner(i, o, center, v7 * side, float3(0, 1, uv.x));
	o.RestartStrip();

	// Top/Bottom face
	VOXELPLAY_SET_VERTEX_LIGHT(i, worldCenter, float3(0,normal.y,0))
	i.light  = light.y;
	side = lerp(1.0.xxx, float3(-1,-1,1), viewSide.y);
	float sideUV = lerp(uv.z, uv.y, viewSide.y);
	PushCorner(i, o, center, v0 * side, float3(0, 1, sideUV));
	PushCorner(i, o, center, v1 * side, float3(1, 1, sideUV));
	PushCorner(i, o, center, v4 * side, float3(0, 0, sideUV));
	PushCorner(i, o, center, v5 * side, float3(1, 0, sideUV));
	o.RestartStrip();
}


[maxvertexcount(12)]
void geom(point appdata i[1], inout TriangleStream<g2f> o) {
#if VOXELPLAY_USE_TINTING
	PushVoxelNoAO(i[0].vertex.xyz, i[0].uv, i[0].color, o);
#else
	PushVoxelNoAO(i[0].vertex.xyz, i[0].uv, o);
#endif
}


fixed4 frag (g2f i) : SV_Target {

	// Diffuse
	fixed4 color   = VOXELPLAY_GET_TEXEL_GEO(i.uv.xyz);

	#if defined(VP_CUTOUT)
	clip(color.a - 0.1);
	#endif
	#if VOXELPLAY_USE_TINTING
	color.rgb *= i.color;
	#endif

	VOXELPLAY_APPLY_LIGHTING(color, i);
	VOXELPLAY_APPLY_FOG(color, i);

	return color;
}

