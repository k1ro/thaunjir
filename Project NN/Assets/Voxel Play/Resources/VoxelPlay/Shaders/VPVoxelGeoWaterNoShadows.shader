﻿Shader "Voxel Play/Voxels/Geo Water No Shadows"
{
	Properties
	{
		[HideInInspector] _MainTex ("Main Texture Array", 2DArray) = "white" {}
		_SkyTint ("Sky Tint", Color) = (0.52, 0.5, 1.0)
		_Exposure("Exposure", Range(0, 8)) = 1.3
		_FogAmount("Fog Amount", Range(0,1)) = 0.5
		_FogData("Fog Data", Vector) = (10000, 0.00001, 0)
		_AmbientLight ("Ambient Light", Float) = 0
	}
	SubShader {

		Tags { "Queue" = "Transparent" "RenderType" = "Transparent" }

		Pass {
			Tags { "LightMode" = "ForwardBase" }
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off
			CGPROGRAM
			#pragma target 4.0
			#pragma vertex   vert
			#pragma geometry geom
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma multi_compile _ VOXELPLAY_GLOBAL_USE_FOG
			#pragma multi_compile _ VERTEXLIGHT_ON
			#pragma multi_compile _ VOXELPLAY_USE_AA
			#include "VPVoxelGeoWater.cginc"
			ENDCG
		}
	}
	Fallback Off
}