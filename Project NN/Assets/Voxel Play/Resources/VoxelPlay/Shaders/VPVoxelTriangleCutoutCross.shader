﻿Shader "Voxel Play/Voxels/Triangle Cutout Cross"
{
	Properties
	{
		[HideInInspector] _MainTex ("Main Texture Array", 2DArray) = "white" {}
		_SkyTint ("Sky Tint", Color) = (0.52, 0.5, 1.0)
		_Exposure("Exposure", Range(0, 8)) = 1.3
		_FogAmount("Fog Amount", Range(0,1)) = 0.5
		_FogData("Fog Data", Vector) = (10000, 0.00001, 0)
		_AmbientLight ("Ambient Light", Float) = 0
	}
	SubShader {

		Tags { "Queue" = "Geometry" "RenderType" = "Opaque" }
		Pass {
			Tags { "LightMode" = "ForwardBase" }
			Cull Off
			CGPROGRAM
			#pragma target 3.5
			#pragma vertex   vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma multi_compile_fwdbase nolightmap nodynlightmap novertexlight nodirlightmap
			#pragma multi_compile _ VOXELPLAY_GLOBAL_USE_FOG
			#pragma multi_compile _ VOXELPLAY_USE_AA
			#pragma multi_compile _ VERTEXLIGHT_ON
			#include "VPVoxelTriangleCutoutCross.cginc"
			ENDCG
		}

		Pass {
			Name "ShadowCaster"
			Tags { "LightMode" = "ShadowCaster" }
//			Cull Off // Commented out for performance; two sided shadows can be expensive for mass grass
			CGPROGRAM
			#pragma target 3.5
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_shadowcaster
			#pragma fragmentoption ARB_precision_hint_fastest
			#include "VPVoxelTriangleCutoutCrossShadows.cginc"
			ENDCG
		}

	}
	Fallback Off
}