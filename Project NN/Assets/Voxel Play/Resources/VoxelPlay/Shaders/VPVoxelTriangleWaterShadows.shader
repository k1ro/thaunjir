﻿Shader "Voxel Play/Voxels/Triangle Water Shadows"
{
	Properties
	{
		[HideInInspector] _MainTex ("Main Texture Array", 2DArray) = "white" {}
		_SkyTint ("Sky Tint", Color) = (0.52, 0.5, 1.0)
		_Exposure("Exposure", Range(0, 8)) = 1.3
		_FogAmount("Fog Amount", Range(0,1)) = 0.5
		_FogData("Fog Data", Vector) = (10000, 0.00001, 0)
		_AmbientLight ("Ambient Light", Float) = 0
	}
	SubShader {

		Tags { "Queue" = "Geometry+1" "RenderType" = "Opaque" }

		GrabPass { "_WaterBackgroundTexture" }

		Pass {
			Tags { "LightMode" = "ForwardBase" }
			ZWrite Off
			CGPROGRAM
			#pragma target 3.5
			#pragma vertex   vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma multi_compile_fwdbase nolightmap nodynlightmap novertexlight nodirlightmap
			#pragma multi_compile _ VOXELPLAY_GLOBAL_USE_FOG
			#pragma multi_compile _ VERTEXLIGHT_ON
			#pragma multi_compile _ VOXELPLAY_USE_AA
			#define USE_SHADOWS
			#include "VPVoxelTriangleWater.cginc"
			ENDCG
		}

		Pass {
			Name "ShadowCaster"
			Tags { "LightMode" = "ShadowCaster" }
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_shadowcaster
			#pragma fragmentoption ARB_precision_hint_fastest
			#include "VPVoxelTriangleShadows.cginc"
			ENDCG
		}

	}
	Fallback Off
}