﻿Shader "Voxel Play/Models/Texture Alpha"
{
	Properties
	{
		_MainTex ("Main Texture", 2D) = "white" {}
		[HideInInspector] _SkyTint ("Sky Tint", Color) = (0.52, 0.5, 1.0)
		[HideInInspector] _Exposure("Exposure", Range(0, 8)) = 1.3
		[HideInInspector] _FogAmount("Fog Amount", Range(0,1)) = 0.5
		[HideInInspector] _FogData("Fog Data", Vector) = (10000, 0.00001, 0)
		[HideInInspector] _AmbientLight ("Ambient Light", Float) = 0
		_Color ("Tint Color", Color) = (1,1,1,1)
		_VoxelLight ("Voxel Light", Range(0,1)) = 1
	}
	SubShader {

		Tags { "Queue" = "Transparent" "RenderType" = "Transparent" }
		Pass {
			Tags { "LightMode" = "ForwardBase" }
			Blend SrcAlpha OneMinusSrcAlpha
			CGPROGRAM
			#pragma target 3.5
			#pragma vertex   vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma multi_compile_fwdbase nolightmap nodynlightmap novertexlight nodirlightmap
			#pragma multi_compile _ VOXELPLAY_GLOBAL_USE_FOG
			#define USE_TEXTURE
			#define NON_ARRAY_TEXTURE
			#include "VPModel.cginc"
			ENDCG
		}

	}
	Fallback Off
}