﻿#include "VPCommon.cginc"

struct appdata {
	float4 vertex   : POSITION;
	float3 normal   : NORMAL;
	fixed4 color    : COLOR;
	#if defined(USE_TEXTURE)
	float2 uv       : TEXCOORD0;
	#endif
};


struct v2f {
	float4 pos    : SV_POSITION;
	VOXELPLAY_LIGHT_DATA(0,1)
	SHADOW_COORDS(2)
	#if defined(USE_TEXTURE)
	float2 uv     : TEXCOORD3;
	#endif
	fixed4 color  : COLOR;
	VOXELPLAY_FOG_DATA(4)
};

fixed4 _Color;
fixed _VoxelLight;
sampler _MainTex;

v2f vert (appdata v) {
	v2f o;

	o.pos    = UnityObjectToClipPos(v.vertex);
	o.color  = v.color;
	#if defined(USE_TEXTURE)
	o.uv     = v.uv;
	#endif

	float3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
	float3 wsNormal = mul(unity_ObjectToWorld, float4(v.normal.xyz, 0));
	VOXELPLAY_INITIALIZE_LIGHT_AND_FOG_NORMAL(worldPos, wsNormal);
	VOXELPLAY_SET_VERTEX_LIGHT(o, worldPos, wsNormal);

	TRANSFER_SHADOW(o);
	return o;
}

fixed4 frag (v2f i) : SV_Target {

	// Diffuse
	#if defined(USE_TEXTURE)
	fixed4 color = tex2D(_MainTex, i.uv) * i.color * _Color;
	#else
	fixed4 color = i.color * _Color;
	#endif
	color.rgb *= _VoxelLight;

	VOXELPLAY_APPLY_LIGHTING(color, i);
	VOXELPLAY_APPLY_FOG(color, i);

	return color;
}

