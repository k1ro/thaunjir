﻿using UnityEngine;
using UnityEngine.EventSystems;

//Attached to Inventory Slot

public class Slot : MonoBehaviour, IDropHandler
{
    public int _id;

    private Inventory _inv;

    private void Start()
    {
        _inv = GameObject.Find("Inventory").GetComponent<Inventory>();
    }

    public void OnDrop(PointerEventData eventData)
    {
        InventoryItemData itemToDrop = eventData.pointerDrag.GetComponent<InventoryItemData>();
        if (_inv._items[_id]._ID == -1) //if there is no item in the slot
        {
            _inv._items[itemToDrop._slot] = new Item();
            _inv._items[_id] = itemToDrop._item;
            itemToDrop._slot = _id;
        }
        else //Swapping items if there is an item in a slot
        {
            Transform itemToSwap = this.transform.GetChild(0);
            itemToSwap.GetComponent<InventoryItemData>()._slot = itemToDrop._slot;
            itemToSwap.transform.SetParent(_inv._slots[itemToDrop._slot].transform);
            itemToSwap.transform.position = _inv._slots[itemToDrop._slot].transform.position;

            itemToDrop._slot = _id;
            itemToDrop.transform.SetParent(this.transform);
            itemToDrop.transform.position = this.transform.position;

            _inv._items[itemToDrop._slot] = itemToSwap.GetComponent<InventoryItemData>()._item;
            _inv._items[_id] = itemToDrop._item;
        }
    }
}
