﻿using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

//Attached to Empty GameObject Inventory

public class Inventory : MonoBehaviour
{
    private GameObject _slotPanel;
    private ItemDatabase _database;
    public GameObject _inventorySlot;
    public GameObject _inventoryItem;

    private int _slotAmount;

    public List<Item> _items = new List<Item>();
    public List<GameObject> _slots = new List<GameObject>();

    private void Start()
    {
        _database = GetComponent<ItemDatabase>();

        _slotAmount = 42;
        _slotPanel = GameObject.Find("Inv Slot Panel");
        //adds all inventory slots to Slots array (visual)
        for (int i = 0; i < _slotAmount; i++)
        {
            _items.Add(new Item());
            _slots.Add(Instantiate(_inventorySlot));
            _slots[i].GetComponent<Slot>()._id = i;
            _slots[i].transform.SetParent(_slotPanel.transform);
        }
        AddItem(0);
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Alpha1))
        {
            AddItem(0);
        }
    }

    public void AddItem(int id)
    {
        Item itemToAdd = _database.FetchItemByID(id);

        if (itemToAdd._Stackable && CheckIfItemIsInInventory(itemToAdd))
        {
            for (int i = 0; i < _items.Count; i++)
            {
                if (_items[i]._ID == id)
                {
                    InventoryItemData data = _slots[i].transform.GetChild(0).GetComponent<InventoryItemData>();
                    //TODO: Add amount of the item you picked up (set in IventoryItemData)

                    int amount = data._amount;
                    amount += data._amount;

                    //int amount = _items[i]._Amount;
                    //amount = amount + _items[i]._Amount;
                    data.transform.GetChild(0).GetComponent<Text>().text = amount.ToString();
                    break;
                }
            }
        }
        else
        {
            for (int i = 0; i < _items.Count; i++)
            {
                //check if slot is empty
                if (_items[i]._ID == -1)
                {
                    _items[i] = itemToAdd;
                    GameObject itemObj = Instantiate(_inventoryItem);
                    itemObj.GetComponent<InventoryItemData>()._item = itemToAdd;
                    itemObj.GetComponent<InventoryItemData>()._slot = i;
                    itemObj.transform.SetParent(_slots[i].transform);
                    itemObj.transform.position = _slots[i].transform.position;
                    itemObj.GetComponent<Image>().sprite = itemToAdd._Sprite;
                    itemObj.name = itemToAdd._Title;
                    break;
                }
            }
        }
    }

    public void CheckInventoryType()
    {
    }

    //Checks if item with the same ID is in the inventory
    public bool CheckIfItemIsInInventory(Item item)
    {
        for (int i = 0; i < _items.Count; i++)
            if (_items[i]._ID == item._ID)
                return true;

        return false;
    }
}
