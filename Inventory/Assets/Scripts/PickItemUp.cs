﻿using UnityEngine;

public class PickItemUp : MonoBehaviour
{
    private Inventory _inv;

    private void Start()
    {
        _inv = GameObject.Find("Inventory").GetComponent<Inventory>();
    }

    private void Update()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit))
            {
                _inv.AddItem(hit.transform.GetComponent<CollectablesData>()._id);
                //Destroy(hit.transform);
                Debug.Log(hit.transform.name);
            }
        }
    }
}
