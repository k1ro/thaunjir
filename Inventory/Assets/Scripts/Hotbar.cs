﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Hotbar : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerDownHandler
{
    public List<GameObject> _slots = new List<GameObject>();

    private void Start()
    {
        //Set ID's of hotbar (1 to 0)
        for (int i = 0; i < _slots.Count; i++)
        {
            if (i == 9) _slots[i].GetComponent<Slot>()._id = 0;
            else _slots[i].GetComponent<Slot>()._id = i + 1;
        }
    }



    public void OnBeginDrag(PointerEventData eventData)
    {
        throw new System.NotImplementedException();
    }

    public void OnDrag(PointerEventData eventData)
    {
        throw new System.NotImplementedException();
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        throw new System.NotImplementedException();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
    }
}
