﻿using UnityEngine;
using UnityEngine.EventSystems;

//Attached to Inventory Items

public class InventoryItemData : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerDownHandler
{
    public Item _item;
    public int _slot;
    public int _amount;

    private Inventory _inv;
    private Vector2 _offset;

    private void Start()
    {
        _inv = GameObject.Find("Inventory").GetComponent<Inventory>();
        _amount = _item._Amount;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        _offset = eventData.position - new Vector2(this.transform.position.x, this.transform.position.y);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (_item != null)
        {
            this.transform.SetParent(GameObject.Find("Panels").transform);
            this.transform.position = eventData.position - _offset;
            GetComponent<CanvasGroup>().blocksRaycasts = false;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (_item != null)
        {
            this.transform.position = eventData.position - _offset;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        this.transform.SetParent(_inv._slots[_slot].transform);
        this.transform.position = _inv._slots[_slot].transform.position;
        GetComponent<CanvasGroup>().blocksRaycasts = true;
    }
}
