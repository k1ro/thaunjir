﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using LitJson;

//Attached to Empty GameObject Inventory

public class ItemDatabase : MonoBehaviour
{
    private List<Item> _dataBase = new List<Item>();
    private JsonData _itemData;

    private void Start()
    {
        _itemData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/Items.json"));
        ConstructItemDatabase();
    }

    public Item FetchItemByID(int id)
    {
        for (int i = 0; i < _dataBase.Count; i++)
            if (_dataBase[i]._ID == id)
                return _dataBase[i];

        return null;
    }

    private void ConstructItemDatabase()
    {
        for (int i = 0; i < _itemData.Count; i++)
        {
            _dataBase.Add(new Item((int)_itemData[i]["id"], (int)_itemData[i]["amount"], _itemData[i]["title"].ToString(), (int)_itemData[i]["value"], 
                (bool)_itemData[i]["stackable"], _itemData[i]["slug"].ToString()));
        }
    }
}

public class Item
{
    public int _ID { get; set; }
    public int _Amount { get; set; }
    public string _Title { get; set; }
    public int _Value { get; set; }
    public bool _Stackable { get; set; }
    public string _Slug { get; set; }
    public Sprite _Sprite { get; set; }

    public Item(int id, int amount, string title, int value, bool stackable, string slug)
    {
        this._ID = id;
        this._Amount = amount;
        this._Title = title;
        this._Value = value;
        this._Stackable = stackable;
        this._Slug = slug;
        this._Sprite = Resources.Load<Sprite>("Sprites/" + slug);
    }

    public Item()
    {
        this._ID = -1;
    }
}
