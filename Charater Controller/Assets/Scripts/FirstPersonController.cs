﻿using UnityEngine;

namespace Kiro
{
    [RequireComponent(typeof(CapsuleCollider))]
    [RequireComponent(typeof(Rigidbody))]
    public partial class FirstPersonController : MonoBehaviour
    {
        [Header("State Flags")]
        [Tooltip("Player uses walk speeds")]
        public bool _isWalking;

        [Tooltip("When the player is touching a specified ground")]
        public bool _isGrounded;

        [Tooltip("Player is pressing the run key")]
        public bool _isRunning;

        [Tooltip("Player is pressing the crouch key")]
        public bool _isCrouched;

        [Tooltip("Player is pressing the prone key")]
        public bool _isProned;

        [Tooltip("Player is not touching any ground")]
        public bool _isFalling;

        [Space]
        [Header("Movement")]
        public float _forwardSpeed = 5.0f;
        public float _backwardsSpeed = 3.0f;
        public float _strafeSpeed = 5.0f;
        public float _runMultiplier = 2.0f;
        public float _crouchMultiplier = 0.5f;
        //public float _crouchHeight = 0.5f;
        public float _proneMultiplier = 0.25f;
        //public float _proneHeight = 0.2f;
        public float _gravityMultiplier = 2.0f;
        public float _jumpForce = 3.0f;
        public float _crouchProneSmooth = 2.0f;
        public bool _airControl = false;

        [Space]
        [Header("Climbing")]
        public float _distanceToClimb = 0.3f;

        [Space]
        [Header("Head Bob and FOVKick")]
        public float _footStepInterval = 5.0f;
        public bool _useHeadBob = true;
        [Range(0f, 1f)] public float _runstepLengthen = 0.7f;
        public CurveControlledBob _headBob = new CurveControlledBob();
        public LerpControlledBob _jumpBob = new LerpControlledBob();
        public FOVKick _fovKick = new FOVKick();
        private Vector3 _originalCameraPosition;
        private Vector3 newCameraPosition;

        private Vector3 _moveInput = Vector3.zero;

        private bool _jump, _jumping;
        private bool _previouslyGrounded;
        private bool _canClimb, _climbing, _climb;
        private float _maxClimbHeight = 3;

        private bool _willJump, _willClimb;

        private Vector3 _moveDir;

        private Camera _cam;
        private Rigidbody _rb;
        private CapsuleCollider _capsuleCollider;
        private float _capsuleColliderRadius;
        private float _capsuleColliderHeight;
        private Vector3 _capsuleColliderCenter;

        private FirstPersonController _FPController;
        public MouseLook _mouseLook;
        public InputController _input;

        public FirstPersonController instance
        {
            get
            {
                if (_FPController == null)
                    _FPController = GetComponent<FirstPersonController>();
                return _FPController;
            }
        }

        private void Start()
        {
            _input = new KeyboardMouseInputController();
            _input.Init();
            _capsuleCollider = GetComponent<CapsuleCollider>();
            _rb = GetComponent<Rigidbody>();
            _cam = Camera.main;

            _originalCameraPosition = _cam.transform.localPosition;
            _capsuleColliderRadius = _capsuleCollider.radius;
            _capsuleColliderHeight = _capsuleCollider.height;
            _capsuleColliderCenter = new Vector3(0f, _capsuleColliderHeight-_capsuleColliderHeight, 0f);

            _fovKick.Setup(_cam);
            _headBob.Setup(_cam, _footStepInterval);
            _mouseLook.Init(transform, _cam.transform, _input);
        }

        private void Update()
        {
            GroundCheck();
            RotateView();
            CheckMultyBoundKeys();

            if (!_previouslyGrounded && _isGrounded)
            {
                StartCoroutine(_jumpBob.DoBobCycle());
                _moveDir.y = 0f;
                //Play landing sound here
                _jumping = false;
            }
            if (!_isGrounded && !_jumping && _previouslyGrounded)
                _moveDir.y = 0f;

            _previouslyGrounded = _isGrounded;

            if (_input._focused)
            {
                //bool leftAltPressed = _input.GetButton(INPUT_BUTTON_NAMES.LeftAlt);
                //bool sprintPressed = _input.GetButton(INPUT_BUTTON_NAMES.Sprint);
                //bool crouchPressed = _input.GetButton(INPUT_BUTTON_NAMES.Crouch);
                //bool fire1Pressed = _input.GetButton(INPUT_BUTTON_NAMES.Button1);
                //bool fire2Pressed = _input.GetButton(INPUT_BUTTON_NAMES.Button2);

                if (_input.GetButton(INPUT_BUTTON_NAMES.Sprint))
                    _isRunning = true;
                else if (_input.GetButtonUp(INPUT_BUTTON_NAMES.Sprint))
                    _isRunning = false;
                if (_input.GetButtonDown(INPUT_BUTTON_NAMES.Crouch))
                {
                    _isCrouched = !_isCrouched;
                    ScaleCapsuleCollider();
                }
                else if (_input.GetButtonDown(INPUT_BUTTON_NAMES.Prone))
                {
                    _isProned = !_isProned;
                    ScaleCapsuleCollider();
                }
            }
        }

        private void FixedUpdate()
        {
            float speed = 0f;
            GetInput(out speed);

            Vector3 desiredMove = _rb.transform.forward * _moveInput.y + _rb.transform.right * _moveInput.x;
            _moveDir = desiredMove * speed;
            
            //Jumping
            if (_isGrounded)
            {
                if (_jump)
                {
                    _rb.AddForce(new Vector3(_moveDir.x, _jumpForce, _moveDir.z), ForceMode.Impulse);
                    _jump = false;
                    _jumping = true;
                }
            }

            //Climbing
            if (!_climbing)
            {
                _climb = _input.GetButton(INPUT_BUTTON_NAMES.Climb);
                RaycastHit hit;
                Ray ray = new Ray(transform.position, transform.forward);
                if (Physics.Raycast(ray, out hit, _capsuleCollider.radius + _distanceToClimb))
                {
                    _willJump = false;
                    _willClimb = true;
                    _canClimb = true;
                    if (_canClimb && _climb && _isGrounded)
                    {
                        _climbing = true;
                        ClimbUp(hit);
                    }
                }
                else
                {
                    _canClimb = false;
                    _willJump = true;
                    _willClimb = false;
                }
                //Debug.Log("Will Climb: " + _willClimb);
                //Debug.Log("Will Jump: " + _willJump);
            }

            //Falling
            if (!_isGrounded && !_jumping && _rb.velocity.y < -0.1)
            {
                _rb.AddForce(new Vector3(_moveDir.x, 0f, _moveDir.z), ForceMode.Impulse);
                _isFalling = true;
            }
            else
            {
                _capsuleCollider.radius = _capsuleColliderRadius;
                _isFalling = false;
            }

            Vector3 finalMove = _rb.position + _moveDir * Time.fixedDeltaTime;
            if (_airControl || _isGrounded)
                _rb.MovePosition(finalMove);

            UpdateCameraPosition(speed);
        }

        private void ScaleCapsuleCollider()
        {
            //Crouching
            if (_isCrouched)
            {
                _capsuleCollider.height = 1f;
                transform.position = new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z);
            }
            else
            {
                _capsuleCollider.height = _capsuleColliderHeight;
                transform.position = new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z);
            } 
        }

        private void UpdateCameraPosition(float speed)
        {
            if (!_useHeadBob) return;

            //bool ? if bool = true : if bool = false
            if (_rb.velocity.magnitude > 0 && _isGrounded && !_jumping)
            {
                _cam.transform.localPosition = _headBob.DoHeadBob(_rb.velocity.magnitude + (speed * (!_isRunning ? 1f : _runstepLengthen)));
                newCameraPosition = _cam.transform.localPosition;
                newCameraPosition.y = _cam.transform.localPosition.y - _jumpBob.Offset();
            }
            else
            {
                newCameraPosition = _cam.transform.localPosition;
                newCameraPosition.y = _originalCameraPosition.y - _jumpBob.Offset();
            }

            _cam.transform.localPosition = newCameraPosition;
        }

        private void ClimbUp(RaycastHit obstacleToClimb)
        {
            if (obstacleToClimb.transform.localScale.y <= _maxClimbHeight)
            {
                Vector3 yOffset = new Vector3(0f, obstacleToClimb.transform.localScale.y + 0.8f, 0f);
                transform.position = obstacleToClimb.transform.position + yOffset;
                _climbing = false;
            }
        }

        private void CheckMultyBoundKeys()
        {
            if (!_jump && _willJump)
                _jump = _input.GetButtonDown(INPUT_BUTTON_NAMES.Jump);

            if (!_climb && _willClimb)
                _climb = _input.GetButton(INPUT_BUTTON_NAMES.Climb);
        }

        private void RotateView()
        {
            _mouseLook.LookRotation(transform, _cam.transform);
        }

        private void GetInput(out float speed)
        {
            speed = 0f;
            bool wasWalking = _isWalking;

            if ((_climbing || _isFalling || _jumping) && !_airControl)
            {
                _moveInput = Vector3.zero;
                return;
            }

            if (_input == null)
            {
                speed = 0f;
                return;
            }

            _moveInput = new Vector3(_input._horizontalAxis, _input._verticalAxis, 0f);
            _isWalking = _isGrounded && !_isRunning;

            if (_moveInput.y > 0)
                speed = _forwardSpeed;
            else if (_moveInput.y < 0)
            {
                _isRunning = false;
                speed = _backwardsSpeed;
            }
            else if (_moveInput.x < 0 || _moveInput.x > 0)
                speed = _strafeSpeed;

            if (_isCrouched)
                speed *= _crouchMultiplier;
            else if (_isProned)
                speed *= _proneMultiplier;
            else if (_isRunning)
                speed *= _runMultiplier;

            if (_moveInput.sqrMagnitude > 1f)
                _moveInput.Normalize();

            if (_isWalking != wasWalking && _isRunning && _rb.velocity.magnitude > 0.01)
            {
                StopAllCoroutines();
                StartCoroutine(!_isWalking ? _fovKick.FOVKickUp() : _fovKick.FOVKickDown());
            }
        }

        private void GroundCheck()
        {
            RaycastHit hit;
            if (Physics.SphereCast(transform.position, _capsuleCollider.radius * (1 - 0.1f), Vector3.down, out hit,
                ((_capsuleCollider.height / 2) - _capsuleCollider.radius) + 0.1f, Physics.AllLayers, QueryTriggerInteraction.Ignore))
                _isGrounded = true;
            else
                _isGrounded = false;
            if (_isGrounded && _jumping)
                _jumping = false;
            
        }

        private void OnDrawGizmos()
        {
            //Gizmos.DrawWireSphere(_capsuleColliderCenter, 1f);
        }
    }
}
