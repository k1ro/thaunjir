﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kiro
{

    public class Gameloop : MonoBehaviour
    {
        private FirstPersonController _controller;

	    void Start ()
        {
            _controller = GameObject.FindObjectOfType<FirstPersonController>();
	    }
	
	    void Update ()
        {
            //Updates the InputController
            _controller._input.UpdateButtons();
	    }
    }
}

