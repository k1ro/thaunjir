﻿using System;
using System.Collections;
using UnityEngine;

namespace Kiro
{
    [Serializable]
    public class FOVKick
    {
        public Camera _cam;
        [HideInInspector] public float _originalFov;
        public float _fovIncrease = 3f;
        public float _timeToIncrease = 1f;
        public float _timeToDecrease = 1f;
        public AnimationCurve _increaseCurve;


        public void Setup(Camera camera)
        {
            _cam = camera;
            _originalFov = camera.fieldOfView;
        }

        public IEnumerator FOVKickUp()
        {
            float t = Mathf.Abs((_cam.fieldOfView - _originalFov) / _fovIncrease);
            while (t < _timeToIncrease)
            {
                _cam.fieldOfView = _originalFov + (_increaseCurve.Evaluate(t / _timeToIncrease) * _fovIncrease);
                t += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
        }


        public IEnumerator FOVKickDown()
        {
            float t = Mathf.Abs((_cam.fieldOfView - _originalFov) / _fovIncrease);
            while (t > 0)
            {
                _cam.fieldOfView = _originalFov + (_increaseCurve.Evaluate(t / _timeToDecrease) * _fovIncrease);
                t -= Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }

            //make sure that fov returns to the original size
            _cam.fieldOfView = _originalFov;
        }
    }
}
