﻿using System;
using UnityEngine;

namespace Kiro
{
    [Serializable]
    public class CurveControlledBob
    {
        public float _horizontalBobRange = 0.2f;
        public float _verticalBobRange = 0.2f;
        public AnimationCurve _bobCurve = new AnimationCurve(new Keyframe(0f, 0f), new Keyframe(0.5f, 1f),
                                                            new Keyframe(1f, 0f), new Keyframe(1.5f, -1f),
                                                            new Keyframe(2f, 0f)); // sin curve for head bob
        public float _verticaltoHorizontalRatio = 2f;

        private float _cyclePositionX;
        private float _cyclePositionY;
        private float _bobBaseInterval;
        private Vector3 _originalCameraPosition;
        private float _time;


        public void Setup(Camera camera, float bobBaseInterval)
        {
            _bobBaseInterval = bobBaseInterval;
            _originalCameraPosition = camera.transform.localPosition;

            // get the length of the curve in time
            _time = _bobCurve[_bobCurve.length - 1].time;
        }


        public Vector3 DoHeadBob(float speed)
        {
            float xPos = _originalCameraPosition.x + (_bobCurve.Evaluate(_cyclePositionX) * _horizontalBobRange);
            float yPos = _originalCameraPosition.y + (_bobCurve.Evaluate(_cyclePositionY) * _verticalBobRange);

            _cyclePositionX += (speed * Time.deltaTime) / _bobBaseInterval;
            _cyclePositionY += ((speed * Time.deltaTime) / _bobBaseInterval) * _verticaltoHorizontalRatio;

            if (_cyclePositionX > _time)
                _cyclePositionX = _cyclePositionX - _time;
            if (_cyclePositionY > _time)
                _cyclePositionY = _cyclePositionY - _time;

            return new Vector3(xPos, yPos, 0f);
        }
    }
}
