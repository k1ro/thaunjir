﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kiro
{
    [Serializable]
    public class MouseLook
    {
        public float _xSens = 7.0f;
        public float _ySens = 7.0f;
        public float _xMax = 85.0f;
        public float _xMin = -85.0f;
        public bool _smooth = true;
        public float _smoothTime = 5.0f;

        private Quaternion _playerTargetRot;
        private Quaternion _cameraTargetRot;

        public InputController _input;

        public void Init(Transform player, Transform camera, InputController input)
        {
            _playerTargetRot = player.localRotation;
            _cameraTargetRot = camera.localRotation;
            if (input != null)
                _input = input;
        }

        public void LookRotation(Transform player, Transform camera)
        {
            if (_input == null) return;

            float yRot = _input._mouseX * _xSens;
            float xRot = _input._mouseY * _ySens;
            _playerTargetRot *= Quaternion.Euler(0f, yRot, 0f);
            _cameraTargetRot *= Quaternion.Euler(-xRot, 0f, 0f);

            _cameraTargetRot = ClampRotationAroundXAxis(_cameraTargetRot);

            if (_smooth)
            {
                player.localRotation = Quaternion.Slerp(player.localRotation, _playerTargetRot, _smoothTime * Time.deltaTime);
                camera.localRotation = Quaternion.Slerp(camera.localRotation, _cameraTargetRot, _smoothTime * Time.deltaTime);
            }
            else
            {
                player.localRotation = _playerTargetRot;
                camera.localRotation = _cameraTargetRot;
            }

        }

        Quaternion ClampRotationAroundXAxis(Quaternion q)
        {
            q.x /= q.w;
            q.y /= q.w;
            q.z /= q.w;
            q.w = 1.0f;

            float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);
            angleX = Mathf.Clamp(angleX, _xMin, _xMax);
            q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

            return q;
        }

    }
}
