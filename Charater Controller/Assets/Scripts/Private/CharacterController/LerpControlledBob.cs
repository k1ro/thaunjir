﻿using System;
using System.Collections;
using UnityEngine;

namespace Kiro
{
    [Serializable]
    public class LerpControlledBob
    {
        public float _bobDuration = 0.2f;
        public float _bobAmount = 0.2f;

        private float _offset = 0.0f;

        public float Offset()
        {
            return _offset;
        }

        public IEnumerator DoBobCycle()
        {
            //make the camera move down slightly
            float t = 0f;
            while(t < _bobDuration)
            {
                _offset = Mathf.Lerp(0f, _bobAmount, t / _bobDuration);
                t += Time.deltaTime;
                yield return new WaitForFixedUpdate();
            }
            //make it move back to neutral
            t = 0f;
            while (t < _bobDuration)
            {
                _offset = Mathf.Lerp(_bobAmount, 0f, t / _bobDuration);
                t += Time.deltaTime;
                yield return new WaitForFixedUpdate();
            }
            _offset = 0f;
        }
    }
}
