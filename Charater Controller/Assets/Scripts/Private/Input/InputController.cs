﻿using System;
using UnityEngine;

namespace Kiro
{
    public enum INPUT_BUTTON_NAMES
    {
        Button1,
        Button2,
        Button3,
        Jump,
        Crouch,
        Climb,
        Sprint,
        LeftAlt,
        Prone,
        Inventory
    }

    public enum INPUT_BUTTON_STATE
    {
        Idle,
        Down,
        Up,
        Pressed
    }

    public abstract class InputController
    {
        /// <summary>
		/// Horizontal input axis
		/// </summary>
		[NonSerialized]
        public float _horizontalAxis;

        /// <summary>
        /// Vertical input axis
        /// </summary>
        [NonSerialized]
        public float _verticalAxis;

        /// <summary>
        /// Horizontal mouse axis
        /// </summary>
        [NonSerialized]
        public float _mouseX;

        /// <summary>
        /// Vertical mouse axis
        /// </summary>
        [NonSerialized]
        public float _mouseY;

        /// <summary>
        /// Location of cursor on screen
        /// </summary>
        [NonSerialized]
        public Vector3 _screenPos;

        /// <summary>
        /// If cursor is inside screen
        /// </summary>
        [NonSerialized]
        public bool _focused;

        [NonSerialized]
        public bool _initialized;

        protected INPUT_BUTTON_STATE[] _buttons;

        protected virtual bool Initialize() { return true; }
        protected abstract void UpdateInputState();

        /// <summary>
        /// Returns true if any button or key is pressed
        /// </summary>
        public bool _anyKey;

        public bool GetButton (INPUT_BUTTON_NAMES button)
        {
            return _buttons[(int)button] == INPUT_BUTTON_STATE.Pressed;
        }

        public bool GetButtonDown(INPUT_BUTTON_NAMES button)
        {
            return _buttons[(int)button] == INPUT_BUTTON_STATE.Down;
        }

        public bool GetButtonUp(INPUT_BUTTON_NAMES button)
        {
            return _buttons[(int)button] == INPUT_BUTTON_STATE.Up;
        }

        public void Init()
        {
            int buttonCount = Enum.GetNames(typeof(INPUT_BUTTON_NAMES)).Length;
            _buttons = new INPUT_BUTTON_STATE[buttonCount];
            _initialized = Initialize();
        }

        public void UpdateButtons()
        {
            if (!_initialized) return;
            _anyKey = Input.anyKey;
            for (int i = 0; i < _buttons.Length; i++)
                _buttons[i] = INPUT_BUTTON_STATE.Idle;
            UpdateInputState();
            if (!_anyKey)
            {
                for (int i = 0; i < _buttons.Length; i++)
                {
                    if (_buttons[i] != INPUT_BUTTON_STATE.Idle)
                    {
                        _anyKey = true;
                        break;
                    }
                }
            }
        }

        protected void ReadButtonStates (INPUT_BUTTON_NAMES button, string buttonName)
        {
            if (Input.GetButton(buttonName))
                _buttons[(int)button] = INPUT_BUTTON_STATE.Pressed;
            if (Input.GetButtonDown(buttonName))
                _buttons[(int)button] = INPUT_BUTTON_STATE.Down;
            if (Input.GetButtonUp(buttonName))
                _buttons[(int)button] = INPUT_BUTTON_STATE.Up;
        }

        protected void ReadKeyStates(INPUT_BUTTON_NAMES button, KeyCode keyCode)
        {
            if (Input.GetKey(keyCode))
                _buttons[(int)button] = INPUT_BUTTON_STATE.Pressed;
            if (Input.GetKeyDown(keyCode))
                _buttons[(int)button] = INPUT_BUTTON_STATE.Down;
            if (Input.GetKeyUp(keyCode))
                _buttons[(int)button] = INPUT_BUTTON_STATE.Up;
        }

        protected void ReadMouseButtonState(INPUT_BUTTON_NAMES mouseButton, int button)
        {
            if (Input.GetMouseButton(button))
                _buttons[(int)mouseButton] = INPUT_BUTTON_STATE.Pressed;
            if (Input.GetMouseButtonDown(button))
                _buttons[(int)mouseButton] = INPUT_BUTTON_STATE.Down;
            if (Input.GetMouseButtonUp(button))
                _buttons[(int)mouseButton] = INPUT_BUTTON_STATE.Up;
        }
    }
}

