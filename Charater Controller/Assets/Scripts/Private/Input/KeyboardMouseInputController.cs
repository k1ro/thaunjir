﻿using UnityEngine;

namespace Kiro
{

    public class KeyboardMouseInputController : InputController
    {

	    protected override void UpdateInputState()
        {
            _screenPos = Input.mousePosition;
            _focused = _screenPos.x >= 0 && _screenPos.x < Screen.width && _screenPos.y >= 0 && _screenPos.y < Screen.height;

            _horizontalAxis = Input.GetAxis("Horizontal");
            _verticalAxis = Input.GetAxis("Vertical");
            _mouseX = Input.GetAxis("Mouse X");
            _mouseY = Input.GetAxis("Mouse Y");

            //Left mouse button
            ReadMouseButtonState(INPUT_BUTTON_NAMES.Button1, 0);
            //Right mouse button
            ReadMouseButtonState(INPUT_BUTTON_NAMES.Button2, 1);
            //Middle mouse button
            ReadMouseButtonState(INPUT_BUTTON_NAMES.Button3, 2);

            ReadKeyStates(INPUT_BUTTON_NAMES.Jump, KeyCode.Space);
            ReadKeyStates(INPUT_BUTTON_NAMES.Crouch, KeyCode.C);
            ReadKeyStates(INPUT_BUTTON_NAMES.Climb, KeyCode.Space);
            ReadKeyStates(INPUT_BUTTON_NAMES.Sprint, KeyCode.LeftShift);
            ReadKeyStates(INPUT_BUTTON_NAMES.LeftAlt, KeyCode.LeftAlt);
            ReadKeyStates(INPUT_BUTTON_NAMES.Prone, KeyCode.X);
            ReadKeyStates(INPUT_BUTTON_NAMES.Inventory, KeyCode.I);
        }
    }
}
