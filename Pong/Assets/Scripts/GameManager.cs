﻿using UnityEngine;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    public GameObject _player;
    public GameObject _border;
    public GameObject _goal;
    public GameObject _ball;

    public bool _instBall = false;

    private List<GameObject> _borders = new List<GameObject>();
    private List<GameObject> _goals = new List<GameObject>();

    private Camera _cam;

    private void Awake()
    {
        _cam = Camera.main;

        Vector3 width = _cam.ViewportToWorldPoint(new Vector3(1f, 0f, 0f));
        Vector3 height = _cam.ViewportToWorldPoint(new Vector3(0f, 1f, 0f));
        Vector3 leftBorder = _cam.ViewportToWorldPoint(new Vector3(0f, 0.5f, 0f));
        Vector3 rightBorder = _cam.ViewportToWorldPoint(new Vector3(1f, 0.5f, 0f));

        Vector3 offsetLeft = new Vector3(1f, 0f, 10f);
        Vector3 offsetRight = new Vector3(1f, 0f, -10f);

        //Instantiate Players
        GameObject player = Instantiate(_player, leftBorder + offsetLeft, Quaternion.identity);
        player.transform.name = "Player01";
        player = Instantiate(_player, rightBorder - offsetRight, Quaternion.identity);
        player.transform.name = "Player02";

        //Instantiate Borders
        GameObject lowerBorder = Instantiate(_border, new Vector3(0f, -height.y - 0.5f, 0f), Quaternion.identity);
        lowerBorder.transform.name = "Lower Border";
        _borders.Add(lowerBorder);
        GameObject upperBorder = Instantiate(_border, new Vector3(0f, height.y + 0.5f, 0f), Quaternion.identity);
        upperBorder.transform.name = "Upper Border";
        _borders.Add(upperBorder);
        foreach (GameObject border in _borders)
        {
            BoxCollider borderCol = border.GetComponent<BoxCollider>();
            borderCol.size = new Vector3(width.x * 2, 1f, 0.1f);
            borderCol.isTrigger = false;
        }

        //Instantiate Goals
        GameObject leftGoal = Instantiate(_goal, new Vector3(-width.x - 0.5f, 0f, 0f), Quaternion.identity);
        leftGoal.transform.name = "Left Goal";
        _goals.Add(leftGoal);
        GameObject rightGoal = Instantiate(_goal, new Vector3(width.x + 0.5f, 0f, 0f), Quaternion.identity);
        rightGoal.transform.name = "Right Goal";
        _goals.Add(rightGoal);
        foreach (GameObject goal in _goals)
        {
            BoxCollider goalCol = goal.GetComponent<BoxCollider>();
            goalCol.size = new Vector3(1f, height.y * 2, 0.1f);
            goalCol.isTrigger = true;
        }

        //Instantiate Ball
    }

    private void Update()
    {
        if (_instBall)
        {
            GameObject ball = Instantiate(_ball, Vector3.zero, Quaternion.identity);
            ball.transform.name = "Ball";
            _instBall = false;
        }
    }
}
