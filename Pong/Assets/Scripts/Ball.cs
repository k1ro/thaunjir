﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour
{
    public float _startSpeed = 10f;
    public float _speedSafe;
    public float _speedMultiplyer = 0.1f;

    public bool _canMove = true;
    public bool _reset = false;
    public float _resetTimer = 3.0f;

    private Vector3 _currentVelocity;
    private Vector3 _newVelocity;

    private float _randomX;
    private float _randomY;

    private Countdown _countdown;

    private void Start()
    {
        _countdown = GameObject.FindGameObjectWithTag("Countdown").GetComponent<Countdown>();
        _speedSafe = _startSpeed;
        CalculateRandomDirection();
    }

    private void Update()
    {
        if (_canMove)
        {
            gameObject.transform.position += _currentVelocity * _startSpeed * Time.deltaTime;
            _startSpeed += Time.deltaTime * _speedMultiplyer;

            if (_reset)
            {
                gameObject.transform.position = Vector3.zero;

                _startSpeed = _speedSafe;
                CalculateRandomDirection();
                _countdown._ballReset = true;
                _reset = false;
            }
        }
        if (!_reset)
        {
            _canMove = true;
        }
    }

    private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Border")
        {
            _newVelocity = Vector3.Reflect(_currentVelocity, Vector3.up);
            _currentVelocity = _newVelocity;
        }

        if (col.gameObject.tag == "Player")
        {
            _newVelocity = Vector3.Reflect(_currentVelocity, Vector3.right);
            _currentVelocity = _newVelocity;
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Goal")
        {
            _reset = true;
        }
    }

    private void CalculateRandomDirection()
    {
        _randomX = Random.Range(-5, 5);
        _randomY = Random.Range(-5, 5);
        if (_randomX == 0 || _randomY == 0)
        {
            CalculateRandomDirection();
        }
        Vector3 velocity = new Vector3(_randomX, _randomY, 0f).normalized;
        _currentVelocity = velocity;
    }

    private IEnumerator WaitAfterReset(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
    }
}
