﻿using UnityEngine;

public class Controller : MonoBehaviour
{
    public float _moveSpeed = 10f;

    private bool _moveUp = true;
    private bool _moveDown = true;

    private void Update()
    {
        if (Input.GetKey(KeyCode.W) && _moveUp)
        {
            gameObject.transform.position += Vector3.up * _moveSpeed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.S) && _moveDown)
        {
            gameObject.transform.position += Vector3.down * _moveSpeed * Time.deltaTime;
        }
    }

    private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Border")
        {
            if (col.gameObject.transform.position.y - gameObject.transform.position.y > 0)
            {
                _moveUp = false;
            }
            else if (col.gameObject.transform.position.y - gameObject.transform.position.y < 0)
            {
                _moveDown = false;
            }
        }
    }

    private void OnCollisionExit(Collision col)
    {
        _moveDown = true;
        _moveUp = true;
    }
}
