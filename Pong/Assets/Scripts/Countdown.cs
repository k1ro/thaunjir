﻿using UnityEngine;
using UnityEngine.UI;

public class Countdown : MonoBehaviour
{
    public GameObject _gameManager;
    private GameObject _gameManagerObj;
    private GameObject[] _gameManagerArray;

    public Text _countdownTxt;
    public bool _countingDown = true;

    public bool _ballReset = false;

    private float _countdownAmount = 3.5f;
    private float _countdownValue;

    private void Start()
    {
        _countdownValue = _countdownAmount;
    }

    private void Update()
    {
        _gameManagerArray = GameObject.FindGameObjectsWithTag("GameManager");
        if (_gameManagerArray.Length <= 0)
        {
            _gameManagerObj = Instantiate(_gameManager, Vector3.zero, Quaternion.identity);
        }
        else if (_gameManagerArray.Length > 1)
        {
            Destroy(_gameManagerArray[0]);
        }

        if (_ballReset)
        {
            foreach (GameObject obj in _gameManagerArray)
            {
                Destroy(obj);
            }
            _countingDown = true;
        }

        if (_countingDown)
        {
            _countdownTxt.enabled = true;
            _countdownTxt.text = Mathf.RoundToInt(_countdownValue).ToString();
            _countdownValue -= Time.deltaTime;
            if (_countdownValue <= 0)
            {
                _countdownTxt.enabled = false;
                _countingDown = false;
                _gameManagerObj.GetComponent<GameManager>()._instBall = true;
                _countdownValue = _countdownAmount;
            }
        }
    }
}
